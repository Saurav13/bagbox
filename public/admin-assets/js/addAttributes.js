angular.module('Product', [])
.controller('AttributesController', ['$scope', function($scope){
   
    $scope.attributes=[];

    $scope.Retriever = function(links)
    {
        $scope.attributes = links ? links : [];
    }

    $scope.addAttribute=function(){
      $scope.attributes.push({name:"",link:""});
    }

    $scope.delAttribute = function(i){
        $scope.attributes.splice(i,1);
    }
}])
