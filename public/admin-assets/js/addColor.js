angular.module('SdpProduct', [])
.controller('FixedDetailController', ['$scope', function($scope){
    $scope.colors = [];
    $scope.sizes =[];
    $scope.attributes=[];
    $scope.addColorfield=function(){
      $scope.colors.push({})
    }
    $scope.delColor = function(i){
        $scope.colors.splice(i,1);
    }
    $scope.addSize = function(){
        $scope.sizes.push({});
    }
    $scope.delSize = function(i){
        $scope.sizes.splice(i,1);
    }
    $scope.addAttributeField=function(){
        $scope.attributes.push({name:"",value:[]});
    }
    $scope.delAttr = function(i){
        $scope.attributes.splice(i,1);
    }
    $scope.addvalue = function(i){
       $scope.attributes[i].value.push("");
    }
    $scope.delValue = function(a,i){
        $scope.attributes[a].value.splice(i,1);
    }

}])
