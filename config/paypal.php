<?php 
return [ 
   
    'client_id' => env('PAYPAL_CLIENT_ID'),
    'secret_key' => env('PAYPAL_SECRET_KEY'),
    'mode' => env('PAYPAL_MODE','sandbox'),
];