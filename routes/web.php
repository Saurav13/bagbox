<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Auth::routes(['verify' => true]);

// Route::get('/register/getEmail',function(){
//     return redirect('login');
// });



// Route::post('register/getEmail','Auth\LoginController@setEmail')->name('set_email');
// Route::get('signin/{provider}', 'Auth\LoginController@redirectToProvider');
// Route::get('signin/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('/blog/{alias}','Frontend\BlogController@post')->name('blog');
Route::get('/blog','Frontend\BlogController@index');

Route::get('/', 'Frontend\PageController@home')->name('home');
Route::get('/landing3', 'Frontend\PageController@index3')->name('home');
Route::get('/landing4', 'Frontend\PageController@index4')->name('home');

Route::get('home', 'Frontend\PageController@home');

Route::get('/clients','Frontend\PageController@clients');
Route::post('newsletter/subscribe','Frontend\PageController@NewsLetter')->name('newsletter.subscribe');
Route::get('/category/{slug}/{sub_slug}','Frontend\PageController@category');
Route::get('/category/{slug}','Frontend\PageController@category');
Route::get('/about-us', 'Frontend\PageController@about');
Route::get('/privacy-policy', 'Frontend\PageController@privacyPolicy');
Route::get('/terms-and-conditions', 'Frontend\PageController@termsAndConditions');
Route::get('/inquiry', 'Frontend\PageController@inquiry');
Route::post('/inquiry/submit', 'Frontend\PageController@inquirySubmit');



// Route::get('/shop','Frontend\PageController@shop');

Route::get('/shop/{category_slug?}','Frontend\PageController@shop')->name('shop');

Route::get('/products/{slug}','Frontend\PageController@products');

Route::get('/search','Frontend\PageController@Search');
Route::get('/testimonials','Frontend\PageController@testimonials');
// Route::get('/contact',function(){
//     return view('frontend.contact');
// });
// Route::get('/about',function(){
//     $testimonials= Testimonial::all();
//     return view('frontend.about')->with('testimonials',$testimonials);
// });

Route::get('/cart','Frontend\CartController@cart');

Route::post('/cart/additem','Frontend\CartController@addItem');
Route::post('/cart/removeitem/{rowId}','Frontend\CartController@removeItem');
Route::post('/cart/emptycart','Frontend\CartController@emptyCart');
Route::post('/cart/updateqty','Frontend\CartController@updateQty');

Route::post('/checkout','Frontend\CheckoutController@shippingInformation');
Route::get('/checkout','Frontend\CheckoutController@summary');

Route::post("/payment/paypal", 'Frontend\CheckoutController@payWithPaypal');
Route::post("/payment/paypal/return", 'Frontend\CheckoutController@getPaypalPaymentStatus');

Route::get('payment/{code}','Frontend\CheckoutController@payment')->name('payment');
Route::post('payment/{code}','Frontend\CheckoutController@paymentSuccess')->name('payment');

Route::get('/order/{slug}','Frontend\PageController@Order');

Route::post('post/message','Frontend\PageController@Feedback')->name('contact.us');
Route::post('post/order','Frontend\PageController@GeneralOrder')->name('general.order');


// Route::get('/home', 'HomeController@index')->name('home');
// Route::get('/test', 'HomeController@index');

// Route::post('/subscribe','HomeController@subscribe')->name('subscribe');
// Route::get('unsubscribe/{token}','HomeController@unsubscribe')->name('unsubscribe');
// Route::post('unsubscribe/{token}','HomeController@unsubscribed')->name('unsubscribed');



Route::prefix('admin')->group(function(){
    Route::get('/login','Admin\Auth\LoginController@showLoginForm')->name('admin_login');
	Route::post('/login','Admin\Auth\LoginController@login');
	Route::post('/logout','Admin\Auth\LoginController@logout')->name('admin_logout');
	Route::post('/password/email','Admin\Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin_password.email');
	Route::post('/password/reset','Admin\Auth\ResetPasswordController@reset');
	Route::get('/password/reset','Admin\Auth\ForgotPasswordController@showLinkRequestForm')->name('admin_password.request');
	Route::get('/password/reset/{token}','Admin\Auth\ResetPasswordController@showResetForm')->name('admin_password.reset');
    
    Route::get('profile','Admin\ProfileController@index')->name('profile');
    Route::post('profile/changePassword','Admin\ProfileController@changePassword')->name('profile.changePassword');
    Route::post('profile/changeName','Admin\ProfileController@updateName')->name('profile.updateName');
    
    Route::get('manage/categories','Admin\CategoryController@index')->name('category.index');
    Route::get('manage/infos','Admin\InfoController@index')->name('infos.index');


    Route::post('albums/{id}/addImages','Admin\AlbumController@addImages')->name('albums.addImages');
    Route::delete('albums/{album_id}/deleteImage/{image_id}','Admin\AlbumController@deleteImage')->name('albums.deleteImage');
    Route::resource('albums', 'Admin\AlbumController',['except' => ['create','edit']]);

    Route::get('settings','Admin\SettingsController@index')->name('admin.settings');
    Route::post('settings/addImage','Admin\SettingsController@addImage')->name('admin.settings.addImage');
    Route::delete('settings/removeImage/{id}','Admin\SettingsController@removeImage')->name('admin.settings.removeImage');
    Route::post('settings/contactUpdate','Admin\SettingsController@contactUpdate')->name('admin.settings.contactUpdate');
    Route::post('settings/aboutUpdate','Admin\SettingsController@aboutUpdate')->name('admin.settings.aboutUpdate');
    Route::post('settings/edit-socials','Admin\SettingsController@editSocials')->name('admin.settings.editSocials');
    
    Route::get('newsletter','Admin\NewsletterController@newsletter')->name('newsletter');
    Route::post('newsletter/send','Admin\NewsletterController@newsletterSend')->name('newsletterSend');

    Route::get('contact-us-messages/unseen','Admin\ContactUsMessageController@unseenMsg')->name('contact-us-messages.unseen');    
    Route::get('contact-us-messages/reply/{id}','Admin\ContactUsMessageController@reply')->name('contact-us-messages.reply');
    Route::post('contact-us-messages/reply/{id}','Admin\ContactUsMessageController@replySend')->name('contact-us-messages.replySend');
    Route::resource('contact-us-messages', 'Admin\ContactUsMessageController',['only' => ['show','destroy','index']]);
    Route::get('getUnseenMsgCount','Admin\ContactUsMessageController@getUnseenMsgCount')->name('getUnseenMsgCount');
    Route::get('getUnseenMsg','Admin\ContactUsMessageController@getUnseenMsg')->name('getUnseenMsg');
    
    //order Routes
    Route::get('quotations/all','Admin\QuotationController@allQuotations')->name('all.quotations');
    Route::post('quotations/delete','Admin\AdminController@DeleteQuotation')->name('delete.quotation');
    Route::get('quotations/export','Admin\QuotationController@export')->name('quotations.export');
    Route::get('quotations/{id}','Admin\AdminController@ShowQuotation')->name('show.quotation');
    Route::get('quotations','Admin\QuotationController@index')->name('quotations.index');
    Route::get('getUnseenQuotationCount','Admin\QuotationController@getUnseenOrderCount')->name('getUnseenQuotationCount');
    Route::get('getUnseenQuotation','Admin\QuotationController@getUnseenOrder')->name('getUnseenQuotation');
    Route::post('markseen/quotation','Admin\QuotationController@MarkSeen');

    Route::get('orders/all','Admin\OrderController@allOrders')->name('all.orders');
    Route::delete('orders/{id}/delete','Admin\AdminController@DeleteOrder')->name('orders.destroy');
    Route::get('orders/export','Admin\OrderController@export')->name('orders.export');
    Route::get('orders/{id}','Admin\AdminController@ShowOrder')->name('show.order');
    Route::patch('orders/{id}','Admin\OrderController@update')->name('orders.update');
    Route::get('orders','Admin\OrderController@index')->name('orders.index');
    Route::get('getUnseenOrderCount','Admin\OrderController@getUnseenOrderCount')->name('getUnseenOrderCount');
    Route::get('getUnseenOrder','Admin\OrderController@getUnseenOrder')->name('getUnseenOrder');
    Route::post('markseen/order','Admin\OrderController@MarkSeen');

    Route::resource('blogs', 'Admin\BlogController',['except' => ['create']]);

    Route::post('/testimonials/update-status','Admin\TestimonialController@updateStatus')->name('testimonials.activate');
    Route::resource('testimonials', 'Admin\TestimonialController',['except' => ['create','show']]);

    Route::post('pages/{page}/update','Admin\PageController@update')->name('pages.update');
    Route::get('pages','Admin\PageController@index')->name('pages.index');

    Route::get('dashboard','Admin\AdminController@index')->name('admin_dashboard');

    Route::get('/','Admin\AdminController@index');
    
    //Category routes
    
    Route::post('/manage/categories','Admin\CategoryController@StoreCat')->name('category.add');
    Route::get('/edit/category/{id}','Admin\CategoryController@EditCat')->name('category.edit');
    Route::post('/update/category/{id}','Admin\CategoryController@UpdateCat')->name('category.update');
    Route::post('/delete/category/{id}','Admin\CategoryController@DestroyCat')->name('category.delete');
    
    Route::post('/edit/info','Admin\InfoController@EditInfo')->name('info.edit');
    
    
    Route::get('manage/clients','Admin\ClientController@index');

    Route::post('/manage/clients','Admin\ClientController@StoreCat')->name('client.add');
    Route::post('/edit/client','Admin\ClientController@EditCat')->name('client.edit');
    Route::post('/delete/client','Admin\ClientController@DestroyCat')->name('client.delete');
    Route::post('/client/feature','Admin\ClientController@FeatureClient');

    Route::get('/downloads','Admin\DownloadController@index')->name('downloads.index');
    Route::post('/downloads','Admin\DownloadController@store')->name('download.add');
    Route::post('/edit/download','Admin\DownloadController@update')->name('download.update');
    Route::post('/delete/download','Admin\DownloadController@destroy')->name('download.delete');

    Route::get('/deliveryzones','Admin\DeliveryZoneController@index')->name('delivery_zones.index');
    Route::post('/deliveryzones','Admin\DeliveryZoneController@store')->name('delivery_zone.add');
    Route::post('/edit/delivery_zone','Admin\DeliveryZoneController@update')->name('delivery_zone.update');
    Route::post('/delete/delivery_zone','Admin\DeliveryZoneController@destroy')->name('delivery_zone.delete');
    
    //subcategory routes
    Route::post('/manage/subcategories','Admin\CategoryController@StoreSubCat')->name('subcategory.add');
    Route::post('/delete/subcategory','Admin\CategoryController@DestroySubCat')->name('subcategory.delete');
    Route::post('/edit/subcategory','Admin\CategoryController@EditSubCat')->name('subcategory.edit');
    
    Route::resource('brands', 'Admin\BrandController',['except' => ['create','show']]);

    Route::resource('tags', 'Admin\TagController',['only' => ['index','store','update','destroy']]);

    //Product routes
    Route::get('/product-subcat/{id}','Admin\ProductController@getSubCat');
    Route::get('/filter/products/{subcat_id}','Admin\ProductController@FilterProduct');
    Route::post('/product/feature','Admin\ProductController@FeatureProduct');
    Route::post('/product/activate','Admin\ProductController@ActivateProduct');
    Route::post('/product/showinslider','Admin\ProductController@ShowInSliderProduct');
    Route::post('product/image/delete','Admin\ProductController@RemoveProductImage')->name('productimage.delete');
    Route::resource('products', 'Admin\ProductController',['except' => ['show']]);
    
    Route::get('seo-settings/{page}','Admin\SettingsController@page')->name('admin.seo-settings');
    Route::post('seo-settings/{page}/update','Admin\SettingsController@pageUpdate')->name('admin.seo-settings.update');
});

Route::get('/asset/{source}/{img}/{h}/{w}',function($source,$img, $h, $w){
    $ext = explode(".",$img);
    $ext = end($ext);
    $source = str_replace('*','/',$source);
    return \Image::make(public_path("/".$source."/".$img))->resize($h, $w)->response($ext);
})->name('optimize');

