<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(AdminUserSeeder::class);
        // $this->call(InfoSeeder::class);
        // $this->call(SettingsSeeder::class);
        // $this->call(AdditionalInfoSeeder::class);

        $this->call(SEOSeeder::class);
        
        
    }
}
