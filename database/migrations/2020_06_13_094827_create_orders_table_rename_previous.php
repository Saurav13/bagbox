<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTableRenamePrevious extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('orders', 'quotations');

        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status')->default('pending');
            $table->boolean('paid')->default('0');
            $table->text('billing');
            $table->dateTime('ordered_at')->nullable();
            $table->decimal('amount');
            $table->string('payment_method')->nullable();
            $table->string('shipping_method');
            $table->tinyInteger('seen')->default('0');
            $table->string('code')->unique();
            $table->string('txn_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders_table_rename_previous');
    }
}
