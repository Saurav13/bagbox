<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Product;
use App\Category;
use App\Download;
use App\AdditionalInfo;
use Auth;

class FrontendNavComposer
{
    /**
     * Create a movie composer.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $nav_categories = Category::orderBy('sort')->get();
        $downloads=Download::all();
        $fproducts = Product::where('active',true)->where('featured',true)->orderBy('id','desc')->limit(3)->get();

        $contact = json_decode(AdditionalInfo::where('alias','contact')->first()->info);
        $social_links = json_decode(AdditionalInfo::where('alias','social_links')->first()->info);

        $home_meta_title = 'Home | Bagbox Motorcycles Trading LLC';
        $home_meta_description = '';
        $home_meta_keywords = '';
        if(AdditionalInfo::where('alias','home_page_settings')->exists()){
            $home_meta_tags = json_decode(AdditionalInfo::where('alias','home_page_settings')->first()->info);
            $home_meta_title = $home_meta_tags->meta_title;
            $home_meta_description = $home_meta_tags->meta_description;
            $home_meta_keywords = $home_meta_tags->meta_keywords;
        }

        $view->with('nav_categories',$nav_categories)->with('fproducts',$fproducts)->with('downloads',$downloads)->with('social_links',$social_links)->with('contact',$contact)
            ->with('home_meta_title', $home_meta_title)
            ->with('home_meta_description', $home_meta_description)
            ->with('home_meta_keywords', $home_meta_keywords);
    }
}