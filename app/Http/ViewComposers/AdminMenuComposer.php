<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\ContactUsMessage;
use App\Order;
use App\Quotation;
use Auth;

class AdminMenuComposer
{
    /**
     * Create a movie composer.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $mno = ContactUsMessage::where('seen','0')->count();
        $ordercount = Order::where('seen','0')
                    ->where(function ($query) {
                        $query->where('status','processing')->orWhere('status','complete');
                    })
                    ->count();
        $quotationcount = Quotation::where('seen','0')->count();

        $view->with('mno',$mno)->with('ordercount',$ordercount)->with('quotationcount',$quotationcount);
    }
}