<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\CustomerInvoiceMail;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\Order;
use App\OrderItem;
use App\Product;
use App\DeliveryZone;
use Cart;
use Auth;
use Session;
use Mail;

use PayPalCheckoutSdk\Orders\OrdersCreateRequest;
use PayPalCheckoutSdk\Orders\OrdersCaptureRequest;
use PayPalCheckoutSdk\Core\PayPalHttpClient;
use PayPalCheckoutSdk\Core\SandboxEnvironment;
use PayPalCheckoutSdk\Core\ProductionEnvironment;

class CheckoutController extends Controller
{
    public function summary()
    {
        $cart_items = Cart::content();

        if(count($cart_items) == 0)
            return redirect('cart');

        $total = 0;
        $purchases = [];
        foreach($cart_items as $c){
            $product = new OrderItem;
            $product->product_id = $c->id;
            $product->product_name = $c->name;
            $product->quantity = $c->qty;
            $product->amount = $c->price;
            $total += $product->amount*$product->quantity;
            $purchases []= $product;
        }
        if(!$this->CheckAvailability($purchases))
            return redirect('cart');
        
        $delivery_zones = DeliveryZone::get();

        return view('frontend.checkout',compact('cart_items','delivery_zones'));
    }

    public function shippingInformation(Request $request){

        $this->validate($request,array(
            'shipping.name' => 'required|max:191',
            'shipping.email' => 'required|email',
            'shipping.phone' => 'required',
            'shipping.address' => 'required|max:191',
            'shipping.company' => 'nullable|max:191',
            'shipping.city' => 'required|max:191',
            // 'shipping_method' => 'required|in:standard_delivery,pick_up'
        ));
        
        $cart_items = Cart::content();

        if(count($cart_items) == 0)
            return response()->json(['error' => 'Your Cart is empty.'], 422);   

        $total = 0;
        $purchases = [];
        foreach($cart_items as $c){
            $product = [];
            $product['product_id'] = $c->id;
            $product['product_name'] = $c->name;
            $product['quantity'] = $c->qty;
            $product['amount'] = $c->price;
            $total += $product['amount']*$product['quantity'];
            $purchases []= $product;
        }

        // if($request->shipping_method == 'standard_delivery'){
            $delivery_zone = DeliveryZone::where('name',$request->shipping['city'])->first();
            if(!$delivery_zone)
                return response()->json(['error' => 'Delivery is not available in your location yet.'], 422);   

            $shippingCharge = $delivery_zone->delivery_charge;

            $product = [];
            $product['product_name'] = 'Standard Delivery';
            $product['quantity'] = 1;
            $product['amount'] = $shippingCharge;
            $product['type'] = 'Shipping';
            $total += $shippingCharge;
            $purchases []= $product;
        // }

        $order = new Order;
        $order->amount = $total;
        $order->code = Str::random(20);
        $order->shipping_method = 'standard_delivery';
        $order->billing = json_encode($request->shipping);
         
        $order->save();

        $order->items()->createMany($purchases);

        return response()->json(['orderId' => $order->code], 200);
    }

    public function payment($code)
    {
        $order = Order::where('code','=',$code)->firstOrFail();

        if($order->status != 'pending'){
            Session::flash('success', 'The order has already been completed');
            return redirect('/home');
        }

        return view('frontend.payment')->with('order',$order);
    }

    public function payWithPaypal(Request $request){

        $order = Order::where('status','pending')->where('id',$request->order_uid)->first();
        
        if(!$order){
            return response()->json(['message' => 'Order not found'], 404);
        }

        if($order->amount < 1){
            return response()->json(['message' => 'Online Payment is only available for total above USD 1.'], 422);
        }

        if(!$this->CheckAvailability($order->items()->where('type','Product')->get())){
            return response()->json(['message' => Session::get('error')], 422);
        }

        $order_request = new OrdersCreateRequest();
        $order_request->prefer('return=representation');
        $order_request->body = array(
            'intent' => 'CAPTURE',
            'application_context' =>
                array(
                    'return_url' => url('/'),
                    'cancel_url' => url('/')
                ),
            'purchase_units' =>[[
                "reference_id" => $order->id,
                "description" => 'Payment for Order #'.$order->id,
                "amount" => [
                    "value" => round($order->amount * 0.272,2),
                    "currency_code" => 'USD'
                ]
            ]]
        );
    
        try {
            $paypal = config('paypal');
            $clientId = $paypal['client_id'];
            $clientSecret = $paypal['secret_key'];

            $env = $paypal['mode'];
            if($env == 'live') 
                $client = new PayPalHttpClient(new ProductionEnvironment($clientId, $clientSecret));
            else
                $client = new PayPalHttpClient(new SandboxEnvironment($clientId, $clientSecret));

            $response = $client->execute($order_request);
            $order->txn_id = $response->result->id;
            $order->save();
            return response()->json($response,200);
            
        } catch (\Exception $ex) {
            return response()->json(['message' => 'Something went wrong'],500);
        }
        
    }

    public function getPaypalPaymentStatus(Request $request)
    {
        $order = Order::where('status','pending')->where('id',$request->order_uid)->where('txn_id',$request->orderID)->firstOrFail();

        $order_request = new OrdersCaptureRequest($request->orderID);
    
        try {
            $paypal = config('paypal');
            $clientId = $paypal['client_id'];
            $clientSecret = $paypal['secret_key'];

            $env = $paypal['mode'];

            if($env == 'live') 
                $client = new PayPalHttpClient(new ProductionEnvironment($clientId, $clientSecret));
            else
                $client = new PayPalHttpClient(new SandboxEnvironment($clientId, $clientSecret));

            $response = $client->execute($order_request);
            
            if ($response->result->status == 'COMPLETED') {
                $order->status = 'processing';
                $order->payment_method = 'Paypal';
                $order->paid = 1;
                $order->txn_id = $response->result->purchase_units[0]->payments->captures[0]->id;
                $this->orderSuccess($order);
                return response()->json($response,200);
            }
            return response()->json(['message' => 'Something went wrong'],500);
            
        } catch (HttpException $ex) {
            return response()->json(['message' => 'Something went wrong'],$ex->statusCode);
        }
    }

    public function paymentSuccess($code, Request $request){
        $order = Order::where('code','=',$code)->firstOrFail();

        if($order->status!= 'pending'){
            Session::flash('success', 'The order has already been completed');
            return redirect('/home');
        }
        
        if($request->payment_method == 'COD'){
            if(!$this->CheckAvailability($order->items()->where('type','Product')->get())){
                return redirect('/cart');                
            }
            $order->payment_method = 'COD';
            $order->status = 'processing';

            $this->orderSuccess($order);
            
            return redirect('/home');
        }
            
        return redirect()->route('home');
    }

    private function CheckAvailability($purchases){
        foreach ($purchases as $purchase) { 
            
            $product = Product::find($purchase->product_id);
            
            if(!$product){
                Session::flash('error', $purchase->product_name . ' is no longer available.');
                return false;
            }
            if(!$product->is_retail){
                Session::flash('error', $purchase->product_name . ' cannot be purchased.');
                return false;
            }
            if($product->quantity === 0){
                Session::flash('error', $purchase->product_name . ' has already sold out');
                return false;
            }

            if($product->quantity && $purchase->quantity > $product->quantity){
                Session::flash('error', $purchase->product_name . ' only ' . $product->quantity . ' in stock');
                return false;
            }
        }
        return true;
    }

    private function orderSuccess($order){
        $purchases = $order->items()->where('type','Product')->get();
        //update quantity

        foreach ($purchases as $purchase) { 

            $product = Product::find($purchase->product_id);

            if($product->quantity == null) continue;

            $product->quantity = $product->quantity - $purchase->quantity;

            if($product->quantity<0) $product->quantity = 0;

            $product->save();
        }

        $order->ordered_at = Carbon::now();
        $order->save();

        Cart::destroy();

        Session::flash('success', 'Thank you. Your order has successfully been placed and an invoice has been sent to your email address.');
        Mail::send(new CustomerInvoiceMail($order));
    }

}
