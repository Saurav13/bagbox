<?php

namespace App\Http\Controllers\Frontend;

use App\AdditionalInfo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Blog;

class BlogController extends Controller
{
    public function index()
    {
        $meta_title = 'News & Events | Bagbox Motorcycles Trading LLC';
        $meta_description = '';
        $meta_keywords = '';
        if(AdditionalInfo::where('alias','blog_page_settings')->exists()){
            $meta_tags = json_decode(AdditionalInfo::where('alias','blog_page_settings')->first()->info);
            $meta_title = $meta_tags->meta_title ? : $meta_title;
            $meta_description = $meta_tags->meta_description;
            $meta_keywords = $meta_tags->meta_keywords;
        }

        $posts = Blog::orderBy('updated_at','desc')->paginate(10);
        return view('frontend.blog',compact('posts', 'meta_title', 'meta_description', 'meta_keywords'));
    }

    public function post($alias){
        $post = Blog::where('slug',$alias)->first();
        if(!$post) abort(404);

        return view('frontend.post',compact('post'));
    }
}
