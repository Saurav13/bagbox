<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use Cart;

class CartController extends Controller
{

    public function addItem(Request $request)
    {
        $request->validate([
            'id' => 'required|exists:products,id',
            'quantity' => 'required|integer|min:1'
        ]);

        $product = Product::findOrFail($request->id);
        if(!$product->is_retail)
            return response()->json(['error' => 'Product cannot be added to the cart.'], 422);
            
        $qty = $product->quantity;
        $qty_buy = $request->quantity ? (int)$request->quantity : 1;

        if($qty !== null && ($qty === 0 || $qty < $qty_buy))
        {
            return response()->json(['error' => 'Out of Stock'], 422);
        }
        //checking if the added product quantity exceeds the remaining quantity

        if($qty){
            foreach (Cart::content() as $c) {
                
                if($c->id == $product->id )
                {
                    if($c->qty + $qty_buy > $qty)
                        return response()->json(['error' => 'Out of Stock'], 422);
                }
            }
        }

        Cart::add(['id'=>$product->id,'name'=>$product->product_name,'qty'=>$qty_buy,'price'=>$product->price, 'options' => ['image' => $product->firstImage ]]);       

        return response()->json(['cart' => Cart::content(), 'count' => Cart::content()->count()], 200);

    }

    public function emptyCart()
    {
        Cart::destroy();
        return redirect('cart');      
    }

    public function updateQty(Request $request)
    {   
        $request->validate([
            'quantity' => 'required|integer|min:1'
        ]);

        $product=Cart::get($request->rowId);

        $qty = Product::find($product->id)->quantity;

        if($request->quantity < 0)
        {
            return response()->json(['error' => 'Invalid Entry'], 422);   
        }
        if($request->quantity > 10)
        {
             return response()->json(['error' => 'Exceeds Purchase Limit'], 422);   
        }

        if((int)$request->quantity-$request->quantity!=0)
        {
            return response()->json(['error' => 'Input integer'], 422);   
        }

        if($qty === 0)
        {
            return response()->json(['error' => 'The Product has sold out.'], 422);   
        }

        if(!$qty || $request->quantity <= $qty)
        {
            Cart::update($request->rowId, $request->quantity);
            return response()->json(['item' => Cart::get($request->rowId), 'total' => Cart::subtotal()], 200);
        }
        else
        {
            return response()->json(['error' => 'Out of Stock'], 422);
        }
    }

    public function removeItem(Request $request)
    {
        Cart::remove($request->rowId);
        return response()->json(['rowId' => $request->rowId, 'total' => Cart::subtotal(), 'count' => Cart::content()->count()], 200);
    }

    public function cart()
    {
        $cart_items = Cart::content();

        if(count($cart_items) == 0)
            return view('frontend.emptycart');

        return view('frontend.cart')->with('cart_items',$cart_items);
    }
}
