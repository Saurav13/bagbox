<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Subcategory;
use App\Product;
use App\ProductImage;
use Session;
use App\ContactUsMessage;
use App\Quotation;
use App\Testimonial;
use App\Subscriber;
use App\LandingImage;
use App\Blog;
use App\Client;
use App\Info;
use Validator;
use App\AdditionalInfo;
class PageController extends Controller
{
    public function home()
    {
        $recent_products=Product::where('active','=',true)->orderBy('id','desc')->take(4)->get();
        $featured_products=Product::where('active','=',true)->where('featured','=',true)->get();
        $slider_products=Product::where('active','=',true)->where('in_landing_slider','=',true)->get();

        $clients=Client::where('featured','=',true)->get();
        $infos=Info::all();
        $images=collect([]);
        // $testimonials =Testimonial::where('active',1)->get();
        $landing_images = LandingImage::all();
        $posts = Blog::orderBy('updated_at','desc')->limit(3)->get();

        foreach($featured_products as $p){
            $image=ProductImage::where('product_id','=',$p->id)->get();
            $images=$images->merge($image);   
        }
        foreach($recent_products as $p){
            $image=ProductImage::where('product_id','=',$p->id)->get();
            $images=$images->merge($image);   
        }
        
        return view('frontend.landing3')->with('clients',$clients)->with('infos',$infos)->with('posts',$posts)->with('slider_products',$slider_products)->with('recent_products',$recent_products)->with('featured_products',$featured_products)->with('images',$images)->with('landing_images',$landing_images);
    }
    public function index3()
    {
        $recent_products=Product::where('active','=',true)->orderBy('id','desc')->take(4)->get();
        $featured_products=Product::where('active','=',true)->where('featured','=',true)->get();

        $images=collect([]);
        $testimonials =Testimonial::where('active',1)->get();
        $landing_images = LandingImage::all();
        $posts = Blog::orderBy('updated_at','desc')->limit(3)->get();

        foreach($featured_products as $p){
            $image=ProductImage::where('product_id','=',$p->id)->get();
            $images=$images->merge($image);   
        }
        foreach($recent_products as $p){
            $image=ProductImage::where('product_id','=',$p->id)->get();
            $images=$images->merge($image);   
        }
        
        return view('frontend.landing3')->with('posts',$posts)->with('recent_products',$recent_products)->with('featured_products',$featured_products)->with('images',$images)->with('testimonials',$testimonials)->with('landing_images',$landing_images);
        
    }
    public function index4()
    {
        $recent_products=Product::where('active','=',true)->orderBy('id','desc')->take(4)->get();
        $featured_products=Product::where('active','=',true)->where('featured','=',true)->get();

        $images=collect([]);
        $testimonials =Testimonial::where('active',1)->get();
        $landing_images = LandingImage::all();
        $posts = Blog::orderBy('updated_at','desc')->limit(3)->get();

        foreach($featured_products as $p){
            $image=ProductImage::where('product_id','=',$p->id)->get();
            $images=$images->merge($image);   
        }
        foreach($recent_products as $p){
            $image=ProductImage::where('product_id','=',$p->id)->get();
            $images=$images->merge($image);   
        } 
        return view('frontend.landing4')->with('posts',$posts)->with('recent_products',$recent_products)->with('featured_products',$featured_products)->with('images',$images)->with('testimonials',$testimonials)->with('landing_images',$landing_images);

    }

    public function category($slug)
    {
        $category=Category::where('slug',$slug)->first();
        $products=Product::where('active','=',true)->get();
        $images=ProductImage::all();
        return view('frontend.category')->with('category',$category)->with('products',$products)->with('images',$images);
    }

   
    public function shop($category_slug = 'catalog', Request $request, Product $product){
        $meta_title = '';
        $meta_description = '';
        $meta_keywords = '';

        $product = $product->where('active','=',true)->newQuery();

        $category = null;

        if($category_slug != 'catalog'){
            $category=Category::where('slug',$category_slug)->firstOrFail();
            $product->whereHas('category', function ($query) use ($request) {
                $query->where('categories.slug', $request->category_slug);
            });

            $meta_title = $category->meta_title ? : $category->cat_name;
            $meta_description = $category->meta_description;
            $meta_keywords = $category->meta_keywords;
        }else{
            if(AdditionalInfo::where('alias','shop_page_settings')->exists()){
                $meta_tags = json_decode(AdditionalInfo::where('alias','shop_page_settings')->first()->info);
                $meta_title = $meta_tags->meta_title ? : $meta_title;
                $meta_description = $meta_tags->meta_description;
                $meta_keywords = $meta_tags->meta_keywords;
            }
        }

        if($request->search){
            $tags = explode(' ',$request->search);

            $product->join('product_tag', 'product_tag.product_id', 'products.id')
                ->join('tags', 'product_tag.tag_id', 'tags.id')
                ->where(function ($query) use ($tags) {
                    $queryMethod = 'where';

                    if (is_array($tags)) {
                        $queryMethod = 'whereIn';
                    }

                    $orQueryMethod = 'or' . ucfirst($queryMethod);

                    $query->{$queryMethod}('tags.slug', $tags)
                        ->{$orQueryMethod}('tags.id', $tags);
                })
                ->select('products.*')->distinct();
        }

        if($request->filter)
        {
            $product->whereHas('subcategory', function ($query) use ($request) {
                $query->where('subcategories.slug', $request->input('filter'));
            });  
        }

        if(is_array($request->price)){
            $product->whereRaw('ifnull(sale_price, regular_price) >= ? and ifnull(sale_price, regular_price) <= ?',$request->price);
        }
        

        if($request->sort){
            if($request->sort == 'low_high_price')
                $product->orderByRaw('ifnull(sale_price, regular_price)', 'asc');
            
            elseif($request->sort == 'high_low_price')
                $product->addSelect(\DB::raw('products.*, IFNULL(products.sale_price, products.regular_price) as price'))->orderBy('price','desc');
                // $product->orderByRaw('ifnull(sale_price, regular_price)', 'desc'); 
        }
        else
            $product->orderBy('products.id','desc');
            
        $products = $product->paginate(9);  

        
        $categories = Category::orderBy('sort')->get();
    
        return view('frontend.products',compact('products','category','categories', 'meta_title', 'meta_description', 'meta_keywords'));
        
    }
    public function products($slug)
    {   
        $product = Product::where('active','=',true)->where('slug','=',$slug)->first();
        if(!$product) abort(404);

        $similar = Product::where('active','=',true)->where('cat_id',$product->cat_id)->where('id','!=',$product->id)->orderBy('created_at','desc')->limit(4)->get();

        $images=ProductImage::where('product_id','=',$product->id)->get();
        return view('frontend.product')->with('product',$product)->with('images',$images)->with('similar',$similar);
    }

    public function Search(Request $request)
    {
        $query=$request->search;
        $subcat=Subcategory::all();
        $products=Product::where('active','=',true)->where('product_name', 'LIKE', "%$query%")->paginate(10);
        
        return view ('frontend.search')->with('products',$products)->with('query',$query)->with('subcat',$subcat);
    }

    public function clients(Request $request)
    {
        $meta_title = 'Our Clients | Bagbox Motorcycles Trading LLC';
        $meta_description = '';
        $meta_keywords = '';
        
        if(AdditionalInfo::where('alias','clients_page_settings')->exists()){
            $meta_tags = json_decode(AdditionalInfo::where('alias','clients_page_settings')->first()->info);
            $meta_title = $meta_tags->meta_title ? : $meta_title;
            $meta_description = $meta_tags->meta_description;
            $meta_keywords = $meta_tags->meta_keywords;
        }

        $clients=Client::paginate(30);
        return view('frontend.clients',compact('clients', 'meta_title', 'meta_description', 'meta_keywords'));
    }

    public function testimonials(Request $request)
    {
        $meta_title = 'Testimonials | Bagbox Motorcycles Trading LLC';
        $meta_description = '';
        $meta_keywords = '';
        if(AdditionalInfo::where('alias','testimonials_page_settings')->exists()){
            $meta_tags = json_decode(AdditionalInfo::where('alias','testimonials_page_settings')->first()->info);
            $meta_title = $meta_tags->meta_title ? : $meta_title;
            $meta_description = $meta_tags->meta_description;
            $meta_keywords = $meta_tags->meta_keywords;
        }

        $testimonials = Testimonial::where('active',1)->paginate(10);
        return view('frontend.testimonials',compact('testimonials', 'meta_title', 'meta_description', 'meta_keywords'));
    }

    public function Feedback(Request $request)
    {  
        $this->validate(
            $request, 
            [   
                'name'          => 'required|max:255',
                'content' => 'required|max:500',
                'photo'=>'mimes:png,jpeg,bmp|max:1024'

            ],
            [   
                'name.required' => 'Name is required.',
                'name.max'      => 'Your name should be less than 255 charcaters',
               
                'content' => 'A feedback message is required',
                'photo.max'=>'File size must be less than 1 MB'
                
            ]
        );


        $testimonial = new Testimonial;
        $testimonial->name = $request->name;
        $testimonial->profession = $request->profession;
        $testimonial->content = $request->content;
        $testimonial->active = 0;
        
        if($request->hasFile('photo')){
            $photo = $request->file('photo');
            $filename = time() . '.' . $photo->getClientOriginalExtension();
            $location = public_path('landing_images/');
            $photo->move($location,$filename);
            $testimonial->photo = $filename;
        }
        $testimonial->save();
        Session::flash('success','Thank you for your valuable feedback!!!');
        return redirect("/");


    }
    
    public function Order($slug)
    {
        $product=Product::where('slug','=',$slug)->first();
        if(!$product->is_wholesale){
            Session::flash('error', $product->product_name . ' is not available for bulk ordering.');
            return redirect()->back();
        }
        $image=Productimage::where('product_id','=',$product->id)->first();
        return view('frontend.order')->with('product',$product)->with('image',$image);

    }

    public function GeneralOrder(Request $request)
    {

        $validator = Validator::make($request->order, 
                [   
                    'email' => 'nullable|email',
                    'name' => 'required|max:100',
                    'phone' => 'required|numeric',
                    'address' => 'required|max:500',    
                ],
                [ 
                    'email.email'      => 'Sorry, Your email is not a valid email',
                    'name.required' => 'Your name is required.',
                    'name.max'      => 'Your name should be less than 100 charcaters',
                    'phone.required' => 'Contact Number is required',
                    'phone.numeric' => 'Enter a valid Contact number.'
                ]
            );
        

        if($validator->fails()){
            if($request->ajax())
                return response()->json(['errors'=>$validator->errors()],422);
            else {
                return redirect()->back()->withErrors($validator)->withInput();
            }
        }
        $ordereq=$request->order;
        $product = Product::findOrFail($ordereq['product_id']);
        if(!$product->is_wholesale){
            Session::flash('error', $product->product_name . ' is not available for bulk ordering.');
            return redirect()->back()->withInput();
        }

        $order= new Quotation;
        $order->name= $ordereq['name'];
        $order->product_name= $product->product_name;
        $order->price= $product->price;
        $order->email=$ordereq['email'];
        $order->phone=$ordereq['phone'];
        $order->address=$ordereq['address'];
        $order->message=$ordereq['message'];
        $order->quantity=$ordereq['quantity'];
        $order->seen=false;
        
        $order->save();

        if($request->ajax()){
            return 'success';
        }else{
            Session::flash('success','Your order is received. We will get back to you within 24 hours.');
            return redirect("/shop");
        }
        
    }
    public function NewsLetter(Request $request)
    {
        $subscriber=new Subscriber;
        $subscriber->email=$request->email;
        $subscriber->token=$request->_token;
        $subscriber->save();
        Session::flash('success','Thank you for joining our newsletter!');
        return redirect("/");

    }
    public function privacyPolicy()
    {
        $privacyPolicy=json_decode(AdditionalInfo::where('alias','privacy_policy')->first()->info);
        return view("frontend.privacy",compact("privacyPolicy"));
    }
    public function termsAndConditions()
    {
        $terms_and_conditions=json_decode(AdditionalInfo::where('alias','terms_and_conditions')->first()->info);
        return view("frontend.terms",compact("terms_and_conditions"));
    }
    public function about()
    {
        $about = json_decode(AdditionalInfo::where('alias','about')->first()->info);
        return view("frontend.about",compact("about"));
    }
    public function inquiry()
    {
        return view("frontend.inquiry");
    }
    public function inquirySubmit(Request $request)
    {
        $validator = Validator::make($request->order, 
                [   
                    'email' => 'nullable|email',
                    'name' => 'required|max:100',
                    'phone' => 'required|numeric',
                    'address' => 'required|max:500',    
                ],
                [ 
                    'email.email'      => 'Sorry, Your email is not a valid email',
                    'name.required' => 'Your name is required.',
                    'name.max'      => 'Your name should be less than 100 charcaters',
                    'phone.required' => 'Contact Number is required',
                    'phone.numeric' => 'Enter a valid Contact number.'
                ]
            );
        

        if($validator->fails()){
            if($request->ajax())
                return response()->json(['errors'=>$validator->errors()],422);
            else {
                return redirect()->back()->withErrors($validator)->withInput();
            }
        }
        $ordereq=$request->order;
       

        $order= new Quotation;
        $order->name= $ordereq['name'];
        $order->product_name= "General Inquiry";
        $order->price= 0;
        $order->email=$ordereq['email'];
        $order->phone=$ordereq['phone'];
        $order->address=$ordereq['address'];
        $order->message=$ordereq['message'];
        $order->quantity=0;
        $order->seen=false;
        
        $order->save();

        if($request->ajax()){
            return 'success';
        }else{
            Session::flash('success','Your inquiry is received. We will get back to you within 24 hours.');
            return redirect("/");
        }
    }

}
