<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tag;

class TagController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
    }

    public function index()
    {
        $tags = Tag::orderBy('updated_at','desc')->paginate(10);
        
        return view('admin.tags.index')->with('tags',$tags);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([         
            'name' => 'required|unique:tags,name|max:191'
        ]);
        
        $tag = new Tag;
        $tag->name = $request->name;
        $tag->save();

        $request->session()->flash('success', 'Tag added.');        
        
        return redirect()->route('tags.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([         
            'name' => 'required|max:191|unique:tags,name,' . $id
        ]);

        $tag = Tag::findOrFail($id);
        $tag->name = $request->name;

        $tag->save();

        $request->session()->flash('success', 'Tag updated.');        
        
        return redirect()->route('tags.index');
    }

    public function destroy(Request $request, $id)
    {
        $tag = Tag::findOrFail($id);
            
        $tag->delete();

        $request->session()->flash('success', 'Tag deleted.');        
        
       return redirect()->back();
    }
}
