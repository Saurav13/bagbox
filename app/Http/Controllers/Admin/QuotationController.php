<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exports\QuotationsExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Quotation;

class QuotationController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
    }
    
    public function index()
    {
        $quotations=Quotation::where('seen','0')->orderBy('id','desc')->paginate(10);
        return view('admin.quotations.index')->with('quotations',$quotations);
    }

    public function export() 
    {
        return Excel::download(new QuotationsExport, 'quotations.xlsx');
    }

    public function getUnseenOrderCount(Request $request){
        
        if ($request->ajax()){
            $quotationcount = Quotation::where('seen','0')->count();
            return response()->json(['quotationcount' => $quotationcount]);
        }
        abort(404);  
    }

    public function getUnseenOrder(Request $request){
        if ($request->ajax()){
            $quotations = Quotation::where('seen','0')->latest()->limit(5)->get();
            $quotationcount = Quotation::where('seen','0')->count();
            $view = view('admin.partials.rnotis')->with('orders',$quotations)->with('ordercount',$quotationcount)->render(); 
            return response()->json(['html' => $view, 'ordercount' => $quotationcount]);
        }
        abort(404);
    }

    public function MarkSeen(Request $request)
    {
        if($request->ajax()){
            $quotation=Quotation::where('id','=',$request->id)->first();
            $quotation->seen=1;
            $quotation->save();
            return "Marked as seen";
        }
        abort(404);
    }

    public function allQuotations()
    {
       
        $quotations=Quotation::orderBy('created_at','desc')->paginate(10);
        return view('admin.quotations.allquotations')->with('quotations',$quotations); 
    }

}
