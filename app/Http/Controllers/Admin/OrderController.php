<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\OrderStatusUpdateMail;
use App\Order;
use App\Exports\OrdersExport;
use Maatwebsite\Excel\Facades\Excel;
use Mail;

class OrderController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
    }
    
    public function index()
    {
        $orders=Order::where('status','!=','pending')->where('seen','0')->orderBy('ordered_at','desc')->paginate(10);
        return view('admin.orders.index')->with('orders',$orders);
    }

    public function export() 
    {
        return Excel::download(new OrdersExport, 'orders.xlsx');
    }

    public function getUnseenOrderCount(Request $request){
        
        if ($request->ajax()){
            $ordercount = Order::where('seen','0')
                        ->where(function ($query) {
                            $query->where('status','processing')->orWhere('status','complete');
                        })->count();
            return response()->json(['ordercount' => $ordercount]);
        }
        abort(404);  
    }

    public function getUnseenOrder(Request $request){
        if ($request->ajax()){
            $orders = Order::where('seen','0')
                    ->where(function ($query) {
                        $query->where('status','processing')->orWhere('status','complete');
                    })->latest()->limit(5)->get();
            $ordercount = Order::where('seen','0')
                        ->where(function ($query) {
                            $query->where('status','processing')->orWhere('status','complete');
                        })->count();

            $view = view('admin.partials.onotis')->with('orders',$orders)->with('ordercount',$ordercount)->render(); 
            return response()->json(['html' => $view, 'ordercount' => $ordercount]);
        }
        abort(404);
    }

    public function MarkSeen(Request $request)
    {
        if($request->ajax()){
            $order=Order::where('id','=',$request->id)->first();
            $order->seen=1;
            $order->save();
            return "Marked as seen";
        }
        abort(404);
    }

    public function allOrders()
    {
       
        $orders=Order::where('status','!=','pending')->orderBy('ordered_at','desc')->paginate(10);
        return view('admin.orders.allorders')->with('orders',$orders); 
    }

    public function update(Request $request, $id)
    {
        $order = Order::where('status','!=','pending')->findOrFail($id);
        $order->status = $request->status;

        if($request->status == 'delivered') $order->paid = 1;

        $order->save();

        Mail::send(new OrderStatusUpdateMail($order));
        
        $request->session()->flash('success', 'Order has been updated.');
        return redirect()->back();
    }

}
