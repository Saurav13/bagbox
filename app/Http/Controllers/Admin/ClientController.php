<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Client;
use Image;
use Session;

class ClientController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
    }
    public function index()
    {
        $clients=Client::orderBy('id','desc')->paginate(18);
        return view('admin.clients.index')->with('clients',$clients);
    }

    public function StoreCat(Request $request)
    {
        // dd($request->all());
        $this->validate($request, array(
            'name'=>'required|max:255',
            'image'=>'required|mimes:jpg,jpeg,png,bmp|max:20000'

        ));
        $client=new Client;
        $client->name=$request->name;
        $client->link=$request->link;
        if($request->hasFile('image'))
        {
            $file=$request->file('image');
            $filename = time().rand(111,999). '.' . $file->getClientOriginalExtension();
            $path = public_path('client-images/'.$filename);
            Image::make($file)->save($path);
            $client->image = $filename;
        }
        
        $client->save();
        Session::flash("success","Client Added Successfully.");
        return redirect('admin/manage/clients');


    }

    public function DestroyCat(Request $request)
    {
        $this->validate($request, array(
            'id'=>'required',
        ));
        $client=Client::findorFail($request->id);
        $client->delete();
    }

    public function EditCat(Request $request)
    {
        $this->validate($request, array(
            'id'=>'required',
            'name'=>'required|max:255',
        ));
        $client=Client::findOrFail($request->id);
        $client->name=$request->name;
        $client->link=$request->link;

        if($request->hasFile('image'))
        {
            $file=$request->file('image');
            $filename = time().rand(111,999). '.' . $file->getClientOriginalExtension();
            $path = public_path('client-images/'.$filename);
            Image::make($file)->save($path);
            $client->image = $filename;
        }
        $client->save();
        Session::flash("success","Client Edited Successfully.");
        return redirect('admin/manage/clients');
    }

    public function FeatureClient(Request $request)
    {
        $client=Client::findorFail($request->id);
        if($client->featured == true)
        {
            $client->featured=false;
            $client->save();
            return "unfeatured";
        }
        else{
            $client->featured=true;
            $client->save();
            return "featured";
        }
    }

}
