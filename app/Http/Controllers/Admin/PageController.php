<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AdditionalInfo;
use App\Rules\NoEmptyContent;
use Session;
use Validator;

class PageController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
    }

    public function index(Request $request){
        if($request->query('active'))
            $active = $request->query('active');
        else
            $active = 'about';
            
        $about = json_decode(AdditionalInfo::where('alias','about')->first()->info);
        $privacy_policy = json_decode(AdditionalInfo::where('alias','privacy_policy')->first()->info);
        $terms_and_conditions = json_decode(AdditionalInfo::where('alias','terms_and_conditions')->first()->info);

        return view('admin.pages')->with('terms_and_conditions',$terms_and_conditions)->with('privacy_policy',$privacy_policy)->with('about',$about)->with('active',$active);
    }

    public function update($page,Request $request){

        $validator = Validator::make($request->all(), [
            'image'=>'nullable|mimes:jpeg,bmp,png',            
            'content' => ['required', new NoEmptyContent],            
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator,$page)->withInput();
        }

        $about = AdditionalInfo::where('alias',$page)->firstOrFail();
        $info = json_decode($about->info);
        $info->content = $request->content;
        
        $info->meta_title = $request->meta_title;
        $info->meta_description = $request->meta_description;
        $info->meta_keywords = $request->meta_keywords;

        if($request->hasFile('image')){
            if($info->photo!="")
                unlink(public_path('landing_images/'.$info->photo));
            $image = $request->file('image');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('landing_images/');
            $image->move($location,$filename);
            $info->photo = $filename;
        }

        $about->info = json_encode($info);
        $about->save();

        Session::flash('success', 'Content updated.');        
        
        return redirect()->route('pages.index',array('active'=>$page));
    }
}
