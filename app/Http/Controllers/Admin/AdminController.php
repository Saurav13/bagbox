<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Quotation;
use App\Order;

class AdminController extends Controller
{ 
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $quotations=Quotation::where('seen','=',false)->orderBy('id','desc')->paginate(10,['*'], 'quotation');
        // $orderCount = Order::where('status','!=','pending')->count();
        $orders = Order::where('status','!=','pending')->where('seen','0')->orderBy('ordered_at','desc')->paginate(10,['*'], 'order');

        return view('admin.dashboard',compact('quotations','orders'));   
    }

    public function DeleteQuotation(Request $request)
    {
        $quotation=Quotation::findorFail($request->id);
        $quotation->delete();
    }

    public function ShowQuotation($id)
    {
        $quotation=Quotation::findorFail($id);
        if(!$quotation->seen){
            $quotation->seen=1;
            $quotation->save();
        }
        return view('admin.quotations.view')->with('quotation',$quotation);
    }

    public function DeleteOrder(Request $request)
    {
        $order=Order::findorFail($request->id);
        $order->delete();
        return redirect()->to('/admin/orders');

    }
    
    public function ShowOrder($id)
    {
        $order=Order::findorFail($id);

        if(!$order->seen){
            $order->seen=1;
            $order->save();
        }
        return view('admin.orders.view')->with('order',$order);
    }

}
