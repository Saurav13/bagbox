<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Subcategory;
use App\Product;
use App\ProductImage;
use App\Tag;
use App\Brand;
use Response;
use Image;
use Session;

class ProductController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
    }

    public function index()
    {
        $categories=Category::all();
        $products=Product::all();
        $images=ProductImage::all();
        return view('admin.products.index')->with('categories',$categories)->with('products',$products)->with('images',$images);
    }

    public function create()
    {
        $categories=Category::all();
        $tags = Tag::all();
        return view('admin.products.create')->with('categories',$categories)->with('tags',$tags);
    }

    public function getSubCat($id)
    {
        $subcategories=Subcategory::where('cat_id',$id)->get();
        return Response::json($subcategories);
    }

    public function store(Request $request)
    {
        $this->validate($request, array(
            'product_name'=>'required|max:255',
            'subtitle'=>'required|max:255',
            'product_description'=>'required',
            'regular_price'=>'required|integer|min:0',
            'sale_price'=>'nullable|integer|min:1',
            'product_images.*' => 'required|mimes:jpg,jpeg,png,bmp|max:20000',
            'feature'=>'required',
            'active'=>'required',
            'product_category'=>'required|exists:categories,id',
            'quantity'=>'nullable|integer|min:1',
            'type' => 'required|in:Retail,Wholesale,Both',
            'in_landing_slider'=>'required'
        ));
        $product= new Product;
        $product->product_name=$request->product_name;
        $product->product_description=$request->product_description;
        $product->regular_price = $request->regular_price;
        $product->sale_price = $request->sale_price;
        $product->subtitle=$request->subtitle;
        $product->featured=$request->feature;
        $product->active=$request->active;
        $product->video_link=$request->video_link;
        $product->quantity = $request->quantity;
        $product->type = $request->type;
        $product->in_landing_slider = $request->in_landing_slider;
        $product->links = $request->links;
        //Removed empty values from json object of sizes
    
        $product->cat_id = $request->product_category;
        
        $product->meta_title = $request->meta_title;
        $product->meta_description = $request->meta_description;
        $product->meta_keywords = $request->meta_keywords;

        $product->save();

        if($request->hasFile('pdf'))
        {
           
            $file = $request->file('pdf');
            $filename = time().rand(111,999). '.' . $file->getClientOriginalExtension();
            $path = public_path('product-files/');
            $file->move($path, $filename);
            $product->pdf=$filename;
        }
        
        if($request->hasFile('product_images'))
        {
            $photos = $request->file('product_images');
            foreach($photos as $file)
            {
                $product_image=new ProductImage;
                $filename = time().rand(111,999). '.' . $file->getClientOriginalExtension();
                $path = public_path('product-images/'.$filename);
                Image::make($file)->save($path);
                $product_image->image = $filename;
                $product_image->product_id=$product->id;
                $product_image->save();
            }
        }

        $tags = $this->getTags($request);

        $product->tags()->sync($tags);

        $product->save();
        Session::flash('success', 'Product was added');
        return redirect('admin/products');
    }

    public function FilterProduct($cat_id)
    {
        $categories=Category::all();
        $products=Product::where('cat_id','=',$cat_id)->get();
        if(count($products) != null)
        {
            $images=collect([]);
            foreach($products as $p){
                $image=ProductImage::where('product_id','=',$p->id)->get();
                $images=$images->merge($image);   
            }
        }
        if(count($products) != null)
        {
            return view('admin.products.index')->with('categories',$categories)->with('products',$products)->with('images',$images);
        }
        else
        {
            return view('admin.products.index')->with('categories',$categories)->with('products',$products);
        }
    }

    public function destroy(Request $request,$id)
    {
        $product=Product::findorFail($id);
        $images=ProductImage::where('product_id','=',$product->id)->get();

        foreach($images as $i)
        {
            unlink('product-images/'.$i->image);
        }
        $product->delete();
        return redirect('admin/products');
    }

    public function edit($id)
    {
        $categories=Category::all();
        $product=Product::findorFail($id);
        $images=ProductImage::where('product_id','=',$id)->get();
        $tags = Tag::all();
    
        return view('admin.products.edit')->with('categories',$categories)->with('product',$product)->with('images',$images)->with('tags',$tags);
    }

    public function update(Request $request,$id)
    {
        $this->validate($request, array(
            'product_name'=>'required|max:255',
            'subtitle'=>'required|max:255',
            'product_description'=>'required',
            'regular_price'=>'required|integer|min:0',
            'sale_price'=>'nullable|integer|min:1',
            'quantity'=>'nullable|integer|min:1',
            'type' => 'required|in:Retail,Wholesale,Both',
            'in_landing_slider'=>'required',
            'product_images.*' => 'mimes:jpg,jpeg,png,bmp|max:20000',
            'feature'=>'required',
            'active'=>'required',
            'product_category'=>'required|exists:categories,id'
        ));
        $product= Product::findOrFail($id);

        $product->product_name=$request->product_name;
        $product->product_description=$request->product_description;
        $product->regular_price = $request->regular_price;
        $product->sale_price = $request->sale_price;
        $product->quantity = $request->quantity;
        $product->type = $request->type;
        $product->in_landing_slider = $request->in_landing_slider;
        $product->featured=$request->feature;
        $product->subtitle=$request->subtitle;
        $product->active=$request->active;
        $product->cat_id=$request->product_category;
        $product->video_link=$request->video_link;
        $product->links = $request->links;

        $product->meta_title = $request->meta_title;
        $product->meta_description = $request->meta_description;
        $product->meta_keywords = $request->meta_keywords;
        
        if($request->hasFile('pdf'))
        {
           
            $file = $request->file('pdf');
            $filename = time().rand(111,999). '.' . $file->getClientOriginalExtension();
            $path = public_path('product-files/');
            $file->move($path, $filename);
            $product->pdf=$filename;
        }
        $product->save();

        if($request->hasFile('product_images'))
        {
            $photos = $request->file('product_images');
            foreach($photos as $file)
            {
                $product_image=new ProductImage;
                $filename = time().rand(111,999). '.' . $file->getClientOriginalExtension();
                $path = public_path('product-images/'.$filename);
                Image::make($file)->save($path);
                $product_image->image = $filename;
                $product_image->product_id=$product->id;
                $product_image->save();
            }
        }

        $tags = $this->getTags($request);

        $product->tags()->sync($tags);

        Session::flash('success', 'Product was updated');
        return redirect('admin/products');
    }

    public function RemoveProductImage(Request $request)
    {
        $image=ProductImage::findorFail($request->id);
        unlink('product-images'.'/'.$image->image);
        $image->delete();
    }

    public function FeatureProduct(Request $request)
    {
        $product=Product::findorFail($request->id);
        if($product->featured == true)
        {
            $product->featured=false;
            $product->save();
            return "unfeatured";
        }
        else{
            $product->featured=true;
            $product->save();
            return "featured";
        }
    }

    public function ShowInSliderProduct(Request $request)
    {
        $product=Product::findorFail($request->id);
        if($product->in_landing_slider == true)
        {
            $product->in_landing_slider=false;
            $product->save();
            return "unfeatured";
        }
        else{
            $product->in_landing_slider=true;
            $product->save();
            return "featured";
        }
    }

    public function ActivateProduct(Request $request)
    {
        $product=Product::findorFail($request->id);
        if($product->active == true)
        {
            $product->active=false;
            $product->save();
            return "Inactive";
        }
        else{
            $product->active=true;
            $product->save();
            return "Active";
        }
    }

    private function getTags($request)
    {
        $tags = [];

        $requestTags = $request->get('tags', []);

        foreach ($requestTags as $tag) {
            if (is_numeric($tag)) {
                array_push($tags, $tag);
            } else {
                $newTag = new Tag;
                $newTag->name = $tag;
                $newTag->save();

                array_push($tags, $newTag->id);
            }
        }

        return $tags;
    }
}
