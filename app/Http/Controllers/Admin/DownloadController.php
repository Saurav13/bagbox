<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Download;
use Session;

class DownloadController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
    }
    public function index()
    {
        $downloads=Download::orderBy('id','desc')->paginate(18);
        return view('admin.downloads.index')->with('downloads',$downloads);
    }

    public function store(Request $request)
    {
        $this->validate($request, array(
            'title'=>'required|max:255',
            'file'=>'required|max:20000'
        ));
        $download=new Download;
        $download->title=$request->title;
        
        if($request->hasFile('file'))
        {
            $file=$request->file('file');
            $filename = time().rand(111,999). '.' . $file->getClientOriginalExtension();
            $path = public_path('download-files/');
            $file->move($path,$filename);
            $download->file = $filename;
        }
        
        $download->save();
        Session::flash("success","Download Added Successfully.");
        return redirect('admin/downloads');
    }

    public function destroy(Request $request)
    {
        $this->validate($request, array(
            'id'=>'required',
        ));
        $download=Download::findorFail($request->id);
        if($download->file && file_exists(public_path('download-files/'.$download->file)))
            unlink(public_path('download-files/'.$download->file));

        $download->delete();
    }

    public function update(Request $request)
    {
        $this->validate($request, array(
            'id'=>'required',
            'title'=>'required|max:255',
        ));
        $download=Download::findOrFail($request->id);
        $download->title=$request->title;
    
        if($request->hasFile('file'))
        {
            $file=$request->file('file');
            $filename = time().rand(111,999). '.' . $file->getClientOriginalExtension();
            $path = public_path('download-files/');
            $file->move($path,$filename);
            $download->file = $filename;
        }
        $download->save();
        Session::flash("success","Download Edited Successfully.");
        return redirect('admin/downloads');
    }

}
