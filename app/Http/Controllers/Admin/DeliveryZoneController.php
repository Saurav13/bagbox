<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DeliveryZone;
use Session;

class DeliveryZoneController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
    }
    public function index()
    {
        $delivery_zones = DeliveryZone::orderBy('id','desc')->paginate(18);
        return view('admin.delivery_zones.index')->with('delivery_zones',$delivery_zones);
    }

    public function store(Request $request)
    {
        $this->validate($request, array(
            'name'=>'required|max:255',
            'delivery_charge'=>'required|numeric|min:0'
        ));
        $delivery_zone=new DeliveryZone;
        $delivery_zone->name=$request->name;
        $delivery_zone->delivery_charge=$request->delivery_charge;
        
        $delivery_zone->save();
        Session::flash("success","Delivery Zone Added Successfully.");
        return redirect('admin/deliveryzones');
    }

    public function destroy(Request $request)
    {
        $this->validate($request, array(
            'id'=>'required',
        ));
        $delivery_zone = DeliveryZone::findorFail($request->id);
        $delivery_zone->delete();
    }

    public function update(Request $request)
    {
        $this->validate($request, array(
            'id'=>'required',
            'name'=>'required|max:255',
            'delivery_charge'=>'required|numeric|min:0'
        ));
        $delivery_zone=DeliveryZone::findOrFail($request->id);
        $delivery_zone->name=$request->name;
        $delivery_zone->delivery_charge=$request->delivery_charge;
        $delivery_zone->save();

        Session::flash("success","Delivery Zone Edited Successfully.");
        return redirect('admin/deliveryzones');
    }

}
