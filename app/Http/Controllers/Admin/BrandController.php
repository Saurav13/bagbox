<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Brand;

class BrandController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
    }

    public function index()
    {
        $brands = Brand::orderBy('updated_at','desc')->paginate(10);
        
        return view('admin.brands.index')->with('brands',$brands);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([         
            'name' => 'required|max:191',
            'logo' => 'nullable|image',
        ]);
        
        $brand = new Brand;
        $brand->name = $request->name;

        if($request->hasFile('logo')){
            $photo = $request->file('logo');
            $filename = time() . '.' . $photo->getClientOriginalExtension();
            $location = public_path('brand_logos/');
            $photo->move($location,$filename);
            $brand->logo = $filename;
        }

        $brand->save();

        $request->session()->flash('success', 'Brand added.');        
        
        return redirect()->route('brands.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brand = Brand::findOrFail($id);

        return view('admin.brands.edit',compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([         
            'name' => 'required|max:191',
            'logo' => 'nullable|image',
        ]);

        $brand = Brand::findOrFail($id);
        $brand->name = $request->name;     

        if($request->hasFile('logo')){

            if($brand->logo)
                unlink(public_path('brand_logos/'.$brand->logo));

            $photo = $request->file('logo');
            $filename = time() . '.' . $photo->getClientOriginalExtension();
            $location = public_path('brand_logos/');
            $photo->move($location,$filename);
            $brand->logo = $filename;
        }

        $brand->save();

        $request->session()->flash('success', 'Brand updated.');        
        
        return redirect()->route('brands.index');
    }

    public function destroy(Request $request, $id)
    {
        $brand = Brand::findOrFail($id);

        if($brand->file)
            unlink(public_path('brand_logos/'.$brand->file));
            
        $brand->delete();

        $request->session()->flash('success', 'Brand deleted.');        
        
       return redirect()->back();
    }
}
