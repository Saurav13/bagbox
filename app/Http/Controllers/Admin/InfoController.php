<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Info;
use Session;

class InfoController extends Controller
{
    public function index()
    {
        $infos=Info::all();
        return view('admin.infos.index',compact('infos'));
    }
    public function EditInfo(Request $request)
    {
        $this->validate($request, array(
            'id'=>'required',
            'title'=>'required|max:255',
            'description'=>'required|max:255',

        ));
        $info=Info::find($request->id);
        $info->title=$request->title;
        $info->description=$request->description;

        if($request->hasFile('image')){
            $photo = $request->file('image');
            $filename = time() . '.' . $photo->getClientOriginalExtension();
            $location = public_path('info_images/');
            $photo->move($location,$filename);
            $info->image = $filename;
        }
        $info->save();
        $request->session()->flash('success', 'Info Edited Successfully');        
        
        return redirect()->route('infos.index');
    }
}
