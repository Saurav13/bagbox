<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $fillable = ['product_name','quantity','amount','type','product_id'];

    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
