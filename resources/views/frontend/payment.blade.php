@extends('layouts.main')

@section('title','Checkout')

@section('css')
    <style>
        #overlay {
            position: fixed; /* Sit on top of the page content */
            display: none;
            width: 100%; /* Full width (cover the whole page) */
            height: 100%; /* Full height (cover the whole page) */
            top: 0; 
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgba(0,0,0,0.5); /* Black background with opacity */
            z-index: 102; /* Specify a stack order in case you're using a different order for other elements */
        }
        #text{
            position: absolute;
            top: 50%;
            left: 50%;
            font-size: 50px;
            color: white;
            transform: translate(-50%,-50%);
            -ms-transform: translate(-50%,-50%);
        }
    </style>
@endsection

@section('body')
    <div id="overlay">
        <div id="text"><img  src="/img/preloader-black.svg"/></div>
    </div>

  <!-- Breadcrumbs -->
  <section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
      <div class="container">
          <ul class="u-list-inline">
              <li class="list-inline-item g-mr-5">
              <a class="u-link-v5 g-color-text" href="{{URL::to('home')}}">Home</a>
              <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
              </li>
              <li class="list-inline-item g-color-primary">
              <span>Payment</span>
              </li>
          </ul>
      </div>
  </section>
  <!-- End Breadcrumbs -->

      <!-- Checkout Form -->
    <div class="container g-pt-40 g-pb-70">
        <div class="g-mb-60">
            <!-- Step Titles -->
            <ul id="stepFormProgress" class="js-step-progress row justify-content-center list-inline text-center g-font-size-17 mb-0">
                <li class="col-3 list-inline-item g-mb-20 g-mb-0--sm g-checked">
                    <span class="d-block u-icon-v2 u-icon-size--sm g-rounded-50x g-brd-gray-light-v2 g-color-gray-dark-v5 g-brd-primary--active g-color-white--parent-active g-bg-primary--active g-color-white--checked g-bg-primary--checked mx-auto mb-3">
                        <i class="g-font-style-normal g-font-weight-700 g-hide-check">1</i>
                        <i class="fa fa-check g-show-check"></i>
                    </span>
                    <h4 class="g-font-size-16 text-uppercase mb-0">Shopping Cart</h4>
                </li>

                <li class="col-3 list-inline-item g-mb-20 g-mb-0--sm g-checked">
                    <span class="d-block u-icon-v2 u-icon-size--sm g-rounded-50x g-brd-gray-light-v2 g-color-gray-dark-v5 g-brd-primary--active g-color-white--parent-active g-bg-primary--active g-color-white--checked g-bg-primary--checked mx-auto mb-3">
                        <i class="g-font-style-normal g-font-weight-700 g-hide-check">2</i>
                        <i class="fa fa-check g-show-check"></i>
                    </span>
                    <h4 class="g-font-size-16 text-uppercase mb-0">Shipping</h4>
                </li>

                <li class="col-3 list-inline-item active">
                    <span class="d-block u-icon-v2 u-icon-size--sm g-rounded-50x g-brd-primary g-color-primary g-color-white--parent-active g-bg-primary--active g-color-white--checked g-bg-primary--checked mx-auto mb-3">
                        <i class="g-font-style-normal g-font-weight-700 g-hide-check">3</i>
                        <i class="fa fa-check g-show-check"></i>
                    </span>
                    <h4 class="g-font-size-16 text-uppercase mb-0">Payment &amp; Review</h4>
                </li>
            </ul>
            <!-- End Step Titles -->
        </div>

        <div id="stepFormSteps">
            <!-- Shopping Cart -->
            <div id="step1" >
              
            </div>

            <!-- Shipping -->
            <div id="step2">
              
            </div>
            <!-- End Shipping -->

            <!-- Payment & Review -->
            <div id="step3" class="active">
                <h3 style="font-size: 20px;">Please Select a Payment Method:</h3>
                <div class="row">
                    <div class="col-md-8 g-mb-30">
                        <!-- Payment Methods -->
                        <ul class="list-unstyled mb-5">
                            
                            <li class="my-3">
                                <label class="form-check-inline u-check  u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30 mr-5">
                                    <div id="paypal-button"></div>
                                </label>

                                {{-- <label class="form-check-inline u-check  u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30 ml-5">
                                    <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="payment_method" type="radio" value="cod">
                                    <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                    <i class="fa" data-check-icon="&#xf00c"></i>
                                    </span>
                                    Pay with Payoneer
                                </label> --}}
                            </li>
                        </ul>
                        <!-- End Payment Methods -->

                        <div id="esewaDiv" hidden>
                            <form action="{{ route('payment',$order->code) }}" method="POST">
                                @csrf
                                <input type="hidden" name="payment_method" value="esewa" >
                                <p>Pay with Esewa</p>
                                <div class="g-brd-bottom g-brd-gray-light-v3 g-pb-30 g-mb-30">
                                    <div >
                                        <button class="btn u-btn-primary g-font-size-13 text-uppercase g-px-40 g-py-15" type="submit">Pay</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div id="codDiv" hidden>
                            <form action="{{ route('payment',$order->code) }}" method="POST">
                                @csrf
                                <input type="hidden" name="payment_method" value="COD" >
                                <p>Pay with Cash on Delivery</p>
                                <div class="g-brd-bottom g-brd-gray-light-v3 g-pb-30 g-mb-30">
                                    <div >
                                        <button class="btn u-btn-primary g-font-size-13 text-uppercase g-px-40 g-py-15" type="submit">Confirm Order</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="col-md-4 g-mb-30">
                        <!-- Order Summary -->
                        <div class="g-bg-gray-light-v5 g-pa-20 g-pb-50 mb-4">
                            <div class="g-brd-bottom g-brd-gray-light-v3 g-mb-15">
                                <h4 class="h6 text-uppercase mb-3">Order summary ({{ $order->items()->where('type','Product')->count() }} items)</h4>
                            </div>
                            @php
                                $shipping = $order->items()->where('type','Shipping')->sum('amount');
                            @endphp
                            <div class="d-flex justify-content-between mb-3">
                                <span class="g-color-black">Cart Subtotal</span>
                                <span class="g-color-black g-font-weight-300">AED  {{ number_format($order->amount - $shipping) }}</span>
                            </div>
                            <div class="mb-3">
                                <div class="d-flex justify-content-between mb-1">
                                    <span class="g-color-black">Delivery</span>
                                    <span class="g-color-black g-font-weight-300">AED  {{ number_format($shipping) }}</span>
                                </div>
                            </div>
                            <div class="d-flex justify-content-between mb-3">
                                <span class="g-color-black">Order Total</span>
                                <span class="g-color-black g-font-weight-300">AED  {{ number_format($order->amount) }}</span>
                            </div>
                        </div>
                        <!-- End Order Summary -->

                        <!-- Ship To -->
                        <div class="g-px-20 mb-5">
                            <div class="d-flex justify-content-between g-brd-bottom g-brd-gray-light-v3 g-mb-15">
                                <h4 class="h6 text-uppercase mb-3">Ship to</h4>
                                {{-- <span class="g-color-gray-dark-v4 g-color-black--hover g-cursor-pointer">
                                    <i class="fa fa-pencil"></i>
                                </span> --}}
                            </div>
                            @php
                                $billing = json_decode($order->billing);
                            @endphp
                            <ul class="list-unstyled g-color-gray-dark-v4 g-font-size-15">
                                <li class="g-my-3">{{ $billing->name }}</li>
                                <li class="g-my-3">{{ $billing->email }}</li>
                                <li class="g-my-3">{{ $billing->address }}</li>
                                <li class="g-my-3">{{ $billing->city }}</li>
                                <li class="g-my-3">{{ $billing->phone }}</li>
                                @if($billing->company)
                                    <li class="g-my-3">{{ $billing->company }}</li>
                                @endif
                            </ul>
                        </div>
                        <!-- End Ship To -->

                    </div>
                </div>
            </div>
            <!-- End Payment & Review -->
        </div>
    </div>
      <!-- End Checkout Form -->

      <!-- Call to Action -->
    <div class="g-bg-primary">
        <div class="container g-py-20">
          <div class="row justify-content-center">
            <div class="col-md-4 mx-auto g-py-20">
              <!-- Media -->
              <div class="media g-px-50--lg">
                <i class="d-flex g-color-white g-font-size-40 g-pos-rel g-top-3 mr-4 icon-real-estate-048 u-line-icon-pro"></i>
                <div class="media-body">
                  <span class="d-block g-color-white g-font-weight-500 g-font-size-17 text-uppercase">Free Shipping</span>
                  <span class="d-block g-color-white-opacity-0_8">In 2-3 Days</span>
                </div>
              </div>
              <!-- End Media -->
            </div>

            <div class="col-md-4 mx-auto g-brd-x--md g-brd-white-opacity-0_3 g-py-20">
              <!-- Media -->
              <div class="media g-px-50--lg">
                <i class="d-flex g-color-white g-font-size-40 g-pos-rel g-top-3 mr-4 icon-real-estate-040 u-line-icon-pro"></i>
                <div class="media-body">
                  <span class="d-block g-color-white g-font-weight-500 g-font-size-17 text-uppercase">Free Returns</span>
                  <span class="d-block g-color-white-opacity-0_8">No Questions Asked</span>
                </div>
              </div>
              <!-- End Media -->
            </div>

            <div class="col-md-4 mx-auto g-py-20">
              <!-- Media -->
              <div class="media g-px-50--lg">
                <i class="d-flex g-color-white g-font-size-40 g-pos-rel g-top-3 mr-4 icon-hotel-restaurant-062 u-line-icon-pro"></i>
                <div class="media-body text-left">
                  <span class="d-block g-color-white g-font-weight-500 g-font-size-17 text-uppercase">Free 24</span>
                  <span class="d-block g-color-white-opacity-0_8">Days Storage</span>
                </div>
              </div>
              <!-- End Media -->
            </div>
          </div>
        </div>
    </div>
@endsection


@section('js')
<script src="https://www.paypal.com/sdk/js?client-id={{ env('PAYPAL_CLIENT_ID') }}&currency=USD"></script>
<script>
    $(document).ready(function(){
        $("input[name='payment_method']").change(function(){
            
            var method = $(this).val();

            if(method == 'esewa'){
                $('#codDiv').attr('hidden','hidden');            
                $('#esewaDiv').removeAttr('hidden');
            }
            else if(method == 'cod'){
                $('#esewaDiv').attr('hidden','hidden');
                $('#codDiv').removeAttr('hidden');            
            }
        });

        loadPaypalButton();
        
        function loadPaypalButton() {

            paypal.Buttons({

                env: "{{ env('PAYPAL_MODE') }}",
                
                style: {
                    size: 'small',
                    color: 'black',
                    shape: 'rect',
                    label: 'pay'
                },

                createOrder: function() {
                    $('#overlay').show();
                    
                    return fetch("{{ url('/payment/paypal') }}", {
                        method: 'post',
                        body: JSON.stringify({order_uid: "{{ $order->id }}",_token:"{{ csrf_token() }}"}),
                        headers: {
                            'content-type': 'application/json',
                            'accept': 'application/json'
                        }
                    }).then(function(res) {
                        $('#overlay').hide();
                        
                        return Promise.all([res.status, res.json()]);
                        
                    }).then(function([status, data]) {

                        if(status == 200 || status == 201)
                            return data.result.id;
                        else if(status != 201 && data.message)
                            themeNotify('error',data.message);
                        else
                            themeNotify('error','Something went wrong');
                    }).catch(function(err) {
                        themeNotify('error','Something went wrong');
                    });
                },

                onApprove: function(data) {
                    $('#overlay').show();
                    return fetch("{{ url('payment/paypal/return') }}", {
                        method: 'post',
                        headers: {
                            'content-type': 'application/json',
                            'accept': 'application/json'
                        },
                        body: JSON.stringify({
                            orderID: data.orderID,
                            order_uid: "{{ $order->id }}",
                            _token:"{{ csrf_token() }}"
                        })
                    }).then(function(res) {
                        $('#overlay').hide();
                        return Promise.all([res.status, res.json()]);
                    }).then(function([status, data]) {
                        
                        if(status == 200 || status == 201){
                            location.href = '/';
                        }
                        else if(status != 201 && data.message)
                            themeNotify('error',data.message);
                        else
                            themeNotify('error','Something went wrong');
                        
                    }).catch(function(err) {
                        themeNotify('error','Something went wrong');
                    });
                }
                
            }).render('#paypal-button');
        }
    })
</script>

@endsection