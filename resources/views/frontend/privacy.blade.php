@extends('layouts.main')

@section('title', $privacyPolicy->meta_title ? : 'Privacy Policy')
@section('meta-description', $privacyPolicy->meta_description ? : null)
@section('meta-keywords', $privacyPolicy->meta_keywords ? : null)

@section('css')

<link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsparallaxer.css">
<link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsscroller/scroller.css">
<link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/advancedscroller/plugin.css">
<link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/animate.css">
@endsection
@section('body')
    <section class="dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll loaded dzsprx-readyall" data-options="{direction: 'fromtop', animation_duration: 25, direction: 'reverse'}">
        <div class="divimage dzsparallaxer--target w-100 g-bg-pos-top-center g-bg-cover g-bg-black-opacity-0_2--after" style="height: 140%; background-image: url(/bagbox-banner.jpg); transform: translate3d(0px, -87.3959px, 0px);"></div>
    
        <div class="container g-color-white g-pt-70 g-pb-20">
          <div class="g-mb-50">
             
    
            <span class="d-block g-color-white-opacity-0_8 g-font-weight-300 g-font-size-20">Delivery Asset Partner</span>
            <h3 class="g-color-white g-font-size-50 g-font-size-50--md g-line-height-1_2 mb-0">BAGBOX</h3>
            <p class="g-color-white g-font-weight-600 g-font-size-20 text-uppercase">Since, 2007</p>
          </div>
          <div class="d-flex justify-content-end">
              <ul class="u-list-inline g-bg-gray-dark-v1 g-font-weight-300 g-rounded-50 g-py-5 g-px-20">
                <li class="list-inline-item g-mr-5">
                  <a class="u-link-v5 g-color-white g-color-primary--hover" href="/">Home</a>
                  <i class="g-color-white-opacity-0_5 g-ml-5">/</i>
                </li>
                <li class="list-inline-item g-color-primary g-font-weight-400">
                  <span>Privacy Policy</span>
                </li>
              </ul>
            </div>
    
        </div>
    </section>
    <div class="r_container">
    <header class=" g-mb-10">
            <h2 class="text-center h3 u-heading-v2__title text-uppercase g-font-weight-600 mb-0 g-py-10">Privacy Policy</h2>
        </header>
  
      <div>
        {!! $privacyPolicy->content !!}
      </div>
    </div>
@endsection


@section('js')

<script src="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsparallaxer.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsscroller/scroller.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/advancedscroller/plugin.js"></script>


@endsection