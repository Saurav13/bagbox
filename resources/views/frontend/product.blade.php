@extends('layouts.main')

@section('title', $product->meta_title ? : $product->product_name)
@section('meta-description', $product->meta_description)
@section('meta-keywords', $product->meta_keywords)

@section('css')

  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsparallaxer.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsscroller/scroller.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/advancedscroller/plugin.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/animate.css">
@endsection

@section('body')

<section class="dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll loaded dzsprx-readyall" data-options="{direction: 'fromtop', animation_duration: 25, direction: 'reverse'}">
    <div class="divimage dzsparallaxer--target w-100 g-bg-pos-top-center g-bg-cover g-bg-black-opacity-0_2--after" style="height: 140%; background-image: url(/bagbox-banner.jpg); transform: translate3d(0px, -87.3959px, 0px);"></div>

    <div class="container g-color-white g-pt-70 g-pb-20">
      <div class="g-mb-50">
         

        <span class="d-block g-color-white-opacity-0_8 g-font-weight-300 g-font-size-20">Delivery Asset Partner</span>
        <h3 class="g-color-white g-font-size-50 g-font-size-50--md g-line-height-1_2 mb-0">BAGBOX</h3>
        <p class="g-color-white g-font-weight-600 g-font-size-20 text-uppercase">Since, 2007</p>
      </div>
      <div class="d-flex justify-content-end">
          <ul class="u-list-inline g-bg-gray-dark-v1 g-font-weight-300 g-rounded-50 g-py-5 g-px-20">
            <li class="list-inline-item g-mr-5">
              <a class="u-link-v5 g-color-white g-color-primary--hover" href="{{URL::to('home')}}"">Home</a>
              <i class="g-color-white-opacity-0_5 g-ml-5">/</i>
            </li>
            <li class="list-inline-item g-mr-5">
              <a class="u-link-v5 g-color-white g-color-primary--hover" href="/shop">All Products</a>
              <i class="g-color-white-opacity-0_5 g-ml-5">/</i>
            </li>
            <li class="list-inline-item g-color-primary g-font-weight-400">
              <span>{{$product->product_name}}</span>
            </li>
          </ul>
        </div>

    </div>
</section>
  <!-- Breadcrumbs -->
 
  <!-- End Breadcrumbs -->

  
  <!-- Product Description -->
  <div class="r_container g-pt-30 ">
   
    <div class="row g-py-20" style="    background: #f8f8f8;">
      <div class="col-md-4">
        <div style="">
          <div id="carousel-08-1" class="js-carousel text-center g-mb-20" data-infinite="true" data-arrows-classes="u-arrow-v1 g-absolute-centered--y g-width-35 g-height-40 g-font-size-18 g-color-gray g-bg-white g-mt-minus-10" data-arrow-left-classes="fa fa-angle-left g-left-0" data-arrow-right-classes="fa fa-angle-right g-right-0" data-nav-for="#carousel-08-2">
            @foreach($images as $img)
              
              <div class="js-slide">
                <a class="js-fancybox d-block g-pos-rel" href="javascript:;" data-fancybox="lightbox-gallery--08-1" data-src="/frontend-assets/main-assets/assets/img-temp/500x450/img1.jpg" data-caption="Lightbox Gallery" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true">
                <img class="img-fluid w-100" src="/product-images/{{$img->image}}" alt="{{$product->product_name}}">
                </a>
              </div>
            @endforeach
          </div>
            
          <div id="carousel-08-2" class="js-carousel text-center g-mx-minus-10 u-carousel-v3" data-infinite="true" data-center-mode="true" data-slides-show="4" data-is-thumbs="true" data-nav-for="#carousel-08-1">
            @foreach($images as $img)
             
              <div class="js-slide g-px-10">
                <img class="img-fluid w-100" src="/product-images/{{$img->image}}" alt="{{$product->product_name}}">
              </div>
            @endforeach
          </div>
        </div>
      </div>  
      {{-- <div class="col-lg-5">
        <!-- Carousel -->
        <div id="carouselCus2" class="js-carousel g-pt-10 g-mb-10"
          data-infinite="true"
          data-fade="true"
          data-arrows-classes="u-arrow-v1 g-brd-around g-brd-white g-absolute-centered--y g-width-45 g-height-45 g-font-size-14 g-color-white g-color-primary--hover rounded-circle"
          data-arrow-left-classes="fa fa-angle-left g-left-40"
          data-arrow-right-classes="fa fa-angle-right g-right-40"
          data-nav-for="#carouselCus2">
            @foreach($images as $img)
              <div class="js-slide g-bg-cover g-bg-black-opacity-0_1--after">
                <img class="img-fluid w-100" src="{{asset('product-images'.'/'.$img->image)}}" alt="{{$product->product_name}}">
              </div>
            @endforeach
            
        </div>
        @if(count($images) > 1)
          <div id="carouselCus2" class="js-carousel text-center u-carousel-v3 g-mx-minus-5"
            data-center-mode="true"
            data-slides-show="3"
            data-is-thumbs="true"
            data-focus-on-select="true"
            data-nav-for="#carouselCus1">
            @foreach($images as $img)
              <div class="js-slide g-cursor-pointer g-px-5">
                <img class="img-fluid" src="{{asset('product-images'.'/'.$img->image)}}" alt="{{$product->product_name}}">
              </div>
            @endforeach
          </div>
        @endif
        <!-- End Carousel -->
        
      </div> --}}

      <div class="col-lg-8">
        <div class="g-px-40--lg g-pt-10">
          <!-- Product Info -->
          @if($product->pdf)
            <a class="g-text-underline--none--hover pull-right" download href="/product-files/{{$product->pdf}}">
                  <button  class="btn btn-block u-btn-primary g-font-size-12  text-uppercase g-py-15 g-px-25" type="button">
                    Download Pdf
                    <i class="align-middle ml-2 fa fa-download"></i>
                  </button>
                </a>
            @endif
            @if($product->video_link)
                <a class="g-text-underline--none--hover pull-right g-pr-5" href="javascript:;">
                  <button videosource="{{$product->video_link}}" id="watchVideo{{$product->id}}"  class="btn btn-block u-btn-primary g-font-size-12  text-uppercase g-py-15 g-px-25" type="button">
                    Watch Video 
                    <i class="align-middle ml-2 fa fa-video-camera"></i> 
                  </button>
                </a>
            @endif
          <div class="g-mb-30">
              {{-- <h2 class="g-color-gray-dark-v5 g-font-weight-400 g-font-size-16 text-uppercase mb-2">Product Code: 12324</h2> --}}

            <h1 class="g-font-weight-400 mb-4 g-font-size-24">{{$product->product_name}}</h1>
            <div class="g-mb-30">
                <h2 class="g-color-gray-dark-v5 g-font-weight-600 g-font-size-16 text-uppercase mb-2">Price:</h2>
                <span class="g-font-weight-400 g-font-size-20 mr-2">AED {{ number_format($product->price) }}</span>
                @if($product->sale_price)
               
                  <span style="display:inline"><del>AED {{$product->regular_price}}</del></span>

                @endif
            </div>
            <div class="row g-mx-minus-5 g-mb-20">
                <div class="col g-px-5 g-mb-10">
                    
                      
                  @php
                      $links = $product->links ? json_decode($product->links) : [];
                  @endphp
                  @foreach($links as $link)
                    <a href="{{ $link->link }}" class="btn btn-md u-btn-inset u-btn-inset--rounded u-btn-primary g-font-weight-600 g-letter-spacing-0_5 text-uppercase g-brd-2 g-rounded-50 g-mr-10 g-mb-15">
                        <i class="fa fa-check-circle g-mr-3"></i>
                        Buy on {{ $link->name }} 
                      </a>
                      
                    {{-- <a class="g-text-underline--none--hover" href="{{ $link->link }}">
                      <button  class="btn btn-primary pull-right u-btn-primary g-font-size-12 g-ml-5 text-uppercase g-py-15 g-px-25" type="button">
                        Buy on {{ $link->name }}
                      </button>
                    </a> --}}
  
                  @endforeach
                </div>
  
              </div>
              <h2 class="g-color-gray-dark-v5 g-font-weight-600 g-font-size-16 text-uppercase mb-2">Details:</h2>

            <div style="overflow-y:auto;max-height:28rem " class="g-font-size-20">
            {!! $product->product_description !!}
            </div>
          </div>
        
          <!-- End Product Info --> 

          <!-- Price -->
          
          <!-- End Price -->
            <!-- Quantity -->
            <div class="row">
              @if($product->is_retail)

                <div class="col-md-4">
                    <div class="d-flex justify-content-between align-items-center g-brd-bottom g-brd-gray-light-v3 py-3 g-mb-30" role="tab">
                        <h5 class="g-color-gray-dark-v5 g-font-weight-400 g-font-size-default mb-0">Quantity</h5>
          
                        <div class="js-quantity input-group u-quantity-v1 g-width-80 g-brd-primary--focus">
                          <input class="js-result form-control text-center g-font-size-13 rounded-0" id="quantity" type="text" value="1" min="1" readonly>
          
                          <div class="input-group-addon d-flex align-items-center g-brd-gray-light-v2 g-width-30 g-bg-white g-font-size-13 rounded-0 g-pa-5">
                            <i class="js-plus g-color-gray g-color-primary--hover fa fa-angle-up" id="upnum"></i>
                            <i class="js-minus g-color-gray g-color-primary--hover fa fa-angle-down" id="downnum"></i>
                          </div>
                        </div>
                      </div>
                </div>
                <div class="col-md-4">
                    <div class="row g-mx-minus-5 g-mb-20">
                        <div class="col g-px-5 g-mb-10">
                          @if($product->quantity === 0)
                            <button class="btn u-btn-black g-font-size-12 text-uppercase g-py-15 g-px-25" type="button">
                              Sold Out 
                            </button>
                          @else
                            <button class="btn u-btn-primary g-font-size-12 text-uppercase g-py-15 g-px-25" type="button" id="addCart">
                              Add to Cart <i class="align-middle ml-2 icon-finance-100 u-line-icon-pro"></i>
                            </button>
                          @endif
                        </div>
                      </div>
                </div>
              @endif


              <div class="col-md-4">
                
                @if($product->is_wholesale)
                  <a class="g-text-underline--none--hover" href="{{URL::to('order'.'/'.$product->slug)}}">
                    <button  class="btn btn-primary u-btn-primary g-font-size-12  text-uppercase g-py-15 g-px-25" type="button">
                      Place Mass Order
                      <i class="align-middle ml-2 icon-finance-100 u-line-icon-pro"></i>
                    </button>
                  </a>
                @endif
                
              </div>
           
            </div>
           
            <!-- End Quantity -->
        
     
        </div>
      </div>
    </div>
  </div>
  <!-- End Product Description -->


  <!-- Description -->
  {{-- <div class="container">
    <div class="g-brd-y g-brd-gray-light-v4 g-pt-100 g-pb-70">
      <h2 class="h4 mb-3">Details</h2>

      <div class="row">
        

        @if(count($product->details) != null)
          @foreach ($details->chunk(5) as $chunk)
              
            <div class="col-md-4 g-mb-0 g-mb-30--md">
              <!-- List -->
              <ul class="list-unstyled g-color-text">
                @foreach ($chunk as $c)
                  <li class="g-brd-bottom--dashed g-brd-gray-light-v3 pt-1 mb-3">
                    <span>{{ $c->name }}:</span>
                    <span class="float-right g-color-black">{{ $c->value[0] }}</span>
                  </li>
                @endforeach
                
              </ul>
              <!-- End List -->
            </div>
          @endforeach
        @endif
      </div>
    </div>
  </div> --}}
  <!-- End Description -->

  {{-- @if(count($similar) > 0)
    <!-- Products -->
    <div class="container g-pt-100 g-pb-70">
      <div class="text-center mx-auto g-max-width-600 g-mb-50">
        <h2 class="g-color-black mb-4">Similar Products</h2>
        <p class="lead">We want to create a range of beautiful, practical and modern outerwear that doesn't cost the earth – but let's you still live life in style.</p>
      </div>

      <!-- Products -->
      <div class="row">
        @foreach ($similar as $item)
          <div class="col-6 col-lg-3 g-mb-30">
            <!-- Product -->
            <figure class="g-pos-rel g-mb-20">
              <a href="{{URL::to('products'.'/'.$item->slug)}}">
                <img class="img-fluid" src="{{asset('product-images'.'/'.$item->images->first()->image)}}" alt="{{$item->product_name}}">
              </a>

              @if($item->featured)
                <figcaption class="w-100 g-bg-primary g-bg-black--hover text-center g-pos-abs g-bottom-0 g-transition-0_2 g-py-5">
                  <a class="g-color-white g-font-size-11 text-uppercase g-letter-spacing-1 g-text-underline--none--hover" href="#!">Featured</a>
                </figcaption>
              @endif
            </figure>
    
            <div class="media">
              <!-- Product Info -->
              <div class="d-flex flex-column">
                <h4 class="h6 g-color-black mb-1">
                  <a class="u-link-v5 g-color-black g-color-primary--hover" href="{{URL::to('products'.'/'.$item->slug)}}">
                    {{$item->product_name}}
                  </a>
                </h4>
                <a href="/wallpapers?filter={{$item->subcategory->slug}}" class="d-inline-block g-color-gray-dark-v5 g-font-size-13">{{$item->subcategory->subcat_name}}</a>
                <span class="d-block g-color-black g-font-size-17">Rs.{{ number_format($item->price) }} per roll</span>
              </div>
              <!-- End Product Info -->
    
              <!-- Products Icons -->
              <ul class="list-inline media-body text-right">
                  <li class="list-inline-item align-middle mx-0">
                      <a class="u-icon-v1 u-icon-size--sm g-color-gray-dark-v5 g-color-primary--hover g-font-size-15 rounded-circle"
                          href="{{URL::to('visualizer?alias='.$item->slug.'&filter='.$item->subcategory->slug)}}"
                          data-toggle="tooltip"
                          data-placement="top"
                          title="View" target="_blank">
                                  <i class="fa fa-eye u-line-icon-pro"></i>
                          
                      </a>
                  </li>
                  <li class="list-inline-item align-middle mx-0">
                      <a class="u-icon-v1 u-icon-size--sm g-color-gray-dark-v5 g-color-primary--hover g-font-size-15 rounded-circle"
                          href="{{URL::to('order'.'/'.$item->slug)}}"
                          data-toggle="tooltip"
                          data-placement="top"
                          title="Place Order">
                          <i class="icon-finance-100 u-line-icon-pro" style="margin-top:3px"></i>
                      </a>
                  </li>
              </ul>
              <!-- End Products Icons -->
            </div>
            <!-- End Product -->
          </div>
        @endforeach
      </div>
      <!-- End Products -->
    </div>
    <!-- End Products -->
  @endif --}}
  
  <div id="videoModal" class="text-left g-max-width-600  g-overflow-y-auto g-pa-20" style="display: none; max-width:1000px !important">
      <button type="button" class="close" onclick="Custombox.modal.close();" style="color:white;cursor:pointer">
        <i class="hs-icon hs-icon-close"></i>
      </button>
      <iframe id="ytplayer" type="text/html" width="640" height="360" src=""
      allowscriptaccess="always" allow="autoplay"
      frameborder="0"></iframe>
  </div>
@endsection

@section('js')

<script src="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsparallaxer.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsscroller/scroller.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/advancedscroller/plugin.js"></script>
<script>
  // $.HSCore.components.HSCarousel.init('[class*="js-carousel"]');
  $('[id*="watchVideo"]').click(function(){

    var source=$(this).attr('videosource');

    var modal = new Custombox.modal({

      content: {
        effect: 'fadein',
        target: '#videoModal',
        onOpen: function(){
          $('#ytplayer').attr("src",source+'?autoplay=1&amp;modestbranding=1&amp;showinfo=0')
        },
        onClose: function(){
          $('#ytplayer').attr("src","");
        },
      }
    });
    modal.open();

  });

  $(document).ready(function(){

    $('#addCart').click(function(e){
      if($('#quantity').val() < 1){
        themeAlert('Quantity cannot be less than 1','Error','');                    
        return;
      }

      var id='{{ $product->id }}';

      $(this).attr('disabled','disabled');
      $(this).find('i').removeClass('icon-finance-100').addClass('fa fa-spinner fa-spin');

      $.ajax({
          type: "POST",
          url: "{{ URL::to('/cart/additem') }}",
          data: { id:id, _token:'{{ csrf_token() }}', quantity:$('#quantity').val() }, // serializes the form's elements.
          success: function(data)
          {
              $('#addCart').removeAttr('disabled');
              $('#addCart').find('i').addClass('icon-finance-100').removeClass('fa fa-spinner fa-spin');
              $('#cartTotal').text(data.count);

              themeNotify('success','Item Successfully added to Cart.');
          },
          error:function(response)
          {
              $('#addCart').removeAttr('disabled');
              $('#addCart').find('i').addClass('icon-finance-100').removeClass('fa fa-spinner fa-spin');

              showErrors(response);
          }
      });
    });

    $('#upnum').click(function(){
      var v=parseInt($('#quantity').val()) +1
      $('#quantity').attr('value',v)
    });
    $('#downnum').click(function(){
      var v=parseInt($('#quantity').val()) -1
      if(v <= 0) return;
      $('#quantity').attr('value',v)
    });
  })
</script>
@endsection