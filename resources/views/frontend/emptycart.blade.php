@extends('layouts.main')
@section('title', 'Cart')
@section('body')
    
    <div class="container text-center g-py-100">
        <div class="mb-5">
            <span class="d-block g-color-gray-light-v1 g-font-size-70 g-font-size-90--md mb-4">
            <i class="icon-hotel-restaurant-105 u-line-icon-pro"></i>
            </span>
            <h2 class="g-mb-30">Your Cart is Currently Empty</h2>
            <p>Before proceed to checkout you must add some products to your shopping cart.<br>You will find a lot of interesting products on our "Shop" page.</p>
        </div>
        <a class="btn u-btn-primary g-font-size-12 text-uppercase g-py-12 g-px-25" href="/shop">Start Shopping</a>
    </div>
@endsection