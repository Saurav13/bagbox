<body>
  <main>
    <style>
      .nav-link:hover{
          color:#ee1923 !important;
      }
      .sm-hidden{
        display:none;
      }
      .lg-hidden{
        display:block;
      }
      .r_container{
        margin:1rem 1rem;
      }
      .card_product{
      box-shadow: 1px 2px 11px 2px #3949a054;
      padding: 1rem;
    /* margin: 9px 10px; */
    }
      @media  only screen and (min-width: 600px) {
        .sm-hidden{
          display:none;
        }
        .lg-hidden{
          display:block;
        }
        .r_container{
          margin:1rem 1rem;
        }
      } 

      @media  only screen and (min-width: 768px) {
        .sm-hidden{
          display:none;
        }
        .lg-hidden{
          display:block;
        }
        .r_container{
          margin:1rem 1rem;
        }
      } 

      @media  only screen and (min-width: 992px) {
        .sm-hidden{
          display:block;
        }
        .lg-hidden{
          display:none;
        }
        .r_container{
          margin:1rem 1rem;
        }
      } 

      @media  only screen and (min-width: 1200px) {
        .sm-hidden{
          display:block;
        }
        .lg-hidden{
          display:none;
        }
        .r_container{
          margin:0.5rem 7rem;
        }
      }
      .flex_container{
        display: flex;
        justify-content: space-between;
        align-items: center;
        width: 100%;
      }
      @media (min-width: 1200px){
        .container {
            max-width: 1250px;
        }
      }
      </style>
    <!-- Header -->
    <header id="js-header" class="u-header u-header--static">
      <div class="u-header__section u-header__section--light g-bg-white g-transition-0_3 g-py-10">
        <nav class="js-mega-menu navbar navbar-expand-lg">
          <div class="container " >
            <!-- Responsive Toggle Button -->
            <button class="navbar-toggler navbar-toggler-right btn g-line-height-1 g-brd-none g-pa-0 g-pos-abs g-top-3 g-right-0" type="button"
                    aria-label="Toggle navigation"
                    aria-expanded="false"
                    aria-controls="navBar"
                    data-toggle="collapse"
                    data-target="#navBar">
              <span class="hamburger hamburger--slider">
                <span class="hamburger-box">
                  <span class="hamburger-inner"></span>
                </span>
              </span>
            </button>
            <!-- End Responsive Toggle Button -->

            <!-- Logo -->
            <a href="/" class="navbar-brand">
              <img src="/logo.png" style="height:5rem" alt="Logo">
            </a>
            <!-- End Logo -->

            <!-- Navigation -->
            <div class="collapse navbar-collapse align-items-center flex-sm-row g-pt-10 g-pt-5--lg g-mr-20--lg" id="navBar">
              <ul class="navbar-nav text-uppercase g-pos-rel g-font-weight-600 ml-auto">
                <!-- Intro -->
                <li class="nav-item g-mx-10--lg g-mx-15--xl">
                  <a href="/about-us" class="nav-link g-py-7 g-px-0">About Us</a>
                </li>
                <!-- End Intro -->

                <!-- Pages -->
                <li class="nav-item hs-has-sub-menu g-mx-10--lg g-mx-15--xl"
                    data-animation-in="fadeIn"
                    data-animation-out="fadeOut">
                  <a id="nav-link-pages" class="nav-link g-py-7 g-px-0" href="#!"
                     aria-haspopup="true"
                     aria-expanded="false"
                     aria-controls="nav-submenu-pages"
                  >Categories
                  </a>

                  <ul class="hs-sub-menu list-unstyled u-shadow-v11 g-brd-top g-brd-primary g-brd-top-2 g-min-width-220 g-mt-20 g-mt-10--lg--scrolling" id="nav-submenu-pages"
                      aria-labelledby="nav-link-pages">
                    @foreach($nav_categories as $cat)
                      <li class="dropdown-item">
                        <a class="nav-link" href="/shop/{{$cat->slug}}">{{$cat->cat_name}}</a>
                      </li>
                    @endforeach
                    <li class="dropdown-item">
                      <a href="/shop" class="nav-link">All Products</a>
                    </li>
                  </ul>
                </li>
                <!-- End Pages -->

                <li class="nav-item hs-has-sub-menu g-mx-10--lg g-mx-15--xl"
                    data-animation-in="fadeIn"
                    data-animation-out="fadeOut">
                  <a id="nav-link-pages" class="nav-link g-py-7 g-px-0" href="#!"
                     aria-haspopup="true"
                     aria-expanded="false"
                     aria-controls="nav-submenu-pages"
                  >Downloads
                  </a>

                  <ul class="hs-sub-menu list-unstyled u-shadow-v11 g-brd-top g-brd-primary g-brd-top-2 g-min-width-220 g-mt-20 g-mt-10--lg--scrolling" id="nav-submenu-pages"
                      aria-labelledby="nav-link-pages">
                    @foreach($downloads as $download)
                      <li class="dropdown-item">
                        <a class="nav-link" target="_blank" href="{{ asset('download-files/'.$download->file) }}">{{$download->title}}</a>
                      </li>
                    @endforeach
                  </ul>
                </li>
           
                <!-- Shortcodes -->
                <li class="nav-item g-mx-10--lg g-mx-15--xl">
                  <a href="/clients" class="nav-link g-py-7 g-px-0">Our Clients</a>
                </li>
                 <li class="nav-item g-mx-10--lg g-mx-15--xl">
                  <a href="/testimonials" class="nav-link g-py-7 g-px-0">Testimonials</a>
                </li>
                <li class="nav-item g-mx-10--lg g-mx-15--xl">
                  <a href="/inquiry" class="nav-link g-py-7 g-px-0">Inquiry</a>
                </li>
                 <li class="nav-item g-mx-10--lg g-mx-15--xl">
                  <a href="/blog" class="nav-link g-py-7 g-px-0">News and Events</a>
                </li>
                <!-- End Shortcodes -->

              
              </ul>
            </div>
            <!-- End Navigation -->

            <!-- Basket -->
            <div class="u-basket d-inline-block g-z-index-3 g-mr-0--lg">
              <div class="g-py-10 g-px-6">
                <a href="/cart" id="basket-bar-invoker" class="g-color-primary-opacity-0_8 g-color-primary--hover g-font-size-17 g-text-underline--none--hover"
                   aria-controls="basket-bar"
                   aria-haspopup="true"
                   aria-expanded="false"
                   data-dropdown-event="hover"
                   data-dropdown-target="#basket-bar"
                   data-dropdown-type="css-animation"
                   data-dropdown-duration="300"
                   data-dropdown-hide-on-scroll="false"
                   data-dropdown-animation-in="fadeIn"
                   data-dropdown-animation-out="fadeOut">
                  <span class="u-badge-v1--sm g-color-white g-bg-primary g-font-size-11 g-line-height-1_4 g-rounded-50x g-pa-4" style="top: 7px !important; right: 3px !important;" id="cartTotal">{{ Cart::content()->count() }}</span>
                  <i class="icon-hotel-restaurant-105 u-line-icon-pro"></i> 
                </a>
              </div>

            </div>
            <!-- End Basket -->

            {{-- <div class="d-inline-block g-hidden-xs-down g-pos-rel g-valign-middle g-pl-30 g-pl-0--lg">
              <a class="btn u-btn-outline-primary g-font-size-13 text-uppercase g-py-10 g-px-15" href="/shop" >Shop now</a>
            </div> --}}

          </div>
        </nav>
      </div>
    </header>
    <!-- End Header -->
