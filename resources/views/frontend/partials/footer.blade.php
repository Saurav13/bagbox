
   <img src="/img/Iso.png" style="position:fixed; height:6rem; bottom:9px; left:0px;"/>
   <!-- Copyright Footer -->
      <footer class=" g-color-white text-center" style="background:url(/contact.jpg); background-size:cover">
          <div style="background-color:#00000082" class=" g-py-60">
            <div class="container">
              <header class="u-heading-v8-2 text-center g-width-70x--md mx-auto g-mb-80">
                <h1 class="u-heading-v8__title text-uppercase g-font-weight-600 g-mb-25">Give us Your FeedBack</h1>
                <p class="lead mb-0 g-font-size-20">Your feedbacks are highly appreciated</p>
              </header>
              <form action="{{route('contact.us')}}" enctype="multipart/form-data" method="POST">
                  {{csrf_field()}}
                    <div class="row">
                      <div class="col-md-6">
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group g-mb-20">
                              <input id="inputGroup10_1" name="name" class="form-control g-bg-transparent g-bg-transparent--focus g-brd-white-opacity-0_5 g-brd-white--focus g-color-white g-rounded-50 g-px-20 g-py-12" type="text" placeholder="Name">
                            </div>
              
                            <div class="form-group g-mb-20">
                              <input id="inputGroup10_2" name="profession" class="form-control g-bg-transparent g-bg-transparent--focus g-brd-white-opacity-0_5 g-brd-white--focus g-color-white g-rounded-50 g-px-20 g-py-12" type="tel" placeholder="Profession">
                            </div>
                          </div>
              
                        </div>
                      </div>
              
                      <div class="col-md-6">
                        <div class="form-group g-mb-20">
                          <textarea id="inputGroup11_5" name="content" class="form-control g-resize-none g-bg-transparent g-bg-transparent--focus g-brd-white-opacity-0_5 g-brd-white--focus g-color-white g-rounded-20 g-px-20 g-py-10" rows="5" placeholder="FeedBack"></textarea>
                        </div>
                        
                      
                        
                      </div>
                      <div class="col-md-12">
                        <div class="row">
                          <div class="col-md-6">
                              <div class="form-group g-mb-20">
                                <p style="text-align: left">Photo (Optional)</p>
                                  <input id="inputGroup10_2" name="photo" class="form-control g-bg-transparent g-bg-transparent--focus g-brd-white-opacity-0_5 g-brd-white--focus g-color-white g-rounded-50 g-px-20 g-py-12" type="file" placeholder="Photo">
                              </div>

                            <div class="pull-left">
                                <button class=" btn u-btn-white g-font-size-11 text-uppercase g-font-weight-700 g-rounded-50 g-px-35 g-py-15" type="submit" role="button">Submit</button>
                            </div>
                          </div>
                            
                        </div>
                      </div>
                    </div>
                </form>
          
                <header class="u-heading-v8-2 text-center g-width-70x--md mx-auto g-mb-20">
                    {{-- <h1 class="u-heading-v8__title text-uppercase g-font-weight-600 g-mb-25">Contact Us</h1> --}}
                    <p class="lead mb-0 g-font-size-20">Contacts:</p>
                  </header>
              <address class="row g-color-white-opacity-0_8">
                <div class="col-sm-6 col-md-3 g-mb-60">
                  <i class="icon-line icon-map d-inline-block display-5 g-mb-25"></i>
                  <h4 class="small text-uppercase g-mb-5 g-font-size-18">Address</h4>
                  <strong class="g-font-size-18">{{ $contact->address }}</strong>
                </div>
          
                <div class="col-sm-6 col-md-3 g-mb-60">
                  <i class="icon-call-in d-inline-block display-5 g-mb-25"></i>
                  <h4 class="small text-uppercase g-mb-5 g-font-size-18">Phone number</h4>
                  <strong class="g-font-size-18">Telephone: {{ $contact->phone }}</strong><br>
                   <strong class="g-font-size-18">Fax: {{ $contact->fax }}</strong><br>
                  <strong class="g-font-size-18">Mobile: {{ $contact->mobile }} </strong>
                
                </div>
          
                <div class="col-sm-6 col-md-3">
                  <i class="icon-envelope d-inline-block display-5 g-mb-25"></i>
                  <h4 class="small text-uppercase g-mb-5 g-font-size-18">Email</h4>
                  <?php $emails = explode (',' ,$contact->email) ?>
                  @foreach($emails as $p)
                    <a target="_blank" class="g-color-white-opacity-0_8" href="mailto:{{ $p }}">
                      <strong class="g-font-size-18">{{ $p }}</strong>
                    </a><br>
                  @endforeach
                </div>
          
                <div class="col-sm-6 col-md-3">
                  <i class="icon-earphones-alt d-inline-block display-5 g-mb-25"></i>
                  <h4 class="small text-uppercase g-mb-5 g-font-size-18">Post Box</h4>
                  <strong class="g-font-size-18">{{ $contact->postbox }}</strong>
                </div>
              </address>
          
           
            </div>
          </div>
        </footer>
      <footer class="g-bg-gray-dark-v1 g-color-white-opacity-0_8 g-py-20">
        <div class="container">
          <div class="row">
            <div class="col-md-8 text-center text-md-left g-mb-10 g-mb-0--md">
              <div class="d-lg-flex">
                <small class="d-block g-font-size-default g-mr-10 g-mb-10 g-mb-0--md">{{ date('Y') }} © All Rights Reserved.</small>
                <ul class="u-list-inline">
                  <li class="list-inline-item">
                    <a class="g-color-white-opacity-0_8 g-color-white--hover" href="/privacy-policy">Privacy Policy</a>
                  </li>
                  <li class="list-inline-item">
                    <span>|</span>
                  </li>
                  <li class="list-inline-item">
                    <a class="g-color-white-opacity-0_8 g-color-white--hover" href="/terms-and-conditions">Terms and Conditions</a>
                  </li>
                </ul>
              </div>
            </div>

            <div class="col-md-4 align-self-center">
              <ul class="list-inline text-center text-md-right mb-0">
                <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Facebook">
                  <a href="{{ $social_links->facebook }}" target="_blank" class="g-color-white-opacity-0_5 g-color-white--hover">
                    <i class="fa fa-facebook"></i>
                  </a>
                </li>
                <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Twitter">
                  <a href="{{ $social_links->twitter }}" target="_blank" class="g-color-white-opacity-0_5 g-color-white--hover">
                    <i class="fa fa-twitter"></i>
                  </a>
                </li>
                <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Instagram">
                  <a href="{{ $social_links->instagram }}" target="_blank" class="g-color-white-opacity-0_5 g-color-white--hover">
                    <i class="fa fa-instagram"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </footer>
      <div style="position: fixed; bottom: 0; right: 0;z-index:2147483647;" id="alertStack"></div>

      @if(Session::has('success'))
      <div id="successModal" class="text-left g-max-width-600 g-bg-white g-overflow-y-auto g-pa-20" style="display: none;">
          <button type="button" class="close" onclick="Custombox.modal.close();">
            <i class="hs-icon hs-icon-close"></i>
          </button>
          <h4 class="g-mb-20">Thank You!!!</h4>
          <p>
            {{Session::get('success')}}
          </p>
      </div>
      @endif
      <!-- End Copyright Footer -->
    </main>
  </body>

  <!-- JS Global Compulsory -->
  <script src="/frontend-assets/main-assets/assets/vendor/jquery/jquery.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/jquery-migrate/jquery-migrate.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/jquery.easing/js/jquery.easing.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/popper.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/bootstrap/bootstrap.min.js"></script>

  <!-- JS Implementing Plugins -->
  <script src="/frontend-assets/main-assets/assets/vendor/masonry/dist/masonry.pkgd.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/imagesloaded/imagesloaded.pkgd.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/slick-carousel/slick/slick.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/hs-megamenu/src/hs.megamenu.js"></script>

  <!-- JS Unify -->
  <script src="/frontend-assets/main-assets/assets/js/hs.core.js"></script>
  <script src="/frontend-assets/main-assets/assets/js/components/hs.header.js"></script>
  <script src="/frontend-assets/main-assets/assets/js/helpers/hs.hamburgers.js"></script>
  <script src="/frontend-assets/main-assets/assets/js/components/hs.scroll-nav.js"></script>
  <script src="/frontend-assets/main-assets/assets/js/components/hs.carousel.js"></script>

  <!-- JS Revolution Slider -->
  <script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/jquery.themepunch.tools.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/jquery.themepunch.revolution.min.js"></script>

  <script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution-addons/polyfold/js/revolution.addon.polyfold.min.js"></script>

  <script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.actions.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.migration.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.video.min.js"></script>
  <script  src="/frontend-assets/main-assets/assets/vendor/custombox/custombox.min.js"></script>
  
  <!-- JS Unify -->
  <script  src="/frontend-assets/main-assets/assets/js/components/hs.modal-window.js"></script>
  
  <!-- JS Custom -->
  <script src="/frontend-assets/main-assets/assets/js/custom.js"></script>
  <script src="{{asset('/js/main.js')}}"></script>

  @yield('js')
  <!-- JS Plugins Init. -->
  @if(Session::has('success'))
    <script>
    $(document).on('ready', function () {
      var modal = new Custombox.modal({
                  content: {
                      effect: 'door',
                      target: '#successModal'
                  }
              });
          
              // Open
              modal.open();
    });
    </script>
  @endif
  <script>
    $(document).on('ready', function () {
      // initialization of carousel
      $.HSCore.components.HSCarousel.init('.js-carousel');

      // initialization of masonry
      $('.masonry-grid').imagesLoaded().then(function () {
        $('.masonry-grid').masonry({
          columnWidth: '.masonry-grid-sizer',
          itemSelector: '.masonry-grid-item',
          percentPosition: true
        });
      });

      // Header
      $.HSCore.components.HSHeader.init($('#js-header'));
      $.HSCore.helpers.HSHamburgers.init('.hamburger');

      // Initialization of HSMegaMenu plugin
      $('.js-mega-menu').HSMegaMenu({
        event: 'hover',
        pageContainer: $('.container'),
        breakpoint: 991
      });

      $('.addToCart').on('click',function(e){
          var id = $(this).data('id');
          var ele = $(this);
          ele.attr('disabled','disabled');
          ele.find('i').removeClass('icon-finance-100').addClass('fa fa-spinner fa-spin');

          $.ajax({
              type: "POST",
              url: "{{ URL::to('/cart/additem') }}",
              data: { id:id, _token:'{{ csrf_token() }}', quantity: 1 }, // serializes the form's elements.
              success: function(data)
              {
                  ele.removeAttr('disabled');
                  ele.find('i').addClass('icon-finance-100').removeClass('fa fa-spinner fa-spin');

                  $('#cartTotal').text(data.count);
                  themeNotify('success','Item Successfully added to Cart.');
              },
              error:function(response)
              {
                  ele.removeAttr('disabled');
                  ele.find('i').addClass('icon-finance-100').removeClass('fa fa-spinner fa-spin');

                  showErrors(response);
              }
          });
      });
    });

   
  </script>
  <!--Start of Tawk.to Script-->
  <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5df64e0843be710e1d222cc3/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
  </script>
  <!--End of Tawk.to Script-->
</html>
