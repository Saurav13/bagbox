<!DOCTYPE html>
<html lang="en" class="font-primary">
  <head>
    <title>@yield('title', $home_meta_title)</title>

    <!-- Required Meta Tags Always Come First -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="@yield('meta-description', $home_meta_description)">
    <meta name="keywords" content="@yield('meta-keywords', $home_meta_keywords)">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    @yield('meta-tags')

    <link rel="canonical"  href="{{url()->current()}}"/>
    <!-- Fav-->
    <link rel="shortcut icon" href="/frontend-assets/main-assets/favicon.ico">

    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto%3A700%2C300">
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Poppins:600">

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/bootstrap/bootstrap.min.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/icon-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/icon-etlinefont/style.css">
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/icon-hs/style.css">
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/icon-line-pro/style.css">

    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/animate.css">
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/slick-carousel/slick/slick.css">

    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/hs-megamenu/src/hs.megamenu.css">
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/hamburgers/hamburgers.min.css">

    <!-- CSS Unify -->
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/css/unify.css">

    <!-- Revolution Slider -->
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/css/settings.css">
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution-addons/polyfold/css/revolution.addon.polyfold.css">

    <link  rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/custombox/custombox.min.css">
    @yield('css')
  </head>