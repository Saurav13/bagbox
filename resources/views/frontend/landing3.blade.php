@extends('layouts.main')

@section('css')
<link  rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsparallaxer.css">
<link  rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsscroller/scroller.css">
<link  rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/advancedscroller/plugin.css">
@endsection

@section('body')
<style>
  .hover_img { 
  opacity: 0.9; 
} 

.hover_img:hover { 
  opacity: 1; 
}
.g-bg-primary-opacity-0_8--before::after, .g-bg-primary-opacity-0_8--after::after {
    background-color: rgba(6, 6, 6, 0.6) !important;
}
  </style>

<div id="rev_slider_349_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="cleanproductshowcase" data-source="gallery" style="background:transparent;padding:0px;">
        <!-- START REVOLUTION SLIDER 5.4.3.3 fullscreen mode -->
        <div id="rev_slider_349_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.3.3">
          <ul>  <!-- SLIDE  -->
            @foreach($slider_products as $product)
              <li data-index="rs-97{{$loop->iteration}}" style="background-image: linear-gradient(to bottom, #ee1c24, #c00f20, #94051a, #690213, #410104);" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="assets/img/thumb1.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                <!-- MAIN IMAGE -->
                {{-- <img src="assets/img/transparent.png" data-bgcolor="#ffffff" style="background:linear-gradient(to bottom, #ee1c24, #c00f20, #94051a, #690213, #410104);" alt="Image description" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina> --}}
                <!-- LAYERS -->

                <!-- LAYER NR. 1 -->
                <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme"
                    id="slide-971-layer-19"
                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                    data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                    data-width="2000"
                    data-height="1"
                    data-whitespace="nowrap"

                    data-type="shape"
                    data-basealign="slide"
                    data-responsive_offset="on"

                    data-frames='[{"delay":200,"speed":1000,"frame":"0","from":"opacity:0;","to":"o:1;rZ:35;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                    data-textAlign="['inherit','inherit','inherit','inherit']"
                    data-paddingtop="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"

                    style="z-index: 5;background-color: linear-gradient(to bottom, #ee1c24, #c00f20, #94051a, #690213, #410104);;border-radius:50px 50px 50px 50px;"></div>

                <!-- LAYER NR. 2 -->
                <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme"
                    id="slide-971-layer-20"
                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                    data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                    data-width="2000"
                    data-height="1"
                    data-whitespace="nowrap"

                    data-type="shape"
                    data-basealign="slide"
                    data-responsive_offset="on"

                    data-frames='[{"delay":200,"speed":1000,"frame":"0","from":"opacity:0;","to":"o:1;rZ:-35;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                    data-textAlign="['inherit','inherit','inherit','inherit']"
                    data-paddingtop="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"

                    style="z-index: 6;background-color:rgba(145,152,170,0.25);border-radius:50px 50px 50px 50px;"></div>

                <!-- LAYER NR. 3 -->
                <div class="tp-caption   tp-resizeme tp-svg-layer rs-parallaxlevel-1"
                    id="slide-971-layer-16"
                    data-x="['left','left','left','left']" data-hoffset="['497','476','379','188']"
                    data-y="['top','top','top','top']" data-voffset="['-60','9','49','23']"
                    data-width="['600','400','300','300']"
                    data-height="['600','400','300','300']"
                    data-whitespace="nowrap"

                    data-type="svg"
                    data-svg_src="assets/img/ic_play_arrow_24px.svg"
                    data-svg_idle="sc:transparent;sw:0;sda:0;sdo:0;"
                    data-responsive_offset="on"

                    data-frames='[{"delay":200,"speed":1500,"frame":"0","from":"x:center;y:middle;opacity:0;","to":"o:1;rZ:20;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                    data-textAlign="['inherit','inherit','inherit','inherit']"
                    data-paddingtop="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"

                    style="z-index: 7; min-width: 600px; max-width: 600px; max-width: 600px; max-width: 600px; color: #f9f9f9;">
                  <div class="rs-looped rs-pendulum" data-easing="Linear.easeNone" data-startdeg="20" data-enddeg="-20" data-speed="20" data-origin="50% 50%"></div>
                </div>

                <!-- LAYER NR. 4 -->
                <div class="tp-caption   tp-resizeme rs-parallaxlevel-4"
                    id="slide-971-layer-3"
                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                    data-y="['top','top','top','top']" data-voffset="['50','50','100','50']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"

                    data-type="text"
                    data-responsive_offset="on"

                    data-frames='[{"delay":100,"speed":1500,"frame":"0","from":"y:100px;opacity:0;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                    data-textAlign="['inherit','inherit','inherit','inherit']"
                    data-paddingtop="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"

                    style="z-index: 8; white-space: nowrap; font-size: 18px; line-height: 22px; font-weight: 700; color: #fff; letter-spacing: 3px;font-family:Roboto;">BAGBOX
                </div>

                <!-- LAYER NR. 5 -->
                <div class="tp-caption   tp-resizeme tp-svg-layer rs-parallaxlevel-3"
                    id="slide-971-layer-22"
                    data-x="['left','left','left','left']" data-hoffset="['-18','22','22','4']"
                    data-y="['top','top','top','top']" data-voffset="['127','121','121','-13']"
                    data-width="['180','180','180','100']"
                    data-height="['280','180','180','100']"
                    data-whitespace="nowrap"

                    data-type="svg"
                    data-svg_src="assets/img/ic_play_arrow_24px.svg"
                    data-svg_idle="sc:transparent;sw:0;sda:0;sdo:0;"
                    data-responsive_offset="on"

                    data-frames='[{"delay":200,"speed":1500,"frame":"0","from":"x:center;y:middle;rZ:-90deg;opacity:0;","to":"o:1;rZ:50;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                    data-textAlign="['inherit','inherit','inherit','inherit']"
                    data-paddingtop="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"

                    style="z-index: 9; min-width: 180px; max-width: 180px; max-width: 280px; max-width: 280px; color: rgba(217,77,182,0.05);">
                  <div class="rs-looped rs-pendulum" data-easing="Linear.easeNone" data-startdeg="-40" data-enddeg="40" data-speed="20" data-origin="50% 50%"></div>
                </div>

                <!-- LAYER NR. 6 -->
                <div class="tp-caption   tp-resizeme rs-parallaxlevel-4"
                    id="slide-971-layer-1"
                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                    data-y="['top','top','top','top']" data-voffset="['100','100','150','89']"
                    data-fontsize="['180','130','90','60']"
                    data-lineheight="['160','120','80','70']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"

                    data-type="text"
                    data-responsive_offset="on"

                    data-frames='[{"delay":200,"speed":1500,"frame":"0","from":"y:100px;opacity:0;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                    data-textAlign="['center','center','center','center']"
                    data-paddingtop="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"

                    style="z-index: 10; white-space: nowrap; font-size: 180px; line-height: 160px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Bowlby One;text-shadow:0px 15px 30px rgba(0, 0, 0, 0.15);">
                </div>

                <!-- LAYER NR. 7 -->
                <div class="tp-caption   tp-resizeme tp-svg-layer rs-parallaxlevel-1"
                    id="slide-971-layer-15"
                    data-x="['left','left','left','left']" data-hoffset="['37','78','26','-118']"
                    data-y="['top','top','top','top']" data-voffset="['-5','-24','194','41']"
                    data-width="['1000','800','600','600']"
                    data-height="['1000','800','600','600']"
                    data-whitespace="nowrap"

                    data-type="svg"
                    data-svg_src="assets/img/ic_play_arrow_24px.svg"
                    data-svg_idle="sc:transparent;sw:0;sda:0;sdo:0;"
                    data-responsive_offset="on"

                    data-frames='[{"delay":100,"speed":2000,"frame":"0","from":"x:center;y:middle;opacity:0;","to":"o:1;rZ:75;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                    data-textAlign="['inherit','inherit','inherit','inherit']"
                    data-paddingtop="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"

                    style="z-index: 11; min-width: 1000px; max-width: 1000px; max-width: 1000px; max-width: 1000px; color: #f7f6fb;">
                  <div class="rs-looped rs-pendulum" data-easing="Linear.easeNone" data-startdeg="-10" data-enddeg="10" data-speed="15" data-origin="50% 50%"></div>
                </div>

                <!-- LAYER NR. 8 -->
                <div class="tp-caption   tp-resizeme tp-svg-layer rs-parallaxlevel-3"
                    id="slide-971-layer-11"
                    data-x="['left','left','left','left']" data-hoffset="['391','340','195','104']"
                    data-y="['top','top','top','top']" data-voffset="['199','149','174','126']"
                    data-width="['120','120','100','50']"
                    data-height="['120','120','100','50']"
                    data-whitespace="nowrap"

                    data-type="svg"
                    data-svg_src="assets/img/ic_play_arrow_24px.svg"
                    data-svg_idle="sc:transparent;sw:0;sda:0;sdo:0;"
                    data-responsive_offset="on"

                    data-frames='[{"delay":10,"speed":1500,"frame":"0","from":"x:center;y:middle;rZ:90deg;opacity:0;","to":"o:1;rZ:25;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                    data-textAlign="['inherit','inherit','inherit','inherit']"
                    data-paddingtop="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"

                    style="z-index: 12; min-width: 120px; max-width: 120px; max-width: 120px; max-width: 120px; color: #8500bd;">
                  <div class="rs-looped rs-pendulum" data-easing="Linear.easeNone" data-startdeg="30" data-enddeg="-30" data-speed="15" data-origin="50% 50%"></div>
                </div>

                <!-- LAYER NR. 9 -->
                <div class="tp-caption   tp-resizeme tp-svg-layer rs-parallaxlevel-1"
                    id="slide-971-layer-12"
                    data-x="['center','center','center','center']" data-hoffset="['341','259','278','212']"
                    data-y="['top','top','top','top']" data-voffset="['441','351','385','275']"
                    data-width="['100','70','70','50']"
                    data-height="['100','70','70','50']"
                    data-whitespace="nowrap"

                    data-type="svg"
                    data-svg_src="assets/img/ic_arrow_upward_24px.svg"
                    data-svg_idle="sc:transparent;sw:0;sda:0;sdo:0;"
                    data-responsive_offset="on"

                    data-frames='[{"delay":300,"speed":1500,"frame":"0","from":"x:center;y:middle;rZ:180deg;opacity:0;","to":"o:1;rZ:-45;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                    data-textAlign="['inherit','inherit','inherit','inherit']"
                    data-paddingtop="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"

                    style="z-index: 13; min-width: 100px; max-width: 100px; max-width: 100px; max-width: 100px; color: #140e68;">
                  <div class="rs-looped rs-slideloop" data-easing="Power1.easeInOut" data-speed="0.5" data-xs="0" data-xe="5" data-ys="0" data-ye="5"></div>
                </div>

                <!-- LAYER NR. 10 -->
                <div class="tp-caption   tp-resizeme"
                    id="slide-971-layer-8"
                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                    data-y="['top','top','top','top']" data-voffset="['120','122','120','67']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"

                    data-type="image"
                    data-responsive_offset="on"

                    data-frames='[{"delay":50,"speed":1000,"frame":"0","from":"y:100px;opacity:0;fbr:75%;","to":"o:1;fbr:100;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fbr:100;","ease":"Power3.easeInOut"}]'
                    data-textAlign="['inherit','inherit','inherit','inherit']"
                    data-paddingtop="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    onclick="window.location.href='{{URL::to('products'.'/'.$product->slug)}}';"
                    style="z-index: 14; cursor:pointer">
                    <img src="/product-images/{{$product->images->first()->image}}" alt="{{$product->product_name}}" data-ww="['600px','400','500px','400px']" data-hh="['600px','400px','500px','400px']" width="1200" height="1200" data-no-retina>
                </div>

                <!-- LAYER NR. 11 -->
                <div class="tp-caption   tp-resizeme tp-svg-layer rs-parallaxlevel-1"
                    id="slide-971-layer-18"
                    data-x="['left','left','left','left']" data-hoffset="['731','577','478','314']"
                    data-y="['top','top','top','top']" data-voffset="['504','380','451','350']"
                    data-width="['120','100','100','50']"
                    data-height="['120','100','100','50']"
                    data-whitespace="nowrap"

                    data-type="svg"
                    data-svg_src="assets/img/ic_play_arrow_24px.svg"
                    data-svg_idle="sc:transparent;sw:0;sda:0;sdo:0;"
                    data-responsive_offset="on"

                    data-frames='[{"delay":10,"speed":1500,"frame":"0","from":"x:center;y:middle;rZ:-90deg;opacity:0;","to":"o:1;rZ:100;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                    data-textAlign="['inherit','inherit','inherit','inherit']"
                    data-paddingtop="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"

                    style="z-index: 15; min-width: 120px; max-width: 120px; max-width: 120px; max-width: 120px; color: #d94db6;">
                  <div class="rs-looped rs-pendulum" data-easing="Linear.easeNone" data-startdeg="-30" data-enddeg="30" data-speed="15" data-origin="50% 50%"></div>
                </div>

                <!-- LAYER NR. 12 -->
        

                <!-- LAYER NR. 13 -->
                <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-1"
                    id="slide-971-layer-10"
                    data-x="['center','center','center','center']" data-hoffset="['220','161','181','150']"
                    data-y="['top','top','top','top']" data-voffset="['265','212','242','179']"
                    data-width="['200','150','150','120']"
                    data-height="['200','150','150','120']"
                    data-whitespace="nowrap"

                    data-type="shape"
                    data-responsive_offset="on"

                    data-frames='[{"delay":100,"speed":500,"frame":"0","from":"x:0px;y:50px;rX:0deg;rY:0deg;rZ:0deg;sX:0.5;sY:0.5;opacity:0;","to":"o:1;","ease":"Back.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                    data-textAlign="['inherit','inherit','inherit','inherit']"
                    data-paddingtop="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"

                    style="z-index: 17;background: linear-gradient(rgb(238, 28, 37) 0%, rgb(238, 28, 37) 50%, rgb(162, 5, 12) 100%);border-radius:100px 100px 100px 0px;"></div>

                <!-- LAYER NR. 14 -->
                <div class="tp-caption   tp-resizeme rs-parallaxlevel-1"
                    id="slide-971-layer-13"
                    data-x="['center','center','center','center']" data-hoffset="['220','160','180','150']"
                    data-y="['top','top','top','top']" data-voffset="['340','265','295','218']"
                    data-fontsize="['40','30','30','25']"
                    data-lineheight="['40','30','30','25']"
                    data-width="['201','150','150','120']"
                    data-height="none"
                    data-whitespace="['normal','nowrap','nowrap','nowrap']"

                    data-type="text"
                    data-responsive_offset="on"

                    data-frames='[{"delay":200,"speed":500,"frame":"0","from":"y:30px;sX:0.5;sY:0.5;opacity:0;","to":"o:1;","ease":"Back.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                    data-textAlign="['center','center','center','center']"
                    data-paddingtop="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"

                    style="z-index: 18; min-width: 201px; max-width: 201px; white-space: normal; font-size: 40px; line-height: 40px; font-weight: 900; color: #ffffff; letter-spacing: 0px;font-family:Roboto;">AED {{$product->price}}
                </div>

                <!-- LAYER NR. 15 -->
                <div class="tp-caption   tp-resizeme rs-parallaxlevel-1"
                    id="slide-971-layer-14"
                    data-x="['center','center','center','center']" data-hoffset="['220','160','180','150']"
                    data-y="['top','top','top','top']" data-voffset="['380','295','325','242']"
                    data-fontsize="['18','18','18','15']"
                    data-width="['201','150','150','120']"
                    data-height="none"
                    data-whitespace="['normal','nowrap','nowrap','nowrap']"

                    data-type="text"
                    data-responsive_offset="on"

                    data-frames='[{"delay":250,"speed":500,"frame":"0","from":"y:30px;sX:0.5;sY:0.5;opacity:0;","to":"o:1;","ease":"Back.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                    data-textAlign="['center','center','center','center']"
                    data-paddingtop="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"

                    style="z-index: 19; min-width: 201px; max-width: 201px; white-space: normal; font-size: 18px; line-height: 22px; font-weight: 500; color: rgba(255,255,255,0.5); letter-spacing: 0px;font-family:Roboto;">Incl. Taxes
                </div>

                <!-- LAYER NR. 16 -->
                {{-- <div class="tp-caption   tp-resizeme"
                    id="slide-971-layer-9"
                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                    data-y="['top','top','top','top']" data-voffset="['801','641','731','560']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"

                    data-type="text"
                    data-responsive_offset="on"

                    data-frames='[{"delay":500,"speed":1500,"frame":"0","from":"y:100px;opacity:0;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"200","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:linear-gradient(90deg, rgba(217,77,182,1) 0%, rgba(133,0,188,1) 100%);"}]'
                    data-textAlign="['inherit','inherit','inherit','inherit']"
                    data-paddingtop="[0,0,0,0]"
                    data-paddingright="[50,50,50,50]"
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[50,50,50,50]"
                    style="z-index: 20; white-space: nowrap; font-size: 18px; line-height: 70px; font-weight: 900; color: #ffffff; letter-spacing: 1px;font-family:Roboto;background:linear-gradient(90deg, rgb(160, 28, 35) 0%, rgb(238, 28, 37) 100%);;border-radius:35px 35px 35px 35px;-webkit-box-shadow:0px 15px 40px 0px rgba(0,0,0,0.1);-moz-box-shadow:0px 15px 40px 0px rgba(0,0,0,0.1);box-shadow:0px 15px 40px 0px rgba(0,0,0,0.1);cursor:pointer;">
                    
                    <i class="fa fa-shopping-cart" style=font-size:25px;margin-right:5px;"></i>
                    ENQUIRY NOW
                </div> --}}

                <!-- LAYER NR. 17 -->
                <div class="tp-caption   tp-resizeme tp-svg-layer rs-parallaxlevel-1"
                    id="slide-971-layer-21"
                    data-x="['right','right','right','right']" data-hoffset="['58','-9','-9','0']"
                    data-y="['bottom','bottom','bottom','bottom']" data-voffset="['127','98','98','96']"
                    data-width="['200','200','200','100']"
                    data-height="['200','200','200','100']"
                    data-whitespace="nowrap"

                    data-type="svg"
                    data-svg_src="assets/img/ic_play_arrow_24px.svg"
                    data-svg_idle="sc:transparent;sw:0;sda:0;sdo:0;"
                    data-responsive_offset="on"

                    data-frames='[{"delay":200,"speed":1500,"frame":"0","from":"x:center;y:middle;rZ:-90deg;opacity:0;","to":"o:1;rZ:200;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                    data-textAlign="['inherit','inherit','inherit','inherit']"
                    data-paddingtop="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"

                    style="z-index: 21; min-width: 200px; max-width: 200px; max-width: 200px; max-width: 200px; color: rgba(217,77,182,0.05);">
                  <div class="rs-looped rs-pendulum" data-easing="Linear.easeNone" data-startdeg="30" data-enddeg="-30" data-speed="15" data-origin="50% 50%"></div>
                </div>
              </li>
            @endforeach
          </ul>
          <div class="tp-bannertimer tp-bottom" style="height: 10px; background: rgb(216,215,220);"></div>
        </div>
      </div><!-- END REVOLUTION SLIDER -->

<!-- Hero Info #14 -->
{{-- <div class="row" style="background: #f7f7f7;">
  <div class="col-lg-7 g-mb-50 g-mb-0--lg">
    <div class="g-my-60 g-mx-60">
      <header class="u-heading-v2-3--bottom g-brd-primary g-mb-20">
        <h2 class="h3 u-heading-v2__title text-uppercase g-font-weight-600 mb-0">Bag Box Motorcycles Trading LLC</h2>
      </header>
      <p class="g-font-size-18">A purely dedicated company, providing food delivery asset solutions and serving more than 1000 clients across the globe.</p>
    
      <div class="row">
        <div class="col-sm-6">
          <ul class="list-unstyled g-color-gray-dark-v4 g-mb-30 g-mb-0--sm">
            <li class="d-flex g-mb-10 g-font-size-18">
              <i class="fa fa-check g-color-primary g-mt-5 g-mr-10"></i>
              world 1st illuminated concept with 3 interchangeable branding panels
            </li>
            <li class="d-flex g-mb-10 g-font-size-18">
              <i class="fa fa-check g-color-primary g-mt-5 g-mr-10"></i>
              Smooth edges for safety 
            </li>
            <li class="d-flex g-mb-10 g-font-size-18">
              <i class="fa fa-check g-color-primary g-mt-5 g-mr-10"></i>
              Clean, hygienic and washable
            </li>
            <li class="d-flex g-mb-10 g-font-size-18">
              <i class="fa fa-check g-color-primary g-mt-5 g-mr-10"></i>
              SKAD boxes are environmentally friendly 
            </li>
          </ul>
        </div>

        <div class="col-sm-6">
          <ul class="list-unstyled g-color-gray-dark-v4">
            <li class="d-flex g-mb-10 g-font-size-18">
              <i class="fa fa-check g-color-primary g-mt-5 g-mr-10"></i>
              Safe for delivering food as it is built by food grade material
            </li>
            <li class="d-flex g-mb-10 g-font-size-18">
              <i class="fa fa-check g-color-primary g-mt-5 g-mr-10"></i>
              Odour free 
            </li>
            <li class="d-flex g-mb-10 g-font-size-18">
              <i class="fa fa-check g-color-primary g-mt-5 g-mr-10"></i>
              Superior design 
            </li>
            <li class="d-flex g-mb-10 g-font-size-18">
              <i class="fa fa-check g-color-primary g-mt-5 g-mr-10"></i>
              Cost effective after sales
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <div class="col-lg-5 align-self-end">
    <img class="img-fluid" src="/box1.jpg" alt="Image Description">
  </div>
</div> --}}

<div class="g-pos-rel">
    <!-- Info Blocks -->
    <div class="row align-items-stretch no-gutters">
        
      
      <div class="col-lg-6">
        <!-- Article -->
        <article style="background:linear-gradient(to top, #ee1c24, #c00f20, #94051a, #690213, #410104)" class="h-100 text-left text-lg-right u-block-hover g-flex-middle g-color-white--hover g-bg-gray-dark-v1 g-transition-0_3 g-py-70 g-px-30 g-pr-150--lg">
        <div class="u-block-hover__additional--fade g-bg-size-cover g-bg-cover g-bg-primary-opacity-0_8--after g-pos-abs" data-bg-img-src="{{$infos[0]->image? '/info_images/'.$infos[0]->image :'/bagbox-banner-1.jpg'}}">
          </div>
          <div  class="g-flex-middle-item g-pos-rel g-z-index-1">
            <h3 class="h5 g-color-white g-font-weight-700 text-uppercase g-mb-12">{{$infos[0]->title}}</h3>
            <p class="g-color-white mb-0 g-font-size-20 g-font-weight-300">
               {{$infos[0]->description}}
                
            </p>
          </div>
        </article>
        <!-- End Article -->
      </div>
      
  
      <div class="col-lg-6">
        <!-- Article -->
        <article style="background:linear-gradient(to top, #ee1c24, #c00f20, #94051a, #690213, #410104)" class="h-100 u-block-hover g-flex-middle g-color-white--hover g-bg-black g-transition-0_3 g-py-70 g-px-30 g-pl-150--lg">
          <div class="u-block-hover__additional--fade g-bg-size-cover g-bg-cover g-bg-primary-opacity-0_8--after g-pos-abs" data-bg-img-src="{{$infos[1]->image? '/info_images/'.$infos[1]->image :'/bagbox-banner-1.jpg'}}">
          </div>
          <div class="g-flex-middle-item g-pos-rel g-z-index-1">
            <h3 class="h5 g-color-white g-font-weight-700 text-uppercase g-mb-12">{{$infos[1]->title}}</h3>
            <p class="g-color-white mb-0 g-font-size-20 g-font-weight-300">
                {{$infos[1]->description}}
            </p>
          </div>
        </article>
        <!-- End Article -->
      </div>
    </div>
  
    <div class="row align-items-stretch no-gutters">
      <div class="col-lg-6">
        <!-- Article -->
        <article style="background:linear-gradient(to bottom, #ee1c24, #c00f20, #94051a, #690213, #410104)" class="h-100 text-left text-lg-right u-block-hover g-flex-middle g-color-white--hover g-bg-black g-transition-0_3 g-py-70 g-px-30 g-pr-150--lg">
          <div class="u-block-hover__additional--fade g-bg-size-cover g-bg-cover g-bg-primary-opacity-0_8--after g-pos-abs" data-bg-img-src="{{$infos[2]->image? '/info_images/'.$infos[2]->image :'/bagbox-banner-1.jpg'}}">
          </div>
          <div class="g-flex-middle-item g-pos-rel g-z-index-1">
            <h3 class="h5 g-color-white g-font-weight-700 text-uppercase g-mb-12">{{$infos[2]->title}}</h3>
            <p class="g-color-white mb-0 g-font-size-20 g-font-weight-300" >
                {{$infos[2]->description}}
            </p>
          </div>
        </article>
        <!-- End Article -->
      </div>
  
      <div class="col-lg-6">
        <!-- Article -->
        <article style="background:linear-gradient(to bottom, #ee1c24, #c00f20, #94051a, #690213, #410104)" class="h-100 u-block-hover g-flex-middle g-color-white--hover g-bg-gray-dark-v1 g-transition-0_3 g-py-70 g-px-30 g-pl-150--lg">
          <div class="u-block-hover__additional--fade g-bg-size-cover g-bg-cover g-bg-primary-opacity-0_8--after g-pos-abs" data-bg-img-src="{{$infos[3]->image? '/info_images/'.$infos[3]->image :'/bagbox-banner-1.jpg'}}">
          </div>
          <div class="g-flex-middle-item g-pos-rel g-z-index-1">
            <h3 class="h5 g-color-white g-font-weight-700 text-uppercase g-mb-12">{{$infos[3]->title}}</h3>
            <p class="g-color-white mb-0 g-font-size-20 g-font-weight-300">{{$infos[3]->description}}</p>
          </div>
        </article>
        <!-- End Article -->
      </div>
    </div>
  
    <img class="g-hidden-md-down g-width-200 g-height-200 g-absolute-centered" src="/img/Iso.png" alt="Bagbox">
    <!-- End Info Blocks -->
  </div>
<div class="g-px-30 g-py-40" style="">
    <header class=" g-mb-10">
      <h2 class="text-center h3 u-heading-v2__title text-uppercase g-font-weight-600 mb-4">Featured Products</h2>
    </header>
  <div class="row">
       
      
        @foreach($featured_products as $product)
      
        <div class="col-md-6 col-lg-3 g-mb-30">
          <!-- Article -->
          <article class="text-center u-block-hover u-block-hover--uncroped g-bg-white">
            <!-- Article Image -->
            <div class="g-pos-rel">
                <a class="d-block u-block-hover__additional--jump g-mb-10" href="{{URL::to('products'.'/'.$product->slug)}}">

                  <img class="" style="height:18rem" src="/product-images/{{$product->images->first()->image}}" alt="{{$product->product_name}}">
                </a>
                <div class="u-ribbon-v1 g-width-55 g-bg-primary g-font-weight-600 g-font-size-17 g-top-0 g-left-0 p-0">
                    <span class="d-block g-color-white g-py-15">AED {{$product->price}}</span>
                    @if($product->sale_price)
                    <span class="d-block g-bg-white g-color-primary g-line-height-0_8 g-py-15">{{round((1-$product->sale_price/$product->regular_price)*100)}}%
                        <small class="g-font-size-12">off</small>
                    </span>
                    @endif
                </div>
            </div>
            <!-- End Article Image -->
      
            <!-- Article Content -->
            <div class="u-shadow-v24 g-pa-10">
              {{-- <header class="g-mb-15">
                <div class="js-rating d-inline-block g-color-primary g-font-size-11 g-mr-10" data-rating="5"></div>
              </header> --}}
      
              <!-- Article Info -->
              <h3 class="h4 g-mb-10">
              <a class="g-color-main g-color-primary--hover g-text-underline--none--hover" href="{{URL::to('products'.'/'.$product->slug)}}">{{strlen($product->product_name)>23?substr($product->product_name,0,20).'..':$product->product_name}}</a>
              </h3>
              <p>{{strlen($product->subtitle)>84?substr($product->subtitle,0,80).'..':$product->subtitle}}</p>
              <!-- End Article Info -->

              @if($product->is_retail)
                <!-- Products Icons -->
                <ul class="list-inline media-body" style="margin-bottom: 0;">
                                      
                  <li class="list-inline-item align-middle mx-0">
                    @if(!$product->quantity || $product->quantity > 0)
                      <button data-id="{{ $product->id }}" class="u-icon-v1 u-icon-size--sm g-color-gray-dark-v5 g-color-primary--hover g-font-size-15 rounded-circle addToCart"
                          style="background-color: transparent;border: none;cursor:pointer"
                          data-toggle="tooltip"
                          data-placement="top"
                          title="Add to Cart">
                          <i class="icon-finance-100 u-line-icon-pro" style="margin-top:3px"></i>
                      </button>
                    @endif
                  </li>
                </ul>
                <!-- End Products Icons -->
              @endif
            </div>
            <!-- End Article Content -->
          </article>
          <!-- End Article -->
        </div>
        @endforeach

    </div>
  <div class="text-center">
  <a href="/shop" class="btn btn-primary">View More</a>
  </div>
</div>

<div class="g-px-30 g-py-20" style="">
    <header class=" g-mb-10">
      <h2 class="text-center h3 u-heading-v2__title text-uppercase g-font-weight-600 mb-4">Recently Added Products</h2>
    </header>
    <div class="row">
        @foreach($recent_products as $product)
      
        <div class="col-md-6 col-lg-3 g-mb-30">
          <!-- Article -->
          <article class="text-center u-block-hover u-block-hover--uncroped g-bg-white">
            <!-- Article Image -->
            <div class="g-pos-rel">
                <a class="d-block u-block-hover__additional--jump g-mb-10" href="{{URL::to('products'.'/'.$product->slug)}}">

                <img class="" style="height:18rem" src="/product-images/{{$product->images->first()->image}}" alt="{{$product->product_name}}">
                </a>
                <div class="u-ribbon-v1 g-width-55 g-bg-primary g-font-weight-600 g-font-size-17 g-top-0 g-left-0 p-0">
                    <span class="d-block g-color-white g-py-15">AED {{$product->price}}</span>
                    @if($product->sale_price)
                    <span class="d-block g-bg-white g-color-primary g-line-height-0_8 g-py-15">{{round((1-$product->sale_price/$product->regular_price)*100)}}%
                        <small class="g-font-size-12">off</small>
                    </span>
                    @endif
                </div>
            </div>
            <!-- End Article Image -->
      
            <!-- Article Content -->
            <div class="u-shadow-v24 g-pa-10">
              {{-- <header class="g-mb-15">
                <div class="js-rating d-inline-block g-color-primary g-font-size-11 g-mr-10" data-rating="5"></div>
              </header> --}}
      
              <!-- Article Info -->
              <h3 class="h4 g-mb-10">
              <a class="g-color-main g-color-primary--hover g-text-underline--none--hover" href="{{URL::to('products'.'/'.$product->slug)}}">{{strlen($product->product_name)>23?substr($product->product_name,0,20).'..':$product->product_name}}</a>
              </h3>
               <p>{{strlen($product->subtitle)>84?substr($product->subtitle,0,80).'..':$product->subtitle}}</p>
              <!-- End Article Info -->
              @if($product->is_retail)
                <!-- Products Icons -->
                <ul class="list-inline media-body" style="margin-bottom: 0;">
                                      
                  <li class="list-inline-item align-middle mx-0">
                    @if(!$product->quantity || $product->quantity > 0)
                      <button data-id="{{ $product->id }}" class="u-icon-v1 u-icon-size--sm g-color-gray-dark-v5 g-color-primary--hover g-font-size-15 rounded-circle addToCart"
                          style="background-color: transparent;border: none;cursor:pointer"
                          data-toggle="tooltip"
                          data-placement="top"
                          title="Add to Cart">
                          <i class="icon-finance-100 u-line-icon-pro" style="margin-top:3px"></i>
                      </button>
                    @endif
                  </li>
                </ul>
                <!-- End Products Icons -->
              @endif
            </div>
            <!-- End Article Content -->
          </article>
          <!-- End Article -->
        </div>
        @endforeach

    </div>
    <div class="text-center">
        <a href="/shop" class="btn btn-primary">View More</a>
    </div>
</div>

<div class="g-overflow-hidden g-px-30 g-py-20">
    <header class=" g-mb-10">
        <h2 class="text-center h3 u-heading-v2__title text-uppercase g-font-weight-600 mb-0 g-py-10">Our Clients</h2>
    </header>
    <div class="row text-center mx-0 g-ml-minus-1 g-mb-minus-1">
        @foreach($clients as $c)
      <div class="col-sm-3 col-md-2 px-0">
        <div class="g-brd-left g-brd-bottom g-brd-gray-light-v4 g-py-30 g-px-15">
        <a href="{{$c->link?$c->link:'#'}}">
            <img class="img-fluid g-opacity-0_8--hover g-transition-0_2" style="height:5rem;" src="/client-images/{{$c->image}}" alt="{{$c->name}}">
        </a>
        </div>
      </div>
      @endforeach
  
  
    </div>
    <div class="text-center mt-3">
        <a href="/clients" class="btn btn-primary">View More</a>
    </div>  
    
</div>


  @endsection

@section('js')
<script  src="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsparallaxer.js"></script>
<script  src="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsscroller/scroller.js"></script>
<script  src="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/advancedscroller/plugin.js"></script>
<script >
    $(document).on('ready', function () {
        // initialization of parallax
        $('.dzsparallaxer').dzsparallaxer();
    });
    var revapi349,
      tpj = jQuery;

    tpj(document).ready(function () {
      if (tpj("#rev_slider_349_1").revolution == undefined) {
        revslider_showDoubleJqueryError("#rev_slider_349_1");
      } else {
        revapi349 = tpj("#rev_slider_349_1").show().revolution({
          sliderType: "standard",
          jsFileLocation: "//server.local/revslider/wp-content/plugins/revslider/public/assets/js/",
          sliderLayout: "fullscreen",
          dottedOverlay: "none",
          delay: 9000,
          navigation: {
            keyboardNavigation: "off",
            keyboard_direction: "horizontal",
            mouseScrollNavigation: "off",
            mouseScrollReverse: "default",
            onHoverStop: "off",
            arrows: {
              style: "new-bullet-bar",
              enable: true,
              hide_onmobile: true,
              hide_under: 778,
              hide_onleave: false,
              tmp: '<div class="tp-title-wrap">  	<div class="tp-arr-imgholder"></div>    <div class="tp-arr-img-over"></div> </div>',
              left: {
                h_align: "left",
                v_align: "center",
                h_offset: 30,
                v_offset: 0
              },
              right: {
                h_align: "right",
                v_align: "center",
                h_offset: 30,
                v_offset: 0
              }
            }
          },
          responsiveLevels: [1240, 1024, 778, 480],
          visibilityLevels: [1240, 1024, 778, 480],
          gridwidth: [1240, 1024, 778, 480],
          gridheight: [900, 768, 960, 720],
          lazyType: "none",
          parallax: {
            type: "scroll",
            origo: "slidercenter",
            speed: 1000,
            speedbg: 0,
            speedls: 1000,
            levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 51, 30],
          },
          shadow: 0,
          spinner: "spinner5",
          stopLoop: "off",
          stopAfterLoops: -1,
          stopAtSlide: -1,
          shuffle: "off",
          autoHeight: "off",
          fullScreenAutoWidth: "off",
          fullScreenAlignForce: "off",
          hideThumbsOnMobile: "off",
          hideSliderAtLimit: 0,
          hideCaptionAtLimit: 0,
          hideAllCaptionAtLilmit: 0,
          debugMode: false,
          fallbacks: {
            simplifyAll: "off",
            nextSlideOnWindowFocus: "off",
            disableFocusListener: false
          }
        });
      }

    });
 
</script>
@endsection