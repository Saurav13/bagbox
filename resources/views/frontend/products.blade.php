@extends('layouts.main')


@section('title', $meta_title)

@section('meta-description', $meta_description ? : null)
@section('meta-keywords', $meta_keywords ? : null)

@section('css')

  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsparallaxer.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsscroller/scroller.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/advancedscroller/plugin.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/animate.css">
@endsection

@section('body')
@php
$slug = $category ? $category->slug : 'catalog';

$parameters = request()->input();
$parameters['page'] = 1;
$parameters['category_slug'] = $slug;
@endphp

{{-- <article class="text-center g-color-white g-overflow-hidden">
    <div class="g-min-height-250 g-flex-middle  g-bg-bluegray-opacity-0_3--after g-transition-0_5" data-bg-img-src="url(/bagbox-banner.jpg)" style="background-image: url(/bagbox-banner.jpg);background-repeat: no-repeat;
    background-size: auto;">
    </div>
</article> --}}

<section class="dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll loaded dzsprx-readyall" data-options="{direction: 'fromtop', animation_duration: 25, direction: 'reverse'}">
        <div class="divimage dzsparallaxer--target w-100 g-bg-pos-top-center g-bg-cover g-bg-black-opacity-0_2--after" style="height: 140%; background-image: url(/bagbox-banner.jpg); transform: translate3d(0px, -87.3959px, 0px);"></div>

        <div class="container g-color-white g-pt-100 g-pb-40">
          <div class="g-mb-10">
             
            @if($category)
              <span class="d-block g-color-white-opacity-0_8 g-font-weight-300 g-font-size-20">BAGBOX</span>

              <h1 class="g-color-white g-font-size-50 g-font-size-50--md g-line-height-1_2 mb-0">{{ $category->cat_name }}</h1>
              {{-- <p class="g-color-white g-font-weight-600 g-font-size-20 text-uppercase">Since, 2007</p> --}}

            @else
              <span class="d-block g-color-white-opacity-0_8 g-font-weight-300 g-font-size-20">Delivery Asset Partner</span>
                <h3 class="g-color-white g-font-size-50 g-font-size-50--md g-line-height-1_2 mb-0">BAGBOX</h3>
                <p class="g-color-white g-font-weight-600 g-font-size-20 text-uppercase">Since, 2007</p>
            @endif
          </div>

          <div class="d-flex justify-content-end">
                <ul class="u-list-inline g-bg-gray-dark-v1 g-font-weight-300 g-rounded-50 g-py-5 g-px-20">
                  <li class="list-inline-item g-mr-5">
                    <a class="u-link-v5 g-color-white g-color-primary--hover" href="/">Home</a>
                    <i class="g-color-white-opacity-0_5 g-ml-5">/</i>
                  </li>
                  <li class="list-inline-item g-mr-5">
                    <a class="u-link-v5 g-color-white g-color-primary--hover" href="/shop">All Products</a>
                    <i class="g-color-white-opacity-0_5 g-ml-5">/</i>
                  </li>
                  <li class="list-inline-item g-color-primary g-font-weight-400">
                    <span>{{$category?$category->cat_name:''}}</span>
                  </li>
                </ul>
              </div>
        </div>
</section>


{{-- <header class=" g-mb-10">
        <h2 class="text-center h3 u-heading-v2__title text-uppercase g-font-weight-600 mb-0">Our Products</h2>
</header> --}}

{{-- <div class="g-px-30">
    <div class="row">
        @foreach($products as $product)
        
        <div class="col-md-6 col-lg-3 g-mb-30">
            <!-- Article -->
            <article class="u-block-hover u-block-hover--uncroped  text-center g-bg-white">
            <!-- Article Image -->
            <div class="g-pos-rel">
                    <a class="d-block u-block-hover__additional--jump g-mb-10" href="#">
                            
                            <img class="w-100" src="p2.jpg" alt="Product">
                    </a>
                <div class="u-ribbon-v1 g-width-55 g-bg-primary g-font-weight-600 g-font-size-17 g-top-0 g-left-0 p-0">
                <span class="d-block g-color-white g-py-15">AED {{$product->price}}</span>
                <!--@if($product->sale_price)-->
                <!--<span class="d-block g-bg-white g-color-primary g-line-height-0_8 g-py-15">{{round((1-$product->sale_price/$product->regular_price)*100)}}%-->
                <!--    <small class="g-font-size-12">off</small>-->
                <!--</span>-->
                <!--@endif-->
                </div>
            </div>
            <!-- End Article Image -->
        
            <!-- Article Content -->
            <div class="u-shadow-v24 g-pa-5">
              
        
                <!-- Article Info -->
                <h3 class="h4 g-mb-10">
                <a class="g-color-main g-color-primary--hover g-text-underline--none--hover" href="#">{{strlen($product->product_name)>23?substr($product->product_name,0,20).'..':$product->product_name}}</a>
                </h3>
                <a href="#" class="btn btn-sm u-btn-primary u-btn-3d g-font-weight-600 g-letter-spacing-0_5 text-uppercase g-brd-2 g-rounded-50 g-mr-10 g-px-10 g-py-5 g-mb-15"> Watch Video
                        <i class="fa fa-video-camera g-ml-3"></i>
                      </a>
                <!-- End Article Info -->
            </div>
            <!-- End Article Content -->
            </article>
            <!-- End Article -->
        </div>
        @endforeach
    </div>
</div> --}}



  <div class="r_container" >
        <div class="row">
          <!-- Content -->
          <div class="col-md-9 order-md-2">
            <div class="g-pl-15--lg">
              <!-- Filters -->
              <div class="d-flex justify-content-end align-items-center g-brd-bottom g-brd-gray-light-v4 g-pt-10 g-pb-20">
            

                <!-- Sort By -->
                <div class="g-mr-60">
                  <!--<h2 class="h6 align-middle d-inline-block g-font-weight-400 text-uppercase g-pos-rel g-top-1 mb-0" style=" font-size: 1rem;">Sort by:</h2>-->
                  @php
                    $sort1 = $parameters;
                    $sort1['sort'] = 'low_high_price';

                    $sort2 = $parameters;
                    $sort2['sort'] = 'high_low_price';
                  @endphp
                  <!-- Secondary Button -->
                  <!--<div class="d-inline-block btn-group g-line-height-1_2">-->
                  <!--  <button type="button" style="font-size:1rem" class="btn btn-secondary dropdown-toggle h6 align-middle g-brd-none g-color-gray-dark-v5 g-color-black--hover g-bg-transparent text-uppercase g-font-weight-300 g-pa-0 g-pl-10 g-ma-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">-->
                  <!--    {{ request()->input('sort','Price') }}-->
                  <!--  </button>-->
                  <!--  <div class="dropdown-menu rounded-0">-->
                  <!--    <a class="dropdown-item g-color-gray-dark-v4 g-font-weight-300" href="{{ route('shop',$sort1) }}">Price low to high</a>-->
                  <!--    <a class="dropdown-item g-color-gray-dark-v4 g-font-weight-300" href="{{ route('shop',$sort2) }}">price high to low</a>-->
                  <!--  </div>-->
                  <!--</div>-->
                  <!-- End Secondary Button -->
                </div>
                <!-- End Sort By -->

               
              </div>
              
              @if(Request::get('search'))
                <p> Displaying Products for "{{Request::get('search')}}"</p>
              @endif
              <!-- End Filters -->
              <!-- Products -->
              
                  @if($products->count()==0)
                    <h4 style="text-align:center;margin-top:1rem">No Search Result</h4>
                    <p style="text-align:center">We're sorry. We cannot find any matches for your search term.</p>
                    <p style="text-align:center; font-size:60px;color:#ee1c29"><i class="icon-education-045 u-line-icon-pro"></i></p>
                  @endif
              <div class="row">
                  
                    @foreach($products as $product)
        
                    <div class="col-md-6 col-lg-4 g-mb-30">
                        <!-- Article -->
                        <article class="u-block-hover u-block-hover--uncroped  text-center g-bg-white">
                        <!-- Article Image -->
                        <div class="g-pos-rel">
                        <a class="d-block u-block-hover__additional--jump g-mb-10" href="{{URL::to('products'.'/'.$product->slug)}}">
                                        
                        <img  style="height:18rem" src="/product-images/{{$product->images->first()->image}}" alt="{{$product->product_name}}">
                                </a>
                            <div class="u-ribbon-v1 g-width-55 g-bg-primary g-font-weight-600 g-font-size-17 g-top-0 g-left-0 p-0">
                            <span class="d-block g-color-white g-py-15">AED {{$product->price}}</span>
                            @if($product->sale_price)
                            <span class="d-block g-bg-white g-color-primary g-line-height-0_8 g-py-15">{{round((1-$product->sale_price/$product->regular_price)*100)}}%
                                <small class="g-font-size-12">off</small>
                            </span>
                            @endif
                            </div>
                        </div>
                        <!-- End Article Image -->
                    
                        <!-- Article Content -->
                        <div class="u-shadow-v24 g-pa-5">
                          
                    
                            <!-- Article Info -->
                          <h3 class="h4 g-mb-10">
                            <a class="g-color-main g-color-primary--hover g-text-underline--none--hover" href="{{URL::to('products'.'/'.$product->slug)}}">{{strlen($product->product_name)>23?substr($product->product_name,0,20).'..':$product->product_name}}</a>
                          </h3>
                          <p>{{strlen($product->subtitle)>84?substr($product->subtitle,0,75).'..':$product->subtitle}}</p>

                            {{-- @if($product->video_link)
                            <a href="#videoModal"  data-modal-target="#videoModal" data-modal-effect="fadein" videosource="{{$product->video_link}}" id="watchVideo{{$product->id}}" class="btn btn-sm u-btn-primary u-btn-3d g-font-weight-600 g-letter-spacing-0_5 text-uppercase g-brd-2 g-rounded-50 g-mr-10 g-px-10 g-py-5 g-mb-15"> Watch Video
                                    <i class="fa fa-video-camera g-ml-3"></i>
                            </a>
                            @endif --}}
                            <!-- End Article Info -->
                            @if($product->is_retail)
                              <!-- Products Icons -->
                              <ul class="list-inline media-body" style="margin-bottom: 0;">
                                      
                                <li class="list-inline-item align-middle mx-0">
                                  @if(!$product->quantity || $product->quantity > 0)
                                    <button data-id="{{ $product->id }}" class="u-icon-v1 u-icon-size--sm g-color-gray-dark-v5 g-color-primary--hover g-font-size-15 rounded-circle addToCart"
                                        style="background-color: transparent;border: none;cursor:pointer"
                                        data-toggle="tooltip"
                                        data-placement="top"
                                        title="Add to Cart">
                                        <i class="icon-finance-100 u-line-icon-pro" style="margin-top:3px"></i>
                                    </button>
                                  @endif
                                </li>
                              </ul>
                              <!-- End Products Icons -->
                            @endif
                        </div>
                        
                        <!-- End Article Content -->
                        </article>
                        <!-- End Article -->
                    </div>
                    @endforeach
              </div>
              <!-- End Products -->
              
              <!-- Pagination -->
              <nav class="g-mb-60" aria-label="Page Navigation" style="text-align:center">
                  {{ $products->appends($_GET)->links('frontend.partials.paginate') }}
              </nav>
              <!-- End Pagination -->

              @if($category && $category->description)
                @if($products->count()!=0)
                    <hr class="g-mb-40">
                @endif

                <div id="details" class="container-fluid g-px-0">
                  <div class="row no-gutters g-min-height-100vh">
                    <div class="col-md-12 align-self-center">
                      <div class="mx-auto  g-pb-100">
                        {!! $category->description !!}
      
                      </div>
                    </div>
                  </div>
                </div>
              @endif
              <div id="stickyblock-end"></div>
            </div>
          </div>
          <!-- End Content -->

          <!-- Filters -->
          <div class="col-sm-12 col-md-3 order-md-1 g-brd-right--lg g-brd-gray-light-v4 g-pt-30">
           
              <form action="{{ route('shop',$slug) }}" id="stickyblock-start" class="FilterForm">
                  
                @if(request()->has('search'))
                    <input type="hidden" name='search' value="{{ request()->get('search') }}" />
                @endif

                  @if(request()->has('filter'))
                      <input type="hidden" name='filter' value="{{ request()->get('filter') }}" />
                  @endif

                <div style="" class=" g-pt-30 g-mt-20 js-sticky-block g-sticky-block--lg card card_product" data-type="responsive" data-start-point="#stickyblock-start" data-end-point="#stickyblock-end">
                  <div class="">
                    
                   
                      <!-- Categories -->
                      <div class="g-mb-20">
                          <h3 class="h5 mb-3 g-font-weight-600">CATEGORIES:</h3>
    
                          <ul class="list-unstyled">
                            
                            
                            @foreach ($categories as $c)
                              @php
                                  $parameters['category_slug'] = $c->slug;
                              @endphp
                              <li class="my-3">
                                  <a href="{{ route('shop',$parameters) }}" style="font-size:1rem" class=" g-font-weight-500 text-uppercase d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover">
                                      {{ $c->cat_name }}
                                      <span class="float-right g-font-size-14">{{ $c->products()->count() }}</span></a>
                                  </a>
                              </li>
                            @endforeach
                          </ul>
                        </div>
                        <!-- End Categories -->

                    <!-- Pricing -->
                    <div class="">
                      <!--<h3 class="h5 mb-3 g-font-weight-600">PRICE</h3>-->

                      @php
                          $min = 1;
                          $max = \App\Product::max('regular_price')                               
                      @endphp
                      <div class="text-center">
                          <!--<span class="d-block g-color-primary mb-4">$ (<span id="rangeSliderAmount3">0</span>)</span>-->
                          <!--<div id="rangeSlider1" class="u-slider-v1-3"-->
                          <!--    data-result-container="rangeSliderAmount3"-->
                          <!--    data-range="true"-->
                          <!--    data-default="{{ request()->input('price.min', $min).','. request()->input('price.max', $max) }}"-->
                          <!--    data-min="1"-->
                          <!--    data-max="{{ $max }}">-->
                          <!--</div>-->
                          <!--<input name="price[min]" id="priceMin" type="hidden">-->
                          <!--<input name="price[max]" id="priceMax" type="hidden">-->
                      </div>
                    </div>
                    <!-- End Pricing -->

                    <!--<button class="btn btn-block u-btn-primary g-font-size-12 text-uppercase g-py-12 g-px-25" type="submit" id="filterButton" disabled>Filter</button>-->
                    <br><br>
                  </div>
                </div>

              </form>
          </div>
          <!-- End Filters -->
        </div>

       
  </div>
      <!-- End Products -->

      <!-- Call to Action -->
      <div class="g-bg-primary">
        <div class="container g-py-20">
          <div class="row justify-content-center">
            <div class="col-md-4 mx-auto g-py-20">
              <!-- Media -->
              <div class="media g-px-50--lg">
                <i class="d-flex g-color-white g-font-size-40 g-pos-rel g-top-3 mr-4 icon-real-estate-048 u-line-icon-pro"></i>
                <div class="media-body">
                  <span class="d-block g-color-white g-font-weight-500 g-font-size-17 text-uppercase">Free Shipping</span>
                  <span class="d-block g-color-white-opacity-0_8">In 2-3 Days</span>
                </div>
              </div>
              <!-- End Media -->
            </div>

            <div class="col-md-4 mx-auto g-brd-x--md g-brd-white-opacity-0_3 g-py-20">
              <!-- Media -->
              <div class="media g-px-50--lg">
                <i class="d-flex g-color-white g-font-size-40 g-pos-rel g-top-3 mr-4 icon-real-estate-040 u-line-icon-pro"></i>
                <div class="media-body">
                  <span class="d-block g-color-white g-font-weight-500 g-font-size-17 text-uppercase">Free Returns</span>
                  <span class="d-block g-color-white-opacity-0_8">No Questions Asked</span>
                </div>
              </div>
              <!-- End Media -->
            </div>

            <div class="col-md-4 mx-auto g-py-20">
              <!-- Media -->
              <div class="media g-px-50--lg">
                <i class="d-flex g-color-white g-font-size-40 g-pos-rel g-top-3 mr-4 icon-hotel-restaurant-062 u-line-icon-pro"></i>
                <div class="media-body text-left">
                  <span class="d-block g-color-white g-font-weight-500 g-font-size-17 text-uppercase">Free 24</span>
                  <span class="d-block g-color-white-opacity-0_8">Days Storage</span>
                </div>
              </div>
              <!-- End Media -->
            </div>
          </div>
        </div>
      </div>

@endsection





@section('js')

<script src="/frontend-assets/main-assets/assets/vendor/jquery-ui/ui/widget.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/jquery-ui/ui/widgets/menu.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/jquery-ui/ui/widgets/mouse.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/jquery-ui/ui/widgets/slider.js"></script>
<script src="/frontend-assets/main-assets/assets/js/components/hs.slider.js"></script>
<script src="/frontend-assets/main-assets/assets/js/components/hs.sticky-block.js"></script>

<script src="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsparallaxer.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsscroller/scroller.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/advancedscroller/plugin.js"></script>

<script>

 
  $('document').ready(function(){
    // $.HSCore.components.HSScrollBar.init($('.js-scrollbar'));
        $('#filterButton').removeAttr('disabled');
      $.HSCore.components.HSModalWindow.init('[data-modal-target]');
        
      setTimeout(function() {
          $.HSCore.components.HSStickyBlock.init('.js-sticky-block');
      }, 1);

      
      $('.FilterForm').on('submit',function(e){
          e.preventDefault();

          var value = $( "#rangeSlider1" ).slider( "values" );
          $('#priceMin').val(value[0]);
          $('#priceMax').val(value[1]);

          this.submit();
      });
  
      $('[id*="watchVideo"]').click(function(){
          console.log("asd")
          var source=$(this).attr('videosource');
          $('#ytplayer').removeAttr("src");
          console.log($('#ytplayer').attr(source))

          

        var modal = new Custombox.modal({

                  content: {
                      effect: 'fadein',
                      target: '#videoModal'
                  }
              });
          
              // Open
              modal.open();
            //   $('#ytplayer').playVideo();

      })

  })
  </script>
<script>
    $(document).ready(function(){
      $.HSCore.components.HSSlider.init('#rangeSlider1');
      $.HSCore.components.HSCarousel.init('.js-carousel');


      
    
    });
</script>
@endsection