@extends('layouts.main')

@section('css')
<link  rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsparallaxer.css">
<link  rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsscroller/scroller.css">
<link  rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/advancedscroller/plugin.css">
@endsection

@section('body')
<style>
  <style>
  .hover_img { 
  opacity: 0.7; 
} 

.hover_img:hover { 
  opacity: 1; 
}
.g-bg-primary-opacity-0_8--before::after, .g-bg-primary-opacity-0_8--after::after {
    background-color: rgba(6, 6, 6, 0.6) !important;
}
.outer-bottom .gyges{
  background:transparent !important;
}
  </style>
<div id="rev_slider_474_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" style="background-color: #111111; background-image: linear-gradient(to bottom, #ee1c24, #c00f20, #94051a, #690213, #410104); background-repeat: no-repeat; background-size: cover; background-position: center center; padding: 0; margin: 0 auto;"
data-alias="showcase-carousel"
data-source="gallery">
<div id="rev_slider_474_1" class="rev_slider fullwidthabanner" style="display: none;"
  data-version="5.4.1">
<ul>
 <li data-index="rs-1636"
     data-transition="fade"
     data-slotamount="7"
     data-hideafterloop="0"
     data-hideslideonmobile="off"
     data-easein="default"
     data-easeout="default"
     data-masterspeed="300"
     data-thumb="/3.png"
     data-rotate="0"
     data-opacity="0.7"
     data-saveperformance="off"
     data-title="Number One">
   <img class="rev-slidebg " src="/3.png" alt="Image description"
        data-bgposition="center center"
        data-bgfit="contain"
        data-bgrepeat="no-repeat">
 </li>
 <li data-index="rs-1637"
     data-transition="fade"
     data-slotamount="7"
     data-hideafterloop="0"
     data-hideslideonmobile="off"
     data-easein="default"
     data-easeout="default"
     data-masterspeed="300"
     data-thumb="/2.png"
     data-rotate="0"
     data-saveperformance="off"
     data-title="Number Two">
   <img class="rev-slidebg" src="/2.png" alt="Image description"
        data-bgposition="center center"
        data-bgfit="contain"
        data-bgrepeat="no-repeat">
 </li>
 <li data-index="rs-1638"
     data-transition="fade"
     data-slotamount="7"
     data-hideafterloop="0"
     data-hideslideonmobile="off"
     data-easein="default"
     data-easeout="default"
     data-masterspeed="300"
     data-thumb="/37.png"
     data-rotate="0"
     data-saveperformance="off"
     data-title="Number Three">
   <img class="rev-slidebg" src="/37.png" alt="Image description"
        data-bgposition="center center"
        data-bgfit="contain"
        data-bgrepeat="no-repeat">
 </li>
 <li data-index="rs-1639"
     data-transition="fade"
     data-slotamount="7"
     data-hideafterloop="0"
     data-hideslideonmobile="off"
     data-easein="default"
     data-easeout="default"
     data-masterspeed="300"
     data-thumb="/10.png"
     data-rotate="0"
     data-saveperformance="off"
     data-title="Number Four">
   <img class="rev-slidebg" src="/10.png" alt="Image description"
        data-bgposition="center center"
        data-bgfit="contain"
        data-bgrepeat="no-repeat">
 </li>
 <li data-index="rs-1640"
     data-transition="fade"
     data-slotamount="7"
     data-hideafterloop="0"
     data-hideslideonmobile="off"
     data-easein="default"
     data-easeout="default"
     data-masterspeed="300"
     data-thumb="/6.png"
     data-rotate="0"
     data-saveperformance="off"
     data-title="Number Five">
   <img class="rev-slidebg" src="/6.png" alt="Image description"
        data-bgposition="center center"
        data-bgfit="contain"
        data-bgrepeat="no-repeat">
 </li>
 <li data-index="rs-1641"
     data-transition="fade"
     data-slotamount="7"
     data-hideafterloop="0"
     data-hideslideonmobile="off"
     data-easein="default"
     data-easeout="default"
     data-masterspeed="300"
     data-thumb="/10.png"
     data-rotate="0"
     data-saveperformance="off"
     data-title="Number Six">
   <img class="rev-slidebg" src="/10.png" alt="Image description"
        data-bgposition="center center"
        data-bgfit="contain"
        data-bgrepeat="no-repeat">
 </li>
 <li data-index="rs-1642"
     data-transition="fade"
     data-slotamount="7"
     data-hideafterloop="0"
     data-hideslideonmobile="off"
     data-easein="default"
     data-easeout="default"
     data-masterspeed="300"
     data-thumb="/4.png"
     data-rotate="0"
     data-saveperformance="off"
     data-title="Number Seven">
   <img class="rev-slidebg" src="/4.png" alt="Image description"
        data-bgposition="center center"
        data-bgfit="contain"
        data-bgrepeat="no-repeat">
 </li>
 <li data-index="rs-1643"
     data-transition="fade"
     data-slotamount="7"
     data-hideafterloop="0"
     data-hideslideonmobile="off"
     data-easein="default"
     data-easeout="default"
     data-masterspeed="300"
     data-thumb="/2.png"
     data-rotate="0"
     data-saveperformance="off"
     data-title="Number Eight">
   <img class="rev-slidebg" src="/2.png" alt="Image description"
        data-bgposition="center center"
        data-bgfit="contain"
        data-bgrepeat="no-repeat">
 </li>
 <li data-index="rs-1644"
     data-transition="fade"
     data-slotamount="7"
     data-hideafterloop="0"
     data-hideslideonmobile="off"
     data-easein="default"
     data-easeout="default"
     data-masterspeed="300"
     data-thumb="/3.png"
     data-rotate="0"
     data-saveperformance="off"
     data-title="Number Nine">
   <img class="rev-slidebg" src="/3.png" alt="Image description"
        data-bgposition="center center"
        data-bgfit="contain"
        data-bgrepeat="no-repeat">
 </li>
</ul>
</div>
</div>

<div class="g-pos-rel">
  <!-- Info Blocks -->
  <div class="row align-items-stretch no-gutters">
    <div class="col-lg-6">
      <!-- Article -->
      <article style="background:linear-gradient(to top, #ee1c24, #c00f20, #94051a, #690213, #410104)" class="h-100 text-left text-lg-right u-block-hover g-flex-middle g-color-white--hover g-bg-gray-dark-v1 g-transition-0_3 g-py-70 g-px-30 g-pr-150--lg">
        <div class="u-block-hover__additional--fade g-bg-size-cover g-bg-cover g-bg-primary-opacity-0_8--after g-pos-abs" data-bg-img-src="/bagbox-banner-1.jpg">
        </div>
        <div  class="g-flex-middle-item g-pos-rel g-z-index-1">
          <h3 class="h5 g-color-white g-font-weight-700 text-uppercase g-mb-12">What We Are?</h3>
          <p class="g-color-white mb-0 g-font-size-20 g-font-weight-300">
              We are purely dedicated company, providing <strong> Food Delivery Asset Solutions</strong> and serving more than <strong>1000 clients</strong> across the globe.
              
          </p>
        </div>
      </article>
      <!-- End Article -->
    </div>

    <div class="col-lg-6">
      <!-- Article -->
      <article style="background:linear-gradient(to top, #ee1c24, #c00f20, #94051a, #690213, #410104)" class="h-100 u-block-hover g-flex-middle g-color-white--hover g-bg-black g-transition-0_3 g-py-70 g-px-30 g-pl-150--lg">
        <div class="u-block-hover__additional--fade g-bg-size-cover g-bg-cover g-bg-primary-opacity-0_8--after g-pos-abs" data-bg-img-src="/bagbox-banner-2.jpg">
        </div>
        <div class="g-flex-middle-item g-pos-rel g-z-index-1">
          <h3 class="h5 g-color-white g-font-weight-700 text-uppercase g-mb-12">Why Our Product?</h3>
          <p class="g-color-white mb-0 g-font-size-20 g-font-weight-300">
              We provide <strong> world  1st illuminated concept</strong> with 3 interchangeable branding panels,
              We are safe for delivering food as it is built by food grade material
          </p>
        </div>
      </article>
      <!-- End Article -->
    </div>
  </div>

  <div class="row align-items-stretch no-gutters">
    <div class="col-lg-6">
      <!-- Article -->
      <article style="background:linear-gradient(to bottom, #ee1c24, #c00f20, #94051a, #690213, #410104)" class="h-100 text-left text-lg-right u-block-hover g-flex-middle g-color-white--hover g-bg-black g-transition-0_3 g-py-70 g-px-30 g-pr-150--lg">
        <div class="u-block-hover__additional--fade g-bg-size-cover g-bg-cover g-bg-primary-opacity-0_8--after g-pos-abs" data-bg-img-src="/bagbox-banner-2.jpg">
        </div>
        <div class="g-flex-middle-item g-pos-rel g-z-index-1">
          <h3 class="h5 g-color-white g-font-weight-700 text-uppercase g-mb-12">Why Us?</h3>
          <p class="g-color-white mb-0 g-font-size-20 g-font-weight-300" >
              We understand customers requirements provide solutions and share our recommendation based on our experience.
          </p>
        </div>
      </article>
      <!-- End Article -->
    </div>

    <div class="col-lg-6">
      <!-- Article -->
      <article style="background:linear-gradient(to bottom, #ee1c24, #c00f20, #94051a, #690213, #410104)" class="h-100 u-block-hover g-flex-middle g-color-white--hover g-bg-gray-dark-v1 g-transition-0_3 g-py-70 g-px-30 g-pl-150--lg">
        <div class="u-block-hover__additional--fade g-bg-size-cover g-bg-cover g-bg-primary-opacity-0_8--after g-pos-abs" data-bg-img-src="/bagbox-banner.jpg">
        </div>
        <div class="g-flex-middle-item g-pos-rel g-z-index-1">
          <h3 class="h5 g-color-white g-font-weight-700 text-uppercase g-mb-12">We love our customers</h3>
          <p class="g-color-white mb-0 g-font-size-20 g-font-weight-300">This is where we sit down, grab a cup of coffee and dial in the details. Understanding the task at hand and ironing out the wrinkles is key.</p>
        </div>
      </article>
      <!-- End Article -->
    </div>
  </div>

  <img class="g-hidden-md-down g-width-200 g-height-200 g-absolute-centered" src="/2.png" alt="Image Description">
  <!-- End Info Blocks -->
</div>
<div class="g-px-30 g-py-40" style="">
  <header class=" g-mb-10">
    <h2 class="text-center h3 u-heading-v2__title text-uppercase g-font-weight-600 mb-0">Featured Products</h2>
  </header>
<div class="row">
     
    
      @foreach($featured_products as $product)
    
      <div class="col-md-6 col-lg-3 g-mb-30">
        <!-- Article -->
        <article class="text-center u-block-hover u-block-hover--uncroped g-bg-white">
          <!-- Article Image -->
          <div class="g-pos-rel">
              <a class="d-block u-block-hover__additional--jump g-mb-10" href="{{URL::to('products'.'/'.$product->slug)}}">

              <img class="" style="height:18rem" src="/product-images/{{$product->images->first()->image}}" alt="Image Description">
              </a>
              <div class="u-ribbon-v1 g-width-55 g-bg-primary g-font-weight-600 g-font-size-17 g-top-0 g-left-0 p-0">
                  <span class="d-block g-color-white g-py-15">${{$product->price}}</span>
                  @if($product->sale_price)
                  <span class="d-block g-bg-white g-color-primary g-line-height-0_8 g-py-15">{{round((1-$product->sale_price/$product->regular_price)*100)}}%
                      <small class="g-font-size-12">off</small>
                  </span>
                  @endif
              </div>
          </div>
          <!-- End Article Image -->
    
          <!-- Article Content -->
          <div class="u-shadow-v24 g-pa-10">
            {{-- <header class="g-mb-15">
              <div class="js-rating d-inline-block g-color-primary g-font-size-11 g-mr-10" data-rating="5"></div>
            </header> --}}
    
            <!-- Article Info -->
            <h3 class="h4 g-mb-10">
            <a class="g-color-main g-color-primary--hover g-text-underline--none--hover" href="{{URL::to('products'.'/'.$product->slug)}}">{{strlen($product->product_name)>23?substr($product->product_name,0,20).'..':$product->product_name}}</a>
            </h3>
          <p>{{$product->subtitle}}</p>
            <!-- End Article Info -->
          </div>
          <!-- End Article Content -->
        </article>
        <!-- End Article -->
      </div>
      @endforeach

  </div>
<div class="text-center">
<a href="/shop" class="btn btn-primary">View More</a>
</div>
</div>

<div class="g-px-30 g-py-20" style="">
  <header class=" g-mb-10">
    <h2 class="text-center h3 u-heading-v2__title text-uppercase g-font-weight-600 mb-0">Recently Added Products</h2>
  </header>
  <div class="row">
      @foreach($recent_products as $product)
    
      <div class="col-md-6 col-lg-3 g-mb-30">
        <!-- Article -->
        <article class="text-center u-block-hover u-block-hover--uncroped g-bg-white">
          <!-- Article Image -->
          <div class="g-pos-rel">
              <a class="d-block u-block-hover__additional--jump g-mb-10" href="{{URL::to('products'.'/'.$product->slug)}}">

              <img class="" style="height:18rem" src="/product-images/{{$product->images->first()->image}}" alt="Image Description">
              </a>
              <div class="u-ribbon-v1 g-width-55 g-bg-primary g-font-weight-600 g-font-size-17 g-top-0 g-left-0 p-0">
                  <span class="d-block g-color-white g-py-15">${{$product->price}}</span>
                  @if($product->sale_price)
                  <span class="d-block g-bg-white g-color-primary g-line-height-0_8 g-py-15">{{round((1-$product->sale_price/$product->regular_price)*100)}}%
                      <small class="g-font-size-12">off</small>
                  </span>
                  @endif
              </div>
          </div>
          <!-- End Article Image -->
    
          <!-- Article Content -->
          <div class="u-shadow-v24 g-pa-10">
            {{-- <header class="g-mb-15">
              <div class="js-rating d-inline-block g-color-primary g-font-size-11 g-mr-10" data-rating="5"></div>
            </header> --}}
    
            <!-- Article Info -->
            <h3 class="h4 g-mb-10">
            <a class="g-color-main g-color-primary--hover g-text-underline--none--hover" href="{{URL::to('products'.'/'.$product->slug)}}">{{strlen($product->product_name)>23?substr($product->product_name,0,20).'..':$product->product_name}}</a>
            </h3>
          <p>{{strlen($product->subtitle)>84?substr($product->subtitle,0,80).'..':$product->subtitle}}</p>
            <!-- End Article Info -->
          </div>
          <!-- End Article Content -->
        </article>
        <!-- End Article -->
      </div>
      @endforeach

  </div>
  <div class="text-center">
      <a href="/shop" class="btn btn-primary">View More</a>
  </div>
</div>

  @endsection

@section('js')
<script  src="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsparallaxer.js"></script>
<script  src="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsscroller/scroller.js"></script>
<script  src="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/advancedscroller/plugin.js"></script>
<script >
    $(document).on('ready', function () {
        // initialization of parallax
        $('.dzsparallaxer').dzsparallaxer();
    });
    var tpj = jQuery;

    var revapi474;
    tpj(document).ready(function () {
      if (tpj('#rev_slider_474_1').revolution == undefined) {
        revslider_showDoubleJqueryError('#rev_slider_474_1');
      } else {
        revapi474 = tpj('#rev_slider_474_1').show().revolution({
          sliderType: 'carousel',
          jsFileLocation: 'revolution/js/',
          sliderLayout: 'fullwidth',
          dottedOverlay: 'none',
          delay: 9000,
          navigation: {
            keyboardNavigation: 'off',
            keyboard_direction: 'horizontal',
            mouseScrollNavigation: 'off',
            mouseScrollReverse: 'default',
            onHoverStop: 'off',
            arrows: {
              style: 'erinyen',
              enable: true,
              hide_onmobile: false,
              hide_onleave: false,
              tmp: '<div class="tp-title-wrap">' +
              '<div class="tp-arr-imgholder"></div>' +
              '<div class="tp-arr-img-over"></div>' +
              '<span class="tp-arr-titleholder"></span>' +
              '</div>',
              left: {
                h_align: 'left',
                v_align: 'center',
                h_offset: 30,
                v_offset: 0
              },
              right: {
                h_align: 'right',
                v_align: 'center',
                h_offset: 30,
                v_offset: 0
              }
            }
            ,
            thumbnails: {
              style: 'gyges',
              enable: true,
              width: 60,
              height: 60,
              min_width: 60,
              wrapper_padding: 20,
              wrapper_color: '#00000000',
              wrapper_opacity: '0.15',
              tmp: '<span class="tp-thumb-img-wrap">' +
              '<span class="tp-thumb-image"></span>' +
              '</span>',
              visibleAmount: 9,
              hide_onmobile: false,
              hide_onleave: false,
              direction: 'horizontal',
              span: true,
              position: 'outer-bottom',
              space: 10,
              h_align: 'center',
              v_align: 'bottom',
              h_offset: 0,
              v_offset: 0
            }
          },
          carousel: {
            maxRotation: 65,
            vary_rotation: 'on',
            minScale: 55,
            vary_scale: 'off',
            horizontal_align: 'center',
            vertical_align: 'center',
            fadeout: 'on',
            vary_fade: 'on',
            maxVisibleItems: 5,
            infinity: 'off',
            space: -150,
            stretch: 'off'
          },
          visibilityLevels: [1240, 1024, 778, 480],
          gridwidth: 600,
          gridheight: 600,
          lazyType: 'none',
          shadow: 0,
          spinner: 'off',
          stopLoop: 'on',
          stopAfterLoops: 0,
          stopAtSlide: 1,
          shuffle: 'off',
          autoHeight: 'off',
          disableProgressBar: 'on',
          hideThumbsOnMobile: 'off',
          hideSliderAtLimit: 0,
          hideCaptionAtLimit: 0,
          hideAllCaptionAtLilmit: 0,
          debugMode: false,
          fallbacks: {
            simplifyAll: 'off',
            nextSlideOnWindowFocus: 'off',
            disableFocusListener: false
          }
        });
      }
    });

</script>
@endsection