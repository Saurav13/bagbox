@extends('layouts.main')

@section('title','Order '.$product->product_name)
@section('meta-description', $product->meta_description)
@section('meta-keywords', $product->meta_keywords)

@section('body')

<style>
        .hide{
                display: none;
        }
</style>
<div class="container g-pt-20">			
        <div class="text-center">
                {{-- <img src="{{asset('product-images/180x180'.'/'.$image->image)}}" alt="Shrestha Digital Printers"  style="height:150px;" /> --}}
        <h3>Place your mass order for "{{$product->product_name}}"</h3>
      
        </div>
</div>	
        <div class="container g-pt-20">
        <form action="{{route('general.order')}}" method="POST" enctype="multipart/form-data" class="g-brd-around g-brd-gray-light-v4 g-pa-20 g-mb-30">
                {{csrf_field()}}
                <h3 class="g-mb-15">Order Information</h3>
                <p> *Note: If the following values do not match your requirement please specify your requirement in the message box below</p>
                
                <hr class="g-brd-gray-light-v4 g-my-10">

                
                <div class="row">
                    <div class="form-group col-md-6 g-mb-20">
                                <label class="g-mb-10 g-font-size-20" for="inputGroup1_1">Your Name</label>
                                <input id="inputGroup1_1" class="form-control form-control-md rounded-0" name="order[name]" value="{{ old('order.name') }}" type="text" placeholder="Enter name" required>
                                @if ($errors->has('name'))
                                <span style="color:red">{{ $errors->first('name') }}</span>
                                @endif
                    </div>
                    
                    <div class="form-group col-md-6 g-mb-20">
                            <label class="g-mb-10 g-font-size-20" for="inputGroup1_1">Contact Number</label>
                            <input id="inputGroup1_1" class="form-control form-control-md rounded-0" type="text" placeholder="Enter a reachable phone number..." name="order[phone]" value="{{ old('order.phone') }}" required>
                            @if ($errors->has('phone'))
                            <span style="color:red">{{ $errors->first('phone') }}</span>
                            @endif
                            <small class="form-text text-muted g-font-size-default g-mt-10">We'll never share your number with anyone else.</small>
                          
                 </div>
                    <div class="form-group col-md-6 g-mb-20">
                            <label class="g-mb-10 g-font-size-20" for="inputGroup1_1">Address</label>
                            <input id="inputGroup1_1" class="form-control form-control-md rounded-0" type="text" placeholder="Enter your address..." name="order[address]" value="{{ old('order.address') }}" required>
                            @if ($errors->has('address'))
                            <span style="color:red">{{ $errors->first('address') }}</span>
                            @endif
                        </div>
                    <div class="form-group col-md-6 g-mb-20">
                            <label class="g-mb-10 g-font-size-20" for="inputGroup1_1">Email Address</label>
                            <input id="inputGroup1_1" class="form-control form-control-md rounded-0" name="order[email]" value="{{ old('order.email') }}" type="email" placeholder="Enter email">
                            @if ($errors->has('address'))
                            <span style="color:red">{{ $errors->first('address') }}</span>
                            @endif
                            <small class="form-text text-muted g-font-size-default g-mt-10">We'll never share your email with anyone else.</small>
                   
                        </div>

                <div class="form-group col-md-6 g-mb-20">
                                <label class="g-mb-10 g-font-size-20" for="inputGroup1_1">Order Quantity</label>
                                <input id="inputGroup1_1" class="form-control form-control-md rounded-0" type="number" name="order[quantity]" value="{{ old('order.quantity')?old('order.quantity'):1 }}" min="1" required>
                                @if ($errors->has('quantity'))
                                <span style="color:red">{{ $errors->first('quantity') }}</span>
                                @endif
                                 
                </div>
                    <div class="form-group col-md-12 g-mb-20">
                            <label class="g-mb-10 g-font-size-20" for="inputGroup2_2">Message</label>
                            <textarea id="inputGroup2_2" name="order[message]" class="form-control form-control-md rounded-0" rows="7" placeholder="A short description of what service you would like to use">{{ old('order.message') }}</textarea>
                            @if ($errors->has('message'))
                            <span style="color:red">{{ $errors->first('message') }}</span>
                            @endif
                        </div>
                </div>
                <input type="hidden" value="{{ $product->id }}" name="order[product_id]">

                <div class="form-group g-mb-20 text-center">
                        <input type="submit" class="btn btn-md btn-primary" value="Place Order" id="submit"/>
                </div>
        </form>
    
</div>
@endsection