@extends('layouts.main')

@section('title','Checkout')


@section('body')


  <!-- Breadcrumbs -->
  <section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
      <div class="container">
          <ul class="u-list-inline">
              <li class="list-inline-item g-mr-5">
              <a class="u-link-v5 g-color-text" href="{{URL::to('home')}}">Home</a>
              <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
              </li>
              <li class="list-inline-item g-color-primary">
              <span>Checkout</span>
              </li>
          </ul>
      </div>
  </section>
  <!-- End Breadcrumbs -->

      <!-- Checkout Form -->
    <div class="container g-pt-40 g-pb-70">
        <form class="js-validate js-step-form" id="shippingForm"  data-progress-id="#stepFormProgress" data-steps-id="#stepFormSteps">
          <div class="g-mb-60">
            <!-- Step Titles -->
            <ul id="stepFormProgress" class="js-step-progress row justify-content-center list-inline text-center g-font-size-17 mb-0">
              <li class="col-3 list-inline-item g-mb-20 g-mb-0--sm">
                <span class="d-block u-icon-v2 u-icon-size--sm g-rounded-50x g-brd-primary g-color-primary g-color-white--parent-active g-bg-primary--active g-color-white--checked g-bg-primary--checked mx-auto mb-3">
                  <i class="g-font-style-normal g-font-weight-700 g-hide-check">1</i>
                  <i class="fa fa-check g-show-check"></i>
                </span>
                <h4 class="g-font-size-16 text-uppercase mb-0">Shopping Cart</h4>
              </li>

              <li class="col-3 list-inline-item g-mb-20 g-mb-0--sm">
                <span class="d-block u-icon-v2 u-icon-size--sm g-rounded-50x g-brd-gray-light-v2 g-color-gray-dark-v5 g-brd-primary--active g-color-white--parent-active g-bg-primary--active g-color-white--checked g-bg-primary--checked mx-auto mb-3">
                  <i class="g-font-style-normal g-font-weight-700 g-hide-check">2</i>
                  <i class="fa fa-check g-show-check"></i>
                </span>
                <h4 class="g-font-size-16 text-uppercase mb-0">Shipping</h4>
              </li>

              <li class="col-3 list-inline-item">
                <span class="d-block u-icon-v2 u-icon-size--sm g-rounded-50x g-brd-gray-light-v2 g-color-gray-dark-v5 g-brd-primary--active g-color-white--parent-active g-bg-primary--active g-color-white--checked g-bg-primary--checked mx-auto mb-3">
                  <i class="g-font-style-normal g-font-weight-700 g-hide-check">3</i>
                  <i class="fa fa-check g-show-check"></i>
                </span>
                <h4 class="g-font-size-16 text-uppercase mb-0">Payment &amp; Review</h4>
              </li>
            </ul>
            <!-- End Step Titles -->
          </div>

          <div id="stepFormSteps">
            <!-- Shopping Cart -->
            <div id="step1" class="active">
              <div class="row">
                  <div class="col-md-12 g-mb-30">
                      <!-- Products Block -->
                      <div class="g-overflow-x-scroll g-overflow-x-visible--lg">
                          
                          <table class="text-center w-100">
                              <thead class="h6 g-brd-bottom g-brd-gray-light-v3 g-color-black text-uppercase">
                                  <tr>
                                      <th class="g-font-weight-400 text-left g-pb-20">Product</th>
                                      <th class="g-font-weight-400 g-width-130 g-pb-20">Price</th>
                                      <th class="g-font-weight-400 g-width-150 g-pb-20">Qty</th>
                                      <th class="g-font-weight-400 g-width-130 g-pb-20">Total</th>
                                      <th></th>
                                  </tr>
                              </thead>

                              <tbody>
                                  @foreach ($cart_items as $item)
                                      <!-- Item-->
                                      <tr class="g-brd-bottom g-brd-gray-light-v3">
                                          <td class="text-left g-py-25">
                                              <img class="d-inline-block g-width-100 mr-4" src="{{ $item->options['image'] }}" alt="{{ $item->name }}">
                                              <div class="d-inline-block align-middle">
                                                  <h4 class="h6 g-color-black">{{ $item->name }}</h4>
                                                  <ul class="list-unstyled g-color-gray-dark-v4 g-font-size-12 g-line-height-1_6 mb-0">
                                                      {{-- <li>Color: Black</li>
                                                      <li>Size: MD</li> --}}
                                                  </ul>
                                              </div>
                                          </td>
                                          <td class="g-color-gray-dark-v2 g-font-size-13">AED {{ $item->price }}</td>
                                          <td class="g-color-gray-dark-v2 g-font-size-13">{{ $item->qty }}</td>
                                          <td class="text-right g-color-black">
                                              <span class="g-color-gray-dark-v2 g-font-size-13 mr-4">AED <span id="item-total-{{ $item->rowId }}">{{ $item->price * $item->qty }}</span></span>
                                          </td>
                                      </tr>
                                      <!-- End Item-->
                                  @endforeach
                                  <tr class="border-bottom">
                                    <td class="table-image"></td>
                                    <td style="padding: 20px;"></td>
                                    <td class="small-caps table-bg"
                                        style="text-align: right">Vat Inclusive (5%)</td>
                                    <td style="text-align: right">AED {{ round(5*Cart::subtotal(null,null,'')/100,2) }}</td>
                                    <td class="column-spacer"></td>
                                    <td></td>
                                  </tr>
                                  <tr class="border-bottom">
                                      <td class="table-image"></td>
                                      <td style="padding: 20px;"></td>
                                      <td class="small-caps table-bg"
                                          style="text-align: right">Sub Total</td>
                                      <td style="text-align: right">AED {{ Cart::subtotal() }}</td>
                                      <td class="column-spacer"></td>
                                      <td></td>
                                  </tr>
                              </tbody>
                          </table>
                      </div>
                      <!-- End Products Block -->
                      <a href="/cart" class="btn u-btn-outline-black g-brd-gray-light-v1 g-bg-black--hover g-font-size-13 text-uppercase g-py-15 mt-4">Update Shopping Cart</a>
                      <button style="float: right;" class="btn u-btn-primary g-font-size-13 text-uppercase g-py-15 mt-4" type="button" data-next-step="#step2">Proceed to Checkout</button>

                  </div>
              </div>
            </div>

            <!-- Shipping -->
            <div id="step2">
              <h3 style="font-size:20px;margin-bottom: 1rem;">Enter Shipping Details:</h3>
              @csrf
              <div class="row">
                <div class="col-md-8 g-mb-30">
                  <div class="row">
                    <div class="col-sm-6 g-mb-20">
                      <div class="form-group">
                        <label class="d-block g-color-gray-dark-v2 g-font-size-13">Name</label>
                        <input id="inputGroup4" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" name="shipping[name]" type="text" placeholder="Name" required data-msg="This field is mandatory" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1" value="{{ Auth::check() ? Auth::user()->name : '' }}">
                      </div>
                    </div>

                    <div class="col-sm-6 g-mb-20">
                      <div class="form-group">
                        <label class="d-block g-color-gray-dark-v2 g-font-size-13">Email Address</label>

                        @if(!Auth::check())
                        <input id="inputGroup6" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" name="shipping[email]" type="email" placeholder="Email" required data-msg="This field is mandatory" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1" value="{{ Auth::check() ? Auth::user()->email : '' }}">
                        @else
                        <input id="inputGroup6" readonly class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" name="shipping[email]" type="email" placeholder="Email" required data-msg="This field is mandatory" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1" value="{{ Auth::check() ? Auth::user()->email : '' }}">

                        @endif
                      </div>
                    </div>
                  </div>

                  <div class="row">

                    <div class="col-sm-6 g-mb-20">
                      <div class="form-group">
                        <label class="d-block g-color-gray-dark-v2 g-font-size-13">Phone Number</label>
                        <input id="inputGroup5" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" name="shipping[phone]" type="text" placeholder="Contact Number" required data-msg="This field is mandatory" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1" value="{{ Auth::check() ? Auth::user()->phone : '' }}">
                      </div>
                    </div>
                    <div class="col-sm-6 g-mb-20">
                      <div class="form-group">
                        <label class="d-block g-color-gray-dark-v2 g-font-size-13">Company</label>
                        <input id="inputGroup7" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" name="shipping[company]" type="text" placeholder="Company">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    
                    <div class="col-sm-6 g-mb-20">
                      <div class="form-group">
                        <label class="d-block g-color-gray-dark-v2 g-font-size-13">City</label>
                        <select class="js-custom-select u-select-v1 g-brd-gray-light-v2 g-color-gray-light-v1 g-py-12" style="width: 100%;height: 53px" autocomplete="off" id="shipping_city" name="shipping[city]" data-placeholder="Choose your City" data-open-icon="fa fa-angle-down" data-close-icon="fa fa-angle-up" required data-msg="This field is mandatory" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1">
                          <option selected disabled value="">Select your city</option>
                          @foreach ($delivery_zones as $zone)
                            <option value="{{ $zone->name }}" data-charge="{{ $zone->delivery_charge }}">{{ $zone->name}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>

                    <div class="col-sm-6 g-mb-20">
                      <div class="form-group">
                        <label class="d-block g-color-gray-dark-v2 g-font-size-13">Address</label>
                        <input id="inputGroup8" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" name="shipping[address]" type="text" placeholder="House#, ABC Road" required data-msg="This field is mandatory" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1" value="{{ Auth::check() ? Auth::user()->address : '' }}">
                      </div>
                    </div>

                  </div>

                  {{-- <hr class="g-mb-50">

                  <h3 style="font-size: 20px;">Please Select delivery method:</h3>
                  <div class="row">
                    <div class="col-md-12 g-mb-30">
                      <!-- Payment Methods -->
                      <ul class="list-unstyled mb-5">
                        <li class="g-brd-bottom g-brd-gray-light-v3 pb-3 my-3">
                          <label class="form-check-inline u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
                              <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="shipping_method" value="standard_delivery" type="radio" checked required>
                                <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                  <i class="fa" data-check-icon="&#xf00c"></i>
                                </span>
                                Standard Delivery - AED {{ $shippingCharge }}
                          </label>

                          <label class="form-check-inline u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="shipping_method" value="pick_up" type="radio"  required>
                              <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                <i class="fa" data-check-icon="&#xf00c"></i>
                              </span>
                              Pick Up at our Store
                          </label>
                          

                        </li>
                      </ul>
                      <!-- End Payment Methods -->
                    </div>
                  </div> --}}

               

                  <button style="float: right;" class="btn u-btn-primary g-font-size-13 text-uppercase g-px-40 g-py-15" type="submit" id="submitButton">Proceed to Payment</button>
                </div>

                <div class="col-md-4 g-mb-30">
                  <!-- Order Summary -->
                  <div class="g-bg-gray-light-v5 g-pa-20 g-pb-50 mb-4">
                    <h4 class="h6 text-uppercase mb-3">Order summary</h4>

                    <!-- Accordion -->
                
                    <!-- End Accordion -->

                    <div class="d-flex justify-content-between mb-2">
                      <span class="g-color-black">Subtotal</span>
                      <span class="g-color-black g-font-weight-300">AED {{ number_format(Cart::subtotal(null,null,'')) }}</span>
                    </div>
                    <div class="d-flex justify-content-between mb-2">
                        <span class="g-color-black">Delivery Charge</span>
                        <span class="g-color-black g-font-weight-300">AED <span id="shippingCharge">0</span></span>
                    </div>
                    <hr>

                    <div class="d-flex justify-content-between">
                      <span class="g-color-black">Order Total</span>
                      <span class="g-color-black g-font-weight-300">AED <span id="total">{{ Cart::subtotal(null,null,'') }}</span></span>
                    </div>
                  </div>
                  <!-- End Order Summary -->
                </div>
              </div>

              
            </div>
            <!-- End Shipping -->
          </div>
        </form>
    </div>
      <!-- End Checkout Form -->

      <!-- Call to Action -->
    <div class="g-bg-primary">
        <div class="container g-py-20">
          <div class="row justify-content-center">
            <div class="col-md-4 mx-auto g-py-20">
              <!-- Media -->
              <div class="media g-px-50--lg">
                <i class="d-flex g-color-white g-font-size-40 g-pos-rel g-top-3 mr-4 icon-real-estate-048 u-line-icon-pro"></i>
                <div class="media-body">
                  <span class="d-block g-color-white g-font-weight-500 g-font-size-17 text-uppercase">Free Shipping</span>
                  <span class="d-block g-color-white-opacity-0_8">In 2-3 Days</span>
                </div>
              </div>
              <!-- End Media -->
            </div>

            <div class="col-md-4 mx-auto g-brd-x--md g-brd-white-opacity-0_3 g-py-20">
              <!-- Media -->
              <div class="media g-px-50--lg">
                <i class="d-flex g-color-white g-font-size-40 g-pos-rel g-top-3 mr-4 icon-real-estate-040 u-line-icon-pro"></i>
                <div class="media-body">
                  <span class="d-block g-color-white g-font-weight-500 g-font-size-17 text-uppercase">Free Returns</span>
                  <span class="d-block g-color-white-opacity-0_8">No Questions Asked</span>
                </div>
              </div>
              <!-- End Media -->
            </div>

            <div class="col-md-4 mx-auto g-py-20">
              <!-- Media -->
              <div class="media g-px-50--lg">
                <i class="d-flex g-color-white g-font-size-40 g-pos-rel g-top-3 mr-4 icon-hotel-restaurant-062 u-line-icon-pro"></i>
                <div class="media-body text-left">
                  <span class="d-block g-color-white g-font-weight-500 g-font-size-17 text-uppercase">Free 24</span>
                  <span class="d-block g-color-white-opacity-0_8">Days Storage</span>
                </div>
              </div>
              <!-- End Media -->
            </div>
          </div>
        </div>
    </div>
@endsection


@section('js')
<script src="/frontend-assets/main-assets/assets/js/components/hs.step-form.js"></script>
<script src="/frontend-assets/main-assets/assets/js/components/hs.go-to.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/jquery-validation/dist/jquery.validate.min.js"></script>
<script>
    $(document).ready(function(){
      
        $.HSCore.components.HSStepForm.init('.js-step-form');
        // $.HSCore.components.HSValidation.init('.js-validate');
        var amount = parseFloat('{{ Cart::subTotal(null,null,'') }}');
        var shippingCharge = 0;

        $("input[name='shipping_method']").change(function(){
          var method = $(this).val();

          if(method == 'standard_delivery'){
            $('#shippingCharge').text(shippingCharge);
            $('#total').text(amount+shippingCharge);
          }
          else if(method == 'pick_up'){
            $('#shippingCharge').text(0);
            $('#total').text(amount);
          }
        });

        $("#shipping_city").change(function(){
          shippingCharge = parseFloat($(this).find(':selected').data('charge'));
          shippingCharge = shippingCharge ? shippingCharge : 0;
          $('#shippingCharge').text(shippingCharge);
          $('#total').text(amount+shippingCharge);
        });

        $('#shippingForm').validate({
          submitHandler: function(form) {

            $('#submitButton').attr('disabled','disabled');
            var formData = new FormData($('#shippingForm').get(0));

            $.ajax({
                type: "POST",
                url: "{{ URL::to('checkout') }}",
                data: formData,
                processData: false,
                contentType: false,
                dataType: 'json',
                success: function(response)
                {
                  $('#submitButton').removeAttr('disabled');
                  location.href = "{{ url('payment') }}/"+response.orderId;
                },
                error:function(response)
                {
                  $('#submitButton').removeAttr('disabled');

                  showErrors(response);
                }
            });
          }
        });
    })
</script>

@endsection