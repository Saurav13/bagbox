@extends('layouts.email')

@section('body')
    
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
        <tr> 
            <td align="center" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding-top: 25px;">
                <h2 style="font-size: 30px; font-weight: 800; line-height: 36px; color: #333333; margin: 0;"> Your order has been {{ $order->status }}! </h2> 
            </td>
        </tr>
        <tr>
            <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding-top: 10px;">
                
                <p style="text-align:center;font-size: 12px;margin-bottom: 0px;margin-top: 0px;color: #454282;">Order Number: {{ $order->id }}</p>
            </td>
        </tr>
        <tr>
            <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding-top: 25px;"> 
                <p style="font-size: 18px; font-weight: 800; line-height: 24px; color: #333333;">
                    Dear {{ $billing->name }},
                </p>
                <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #333333;">
                    Your Order has been {{ $order->status }}. Thank you for purchasing with us.
                </p>

                <p style="font-size: 16px; font-weight: 600; line-height: 24px; color: #333333;">                              
                Thanks,<br>
                MUX Team
                </p>
            </td>
        </tr>
    </table>
@endsection
