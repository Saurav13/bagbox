@extends('layouts.email')

@section('body')
    
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
        <tr> 
            <td align="center" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding-top: 5px;">
                <h2 style="font-size: 30px; font-weight: 800; line-height: 36px; color: #333333; margin: 0;"> Thank You For Your Order! </h2> 
            </td>
        </tr>
        <tr>
            <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding-top: 10px;">
                
                <p style="text-align:center;font-size: 12px;margin-bottom: 0px;margin-top: 0px;color: #454282;">Order Number: {{ $invoice->id }}</p>
            </td>
        </tr>
        <tr>
            <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding-top: 25px;"> 
                <p style="font-size: 12px;margin-bottom: 0px;margin-top: 0px;">
                    <span style="font-weight:800">{{ $billing->name }}</span><br>
                    <span>{{ $billing->email }}</span><br>
                    <span>{{ $billing->address }}</span><br>
                    <span>{{ $billing->city }}</span><br>
                    <span>{{ $billing->phone }}</span>
                    @if($billing->company)
                        <br><span>{{ $billing->company }}</span>
                    @endif
                </p>
            </td>
        </tr>
    </table>

    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
        <tr style="padding-top: 20px;">
            <td><h4 style="margin-bottom:5px">Invoice</h4></td>
        </tr>
        <tr>
            <td align="left" >
                <table class="table" cellspacing="0" cellpadding="0" border="0" width="100%">
                    <thead>
                        <tr>
                            <th width="5%" style="border-bottom: 2px solid #e3ebf3;border-top: 1px solid #e3ebf3;">#</th>
                            <th width="45%" style="border-bottom: 2px solid #e3ebf3;border-top: 1px solid #e3ebf3;">Item &amp; Description</th>
                            <th width="20%" style="border-bottom: 2px solid #e3ebf3;border-top: 1px solid #e3ebf3;">Rate</th>
                            <th width="10%" style="border-bottom: 2px solid #e3ebf3;border-top: 1px solid #e3ebf3;">QTY</th>
                            <th width="20%" style="border-bottom: 2px solid #e3ebf3;border-top: 1px solid #e3ebf3;">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $i = 1;
                        @endphp
                        @foreach ($invoice->items()->where('type','Product')->get() as $item)
                            
                            <tr>
                                <th width="5%" align="center">{{ $i++ }}</th>
                                <td width="45%" align="center">{{ $item->product_name }}</td>
                                <td width="20%" align="center">AED {{ $item->amount }}</td>
                                <td width="10%" align="center">{{ $item->quantity }}</td>
                                <td width="20%" align="center">AED {{ number_format($item->quantity*$item->amount) }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" style="padding-top: 20px;">
                
                <table class="table" cellspacing="0" cellpadding="0" border="0" width="100%">
                    @foreach ($invoice->items()->where('type','!=','Product')->get() as $item)
                        <tr>
                            <td width="40%"></td>
                            <td width="40%" align="right" style="font-weight:800;border-top: 3px solid #eeeeee; border-bottom: 3px solid #eeeeee;">
                                {{ $item->product_name }}                       
                            </td>
                            <td width="20%" align="left" style="font-weight:800;border-top: 3px solid #eeeeee; border-bottom: 3px solid #eeeeee;">
                                AED {{ number_format($item->quantity*$item->amount) }}
                            </td>
                        </tr>
                    @endforeach
                    <tr>
                        <td width="40%"></td>
                        <td width="40%" align="right" style="font-weight:800;border-top: 3px solid #eeeeee; border-bottom: 3px solid #eeeeee;">
                            Total                       
                        </td>
                        <td width="20%" align="left" style="font-weight:800;border-top: 3px solid #eeeeee; border-bottom: 3px solid #eeeeee;">
                            AED {{ number_format($invoice->amount) }}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr style="padding-top: 20px;">
            <td align="left"><h4 style="margin-bottom:5px">Shipping & Payment:</h4></td>
        </tr>    
        <tr>
            
            <td>
                <p style="margin: 10px 0;">Shipping Method: 
                    @if($invoice->shipping_method == 'standard_delivery') Standard Delivery
                    @elseif($invoice->shipping_method == 'pick_up') Pick up
                    @endif
                </p>
                <p style="margin: 10px 0;">Payment Method: 
                    @if($invoice->payment_method == 'COD') Cash on Delivery
                    @else Online Payment by {{ $invoice->payment_method }}
                    @endif
                </p>
            </td>
        </tr>
        
    </table>
@endsection
