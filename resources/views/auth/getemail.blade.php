<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Title -->
  <title>Login</title>

  <!-- Required Meta Tags Always Come First -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <!-- Favicon -->
  <link rel="shortcut icon" href="/frontend-assets/main-assets/favicon.ico">
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- CSS Global Compulsory -->
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/icon-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/animate.css">

  <!-- CSS Unify -->
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/css/unify-core.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/css/unify-components.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/css/unify-globals.css">

  <!-- CSS Customization -->
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/css/custom.css">
    <style>
      .help-block{
          color: red;
      }
    </style>
</head>

<body>
  <main>
    <!-- Login -->
    <section class="g-min-height-100vh d-flex align-items-center g-bg-size-cover g-bg-pos-top-center" style="background-image: url(/signin.jpg);">
      <div class="container g-py-40 g-pos-rel g-z-index-1">
        <div class="row justify-content-center">
          <div class="col-sm-8 col-lg-5">
            <div class="g-bg-white rounded g-py-40 g-px-30">
                <header class="text-center mb-4">
                    <h2 class="h2 g-color-black g-font-weight-600">Enter Your Email</h2>
                </header>
    
                <!-- Form -->
                <form class="g-py-15" method="POST" action="{{ route('set_email') }}">
                    {{ csrf_field() }}

                    <div class="photo-container" style="margin-bottom: 10px;    text-align: center;">
                        <img src="{{ $user['photo'] }}" style="height:  123px;width:  123px;"/>
                    </div>

                    <div class="mb-4">
                        <label class="g-color-gray-dark-v2 g-font-weight-600 g-font-size-13">Email:</label>
                        <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15 {{ $errors->has('email') ? 'has-error' : '' }}" type="email" placeholder="johndoe@gmail.com" name="email" value="{{ old('email') }}" required autofocus>
                        
                        @if ($errors->has('email'))
                            <div class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </div>
                        @endif
                    </div>

                    <div class="mb-2">
                        <button class="btn btn-md btn-block u-btn-primary rounded g-pb-10" type="submit">Register</button>
                    </div>
                </form>
                <!-- End Form -->
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Login -->
  </main>

  <div class="u-outer-spaces-helper"></div>


  <!-- JS Global Compulsory -->
  <script src="/frontend-assets/main-assets/assets/vendor/jquery/jquery.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/jquery-migrate/jquery-migrate.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/popper.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/bootstrap/bootstrap.min.js"></script>


  <!-- JS Unify -->
  <script src="/frontend-assets/main-assets/assets/js/hs.core.js"></script>

  <!-- JS Customization -->
  <script src="/frontend-assets/main-assets/assets/js/custom.js"></script>







</body>

</html>
