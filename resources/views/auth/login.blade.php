<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Title -->
  <title>Login</title>

  <!-- Required Meta Tags Always Come First -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <!-- Favicon -->
  <link rel="shortcut icon" href="/frontend-assets/main-assets/favicon.ico">
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- CSS Global Compulsory -->
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/icon-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/animate.css">

  <!-- CSS Unify -->
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/css/unify-core.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/css/unify-components.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/css/unify-globals.css">

  <!-- CSS Customization -->
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/css/custom.css">
  <style>
      .help-block{
          color: red;
      }
    </style>
</head>

<body>
  <main>
    <!-- Login -->
    <section class="g-min-height-100vh d-flex align-items-center g-bg-size-cover g-bg-pos-top-center" style="background-image: url(/signin.jpg);">
      <div class="container g-py-40 g-pos-rel g-z-index-1">
        <div class="row justify-content-center">
          <div class="col-sm-8 col-lg-5">
            <div class="g-bg-white rounded g-py-40 g-px-30">
              <header class="text-center mb-4">
                <h2 class="h2 g-color-black g-font-weight-600">Login</h2>
              </header>

              <!-- Form -->
              <form class="g-py-10" method="POST" action="{{ route('login') }}">
                @csrf
                <div class="mb-4">
                  <input id="email" type="email" class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="Email Address">
                  
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="g-mb-35">
                  <input id="password" class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15 mb-3{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required type="password" placeholder="Password">
                  @if ($errors->has('password'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('password') }}</strong>
                      </span>
                  @endif
                  
                  <div class="row justify-content-between">
                    <div class="col align-self-center">
                      <label class="form-check-inline u-check g-color-gray-dark-v5 g-font-size-12 g-pl-25 mb-0">
                        <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} />
                        <div class="u-check-icon-checkbox-v6 g-absolute-centered--y g-left-0">
                          <i class="fa" data-check-icon="&#xf00c"></i>
                        </div>
                        Keep signed in
                      </label>
                    </div>
                    <div class="col align-self-center text-right">
                      <a class="g-font-size-12" href="{{ route('password.request') }}">Forgot password?</a>
                    </div>
                  </div>
                </div>

                <div class="mb-2">
                  <button class="btn btn-md btn-block u-btn-primary rounded g-pb-10" type="submit">Login</button>
                </div>
              </form>
              <!-- End Form -->
              <div class="text-center mb-3">
                <h3 class="h5">Or Join with</h3>
              </div>
  
                
              <div class="row no-gutters">
                <div class="col">
                  <a class="btn btn-block u-btn-facebook g-font-weight-500 g-font-size-12 text-uppercase g-rounded-left-3 g-rounded-right-0 g-px-25 g-py-13" href="/signin/facebook">
                    <i class="mr-2 fa fa-facebook"></i>
                    Facebook
                  </a>
                </div>
                <div class="col">
                  <a class="btn btn-block u-btn-twitter g-font-weight-500 g-font-size-12 text-uppercase g-rounded-left-0 g-rounded-right-3 g-px-25 g-py-13" style="background:#dd5144" href="/signin/google">
                    <i class="mr-2 fa fa-google"></i>
                    Google
                  </a>
                </div>
              </div>
              
              <footer class="text-center mt-4">
                <p class="g-color-gray-dark-v5 g-font-size-13 mb-0">Don't have an account? <a class="g-font-weight-600" href="/register">Signup</a>
                </p>
              </footer>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Login -->
  </main>

  <div class="u-outer-spaces-helper"></div>


  <!-- JS Global Compulsory -->
  <script src="/frontend-assets/main-assets/assets/vendor/jquery/jquery.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/jquery-migrate/jquery-migrate.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/popper.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/bootstrap/bootstrap.min.js"></script>


  <!-- JS Unify -->
  <script src="/frontend-assets/main-assets/assets/js/hs.core.js"></script>

  <!-- JS Customization -->
  <script src="/frontend-assets/main-assets/assets/js/custom.js"></script>







</body>

</html>
