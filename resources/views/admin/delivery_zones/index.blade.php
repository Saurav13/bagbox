@extends('layouts.admin')

@section('body')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            {{-- <h2 class="content-header-title">Manage DeliveryZones</h2> --}}
            <button class="btn btn-md btn-primary" data-toggle="modal" id="addDeliveryZoneButton" data-target="#addDeliveryZone"><i class="icon-plus-circle"></i> Add DeliveryZone</button>
        </div>
        <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
            <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{URL::to('admin/dashboard')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">Manage Delivery Zones
                    </li>
                </ol>
            </div>
        </div>
    </div>
    <section id="delivery_zones">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Manage Delivery Zones</h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block card-dashboard">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th>Delivery City</th>
                                    <th>Delivery Charge</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($delivery_zones as $c)
                                    <input type="hidden" id="DeliveryZoneName{{$c->id}}" value="{{$c->name}}"/>
                                    <input type="hidden" id="DeliveryZoneCharge{{$c->id}}" value="{{$c->delivery_charge}}"/>

                                    <tr>
                                        <th scope="row">{{ $c->id }}</th>
                                        <td>{{$c->name}}</td>
                                        <td>AED {{ $c->delivery_charge }}</td>
                                        
                                        <td>
                                            <button class="btn btn-outline-warning btn-sm" title="Edit" data-toggle="modal" data-target="#editDeliveryZone" id="DeliveryZoneEdit{{$c->id}}" c_id="{{$c->id}}"><i class="icon-edit2"></i> Edit</button>
                                            <button class="btn btn-sm btn-outline-red" id="deletecat{{$c->id}}"><i class="icon-remove"></i> Delete</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        
                        <div class="text-xs-center mb-3">
                            <nav aria-label="Page navigation">
                                {{ $delivery_zones->links() }}
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
                    

    {{-- Add DeliveryZone Modal --}}
    <div  class="modal fade" id="addDeliveryZone" tabindex="-1" role="dialog" aria-labelledby="myModalLabel19" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form id="savechanges" method="POST" class="modal-content" enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="cattitle">Add Delivery Zone</h4>
                </div>
                {{csrf_field()}}
                <div class="modal-body">
                    <label>Name</label>
                    <input type="text" id="addDeliveryZonesName"  required name="name" class="form-control" placeholder="Name of Delivery Zone"/>
                    <br>
                   
                    <label>Delivery Charge</label>
                    
                    <input type="number" step="0.01" id="addDeliveryZonesCharge" required name="delivery_charge" min="0" placeholder="Delivery Charge" class="form-control" >

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" >Add DeliveryZone</button>
                </div>
            </form>
        </div>
    </div>
    {{-- End Add DeliveryZone Modal --}}
    {{-- Edit DeliveryZone Modal --}}

    <div  class="modal fade" id="editDeliveryZone" tabindex="-1" role="dialog" aria-labelledby="myModalLabel20" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <form id="editchanges" method="POST" enctype="multipart/form-data"  action="{{route('delivery_zone.update')}}" class="modal-content">
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="cattitle">Edit DeliveryZone</h4>
                    </div>
                    {{csrf_field()}}
                    <input type="text"  hidden id="editDeliveryZonesId"  required name="id" class="form-control" placeholder="Name of Delivery Zone"/>

                    <div class="modal-body">
                        <label>Name</label>
                        <input type="text" id="editDeliveryZonesName"  required name="name" class="form-control" placeholder="Name of Delivery Zone"/>
                        <br>
                        <label>Delivery Charge</label>
                        
                        <input type="number" step="0.01" id="editDeliveryZonesCharge" name="delivery_charge" min="0" placeholder="Delivery Charge"  required class="form-control" >
    
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" >Save </button>
                    </div>
        </form>
        </div>
    </div>    
    {{-- EndEdit DeliveryZone Modal --}}

    {{-- Add Sub-delivery_zone Modal --}}

 
@endsection

@section('js')
    <script>

        $(document).ready(function(){
            $("[id*='DeliveryZoneEdit']").click(function(){
                var id = $(this).attr("c_id");
                var name=$('#DeliveryZoneName'+id).val();
                var charge=$('#DeliveryZoneCharge'+id).val();
                
                $('#editDeliveryZonesId').val(id);
                $('#editDeliveryZonesName').val(name);
                $('#editDeliveryZonesCharge').val(charge);

            });

        

        });

        

        $(document).on('click',"[id*='deletecat']",function(event){
            var id = $(this).attr("id").slice(9);
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this file!",
                type: "warning",
        
                showCancelButton: true,
        
            }).then(function(){
                $.post("{{route('delivery_zone.delete')}}",{id:id,_token:"{{csrf_token()}}"},function(data){
                    swal({
                        title:"Deleted Successfully",
                        type:"success"
            
                    }).then(function(){
                        location.reload();
                    })
                })
            });
        });



    </script>
@endsection