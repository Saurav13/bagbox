@extends('layouts.admin')
@section('body')

<style>
    .switch input { 
        display:none;
    }
    .switch {
        display:inline-block;
        width:30px;
        height:15px;
        margin:8px;
        transform:translateY(50%);
        position:relative;
    }
    
    .slider {
        position:absolute;
        top:0;
        bottom:0;
        left:0;
        right:0;
        border-radius:15px;
        box-shadow:0 0 0 2px #777, 0 0 4px #777;
        cursor:pointer;
        border:4px solid transparent;
        overflow:hidden;
         transition:.4s;
    }
    .slider:before {
        position:absolute;
        content:"";
        width:100%;
        height:100%;
        background:#777;
        border-radius:15px;
        transform:translateX(-15px);
        transition:.4s;
    }
    
    input:checked + .slider:before {
        transform:translateX(15px);
        background:limeGreen;
    }
    input:checked + .slider {
        box-shadow:0 0 0 2px limeGreen,0 0 2px limeGreen;
    }
    
</style>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

<div class="content-header row">
    <div class="content-header-left col-md-6 col-xs-12 mb-1">
          <h2 class="content-header-title">Add Product</h2>
    </div>
    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
        <div class="breadcrumb-wrapper col-xs-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ URL::to('admin/dashboard') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item"><a href="{{ URL::to('admin/products') }}">Manage Product</a>
                </li>
                <li class="breadcrumb-item active">Add Products
                </li>
            </ol>
        </div>
    </div>
</div>

<section id="basic-form-layouts">
	<div ng-app="Product" ng-controller="AttributesController" class="row match-height">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title" id="basic-layout-form"><i class="icon-android-cart"></i> Product Information</h4>
					<a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
				</div>
				<div class="card-body collapse in">
					<div class="card-block">
                    <form class="form" id="product-form" action="{{route('products.store')}}" enctype="multipart/form-data" method="POST">
                            {{csrf_field()}}
							<div class="form-body">
								<h4 class="form-section"><i class="icon-information"></i> Product Info</h4>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label for="product_name">Product Name</label>
                                            <input type="text" id="product_name" class="form-control" placeholder="Product Name" value="{{old('product_name')}}" name="product_name" required>
                                            @if ($errors->has('product_name'))
                                                <div class="alert alert-danger no-border mb-2">
                                                    <strong>{{ $errors->first('product_name') }}</strong>
                                                </div>
                                            @endif
                                        </div>
									</div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="product_name">Product Sub Title</label>
                                            <input type="text" id="subtitle" class="form-control" placeholder="Product Sub Title" value="{{old('subtitle')}}" name="subtitle" required>
                                            @if ($errors->has('subtitle'))
                                                <div class="alert alert-danger no-border mb-2">
                                                    <strong>{{ $errors->first('subtitle') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
        
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Regular Price</label>
                                            <input type="number" class="form-control square" name="regular_price" placeholder="Enter the price of the product" value="{{old('regular_price')}}" min="0" required />
                                            @if ($errors->has('regular_price'))
                                                <div class="alert alert-danger no-border mb-2">
                                                    <strong>{{ $errors->first('regular_price') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Sale/Discount Price (Optional)</label>
                                            <input type="number" class="form-control square" name="sale_price" placeholder="Enter the price of the product" value="{{old('sale_price')}}" min="1" />
                                            @if ($errors->has('sale_price'))
                                                <div class="alert alert-danger no-border mb-2">
                                                    <strong>{{ $errors->first('sale_price') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Quantity (Leave blank if unlimited)</label>
                                            <input type="number" class="form-control square" name="quantity" placeholder="Enter the quantity of the product" value="{{ old('quantity') }}" min="1" />
                                            @if ($errors->has('quantity'))
                                                <div class="alert alert-danger no-border mb-2">
                                                    <strong>{{ $errors->first('quantity') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Type</label>
                                            <select class="form-control" name="type" required>
                                                <option {{ !old('type') ? 'selected' : '' }} disabled>Choose a type</option>
                                                <option {{ old('type') == 'Retail' ? 'selected' : '' }}>Retail</option>
                                                <option {{ old('type') == 'Wholesale' ? 'selected' : '' }}>Wholesale</option>
                                                <option {{ old('type') == 'Both' ? 'selected' : '' }}>Both</option>
                                            </select>
                                            @if ($errors->has('type'))
                                                <div class="alert alert-danger no-border mb-2">
                                                    <strong>{{ $errors->first('type') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="product_description">Product Description</label>
                                    <textarea id="product_description" rows="5" class="form-control square" name="product_description" placeholder="Enter the description of the product">{{old('product_description')}}</textarea>
                                    @if ($errors->has('product_description'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('product_description') }}</strong>
                                        </div>
                                    @endif
                                </div>
								

								<h4 class="form-section"><i class="icon-cog"></i> Product Category</h4>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label for="product_category">Product Category</label>
											<select id="product_category" name="product_category" class="form-control" required>
                                                <option value="" selected="" disabled="">Choose Category</option>
                                                @foreach($categories as $c)
                                                    <option value="{{$c->id}}">{{$c->cat_name}}</option>
												@endforeach
                                            </select>
                                            @if ($errors->has('product_category'))
                                                <div class="alert alert-danger no-border mb-2">
                                                    <strong>{{ $errors->first('product_category') }}</strong>
                                                </div>
                                            @endif
										</div>
									</div>

                                    
                                    
                                </div>
                                
                                <h4 class="form-section"><i class="icon-image4"></i> Product Images</h4>
								<div class="form-group">
                                    <label for="product_images">Select Images</label>
                                    <input type="file" class="form-control" id="product_images" name="product_images[]" multiple required/>
                                    @if ($errors->has('product_images'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('product_images') }}</strong>
                                        </div>
                                    @endif
                                </div>
                                    
                                <h4 class="form-section"><i class="icon-clipboard4"></i> Product Details</h4>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="product_images">Upload File (Optional)</label>
                                            <input type="file" class="form-control" id="pdf" name="pdf"  />
                                            @if ($errors->has('pdf'))
                                                <div class="alert alert-danger no-border mb-2">
                                                    <strong>{{ $errors->first('pdf') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Video Embed Link  (Optional)</label>
                                            <input type="text" class="form-control square" name="video_link" placeholder="Enter the embeded link of youtube" value="{{old('video_link')}}"  />
                                            @if ($errors->has('video_link'))
                                                <div class="alert alert-danger no-border mb-2">
                                                    <strong>{{ $errors->first('vide_link') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                
                                <h4 class="form-section"><i class="icon-clipboard4"></i> Product Links (if any)</h4>

                                <div class="form-group" >
                                    <button type="button" class="btn btn-md btn-primary" ng-click="addAttribute()"> <i class="icon-plus-circle"></i> Add Link</button>
                                    <div class="row" ng-repeat="item in attributes" style="padding:5px;">
                                        <div class="col-md-5 col-sm-5 col-xs-5">
                                            <input ng-required="required" ng-model="item.name" type="text" class="form-control" placeholder="Name">
                                        </div>
                                        <div class="col-md-5 col-sm-5 col-xs-5">
                                            <input ng-required="required" ng-model="item.link" type="text" class="form-control" placeholder="Link">
                                        </div>
                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                            <button type="button" ng-click="delAttribute($index)" class="btn btn-md btn-danger"><i class="icon-cross2"></i></button>
                                        </div>  
                                        
                                    </div>
                                    <input type="hidden" name="links" value="@{{attributes}}">
                                </div>

                                <h4 class="form-section"><i class="icon-ios-checkmark-outline"></i> Feature and Activate Product</h4>
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <label>Feature this product</label>
                                        <div class="input-group">
                                            <label class="display-inline-block custom-control custom-radio ml-1">
                                                <input type="radio" value="1" name="feature" class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description ml-0">Yes</span>
                                            </label>
                                            <label class="display-inline-block custom-control custom-radio">
                                                <input type="radio" value="0" name="feature" checked class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description ml-0">No</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Show this product in website</label>
                                            <div class="input-group">
                                                <label class="display-inline-block custom-control custom-radio ml-1">
                                                    <input type="radio" value="1" name="active" class="custom-control-input" checked>
                                                    <span class="custom-control-indicator"></span>
                                                    <span class="custom-control-description ml-0">Yes</span>
                                                </label>
                                                <label class="display-inline-block custom-control custom-radio">
                                                    <input type="radio" value="0" name="active"  class="custom-control-input">
                                                    <span class="custom-control-indicator"></span>
                                                    <span class="custom-control-description ml-0">No</span>
                                                </label>
                                            </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Show this product in landing slider</label>
                                            <div class="input-group">
                                                <label class="display-inline-block custom-control custom-radio ml-1">
                                                    <input type="radio" value="1" name="in_landing_slider" class="custom-control-input">
                                                    <span class="custom-control-indicator"></span>
                                                    <span class="custom-control-description ml-0">Yes</span>
                                                </label>
                                                <label class="display-inline-block custom-control custom-radio">
                                                    <input type="radio" value="0" name="in_landing_slider"  class="custom-control-input">
                                                    <span class="custom-control-indicator"></span>
                                                    <span class="custom-control-description ml-0">No</span>
                                                </label>
                                            </div>
                                    </div>
                                </div>

                                <h4 class="form-section"><i class="icon-tags"></i> Product Tags</h4>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label>Tags</label>
                                        <select class="form-control" name="tags[]" id="selectTags" multiple="multiple">
                                            @foreach ($tags as $tag)
                                                <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('tags'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('tags') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                
                                <h4 class="form-section" style="margin-top: 7rem"><i class="icon-cog"></i> SEO Meta Tags</h4>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="meta_title">Meta Title</label>
                                            <input type="text" id="meta_title" class="form-control" placeholder="Meta Title" value="{{old('meta_title')}}" name="meta_title">
                                            @if ($errors->has('meta_title'))
                                                <div class="alert alert-danger no-border mb-2">
                                                    <strong>{{ $errors->first('meta_title') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="meta_description">Meta Description</label>
                                            <textarea rows="3" id="meta_description" class="form-control" placeholder="Meta Description" name="meta_description">{{old('meta_description')}}</textarea>
                                            @if ($errors->has('meta_description'))
                                                <div class="alert alert-danger no-border mb-2">
                                                    <strong>{{ $errors->first('meta_description') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="meta_keywords">Meta Keywords</label>
                                            <input type="text" id="meta_keywords" class="form-control" placeholder="Meta Keywords" value="{{old('meta_keywords')}}" name="meta_keywords">
                                            @if ($errors->has('meta_keywords'))
                                                <div class="alert alert-danger no-border mb-2">
                                                    <strong>{{ $errors->first('meta_keywords') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
							</div>

							<div class="form-actions pull-right">
                                
                                <button type="submit" id="submit-product-form" class="btn btn-primary">
                                    <i class="icon-check2"></i> Save
								</button>
                                <a class="btn btn-warning mr-1" href="{{url()->previous()}}">
                                        <i class="icon-cross2"></i>
                                    Cancel
                                </a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection

@section('js')
    <script src="//cdn.tiny.cloud/1/i54lxo7292e00pntywltqhso8u1q9mrye4m6pz78losx9jjr/tinymce/5/tinymce.min.js"></script>

    <script>
        var editor_config = {
            path_absolute : "/",
            selector: "textarea#product_description",
            height : 200,
            content_style: "body {font-family: Verdana,Arial,Helvetica,sans-serif;font-size: 20px;}",
            branding: false,
            menubar:true,
            toolbar: false,
            plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
            height: "300",
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
            var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

            var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
            if (type == 'image') {
                cmsURL = cmsURL + "&type=Images";
            } else {
                cmsURL = cmsURL + "&type=Files";
            }

            tinyMCE.activeEditor.windowManager.open({
                file : cmsURL,
                title : 'Filemanager',
                width : x * 0.8,
                height : y * 0.8,
                resizable : "yes",
                close_previous : "no",
                forced_root_block : "", 
                force_br_newlines : true,
                force_p_newlines : false,
                image_class_list: [
                    {title: 'Responsive', value: 'img-responsive'}
                ],
            });
            }
        };

        tinymce.init(editor_config);
    </script>

    <script>
    </script>
    <script src="{{asset('admin-assets/js/addAttributes.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

    <script>
        $("#selectTags").select2({
            tags: true,
            placeholder: "Select tags",
        });
    </script>
@endsection