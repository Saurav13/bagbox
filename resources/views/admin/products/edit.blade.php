@extends('layouts.admin')
@section('body')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
              <h2 class="content-header-title"><i class="icon-edit2"></i> Edit Product and Product Images</h2>
        </div>
        <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
            <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{URL::to('admin/dashboard')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{URL::to('admin/products')}}">Manage Product</a>
                    </li>
                    <li class="breadcrumb-item active">Edit Product Info
                    </li>
                </ol>
            </div>
        </div>
    </div>
    <!-- Justified With Top Border start -->
    <section id="justified-top-border">
        <div class="row match-height">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <ul class="nav nav-tabs nav-justified">
                                <li class="nav-item">
                                    <a class="nav-link active" id="active-tab" data-toggle="tab" href="#active" aria-controls="active" aria-expanded="true"><h4 class="card-title" id="basic-layout-form"><i class="icon-android-cart"></i>Edit Product Information</h4>
                                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="link-tab" data-toggle="tab" href="#link" aria-controls="link" aria-expanded="false"><h4 class="card-title" id="basic-layout-form"><i class="icon-image4"></i> Product Images</h4>
                                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a></a>
                                </li>
                            </ul>
                            <div ng-app="Product" ng-controller="AttributesController" ng-init="Retriever({{$product->links}})" class="tab-content pt-1">
                                <div role="tabpanel" class="tab-pane fade active in" id="active" aria-labelledby="active-tab" aria-expanded="true">
                                    <section id="basic-form-layouts">
                                        <div class="row match-height">
                                            <div class="col-md-12">
                                                <div class="">
                                                    <div class=" collapse in">
                                                        <div class="" style="padding-top: 0px;padding-left: 0px;padding-right: 0px;padding-bottom: 0px;">
                                                            <form class="form" action="{{route('products.update',$product->id)}}" enctype="multipart/form-data" method="POST">
                                                                <input type="hidden" name="_method" value="PATCH" />
                                                                {{csrf_field()}}
                                                                <div class="form-body" style="padding:20px;">
                                                                    <h4 class="form-section"><i class="icon-information"></i> Product Info</h4>
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="form-group">
                                                                                <label for="product_name">Product Name</label>
                                                                                <input type="text" id="product_name" value="{{$product->product_name}}" class="form-control" placeholder="Product Name" name="product_name" required>
                                                                                @if ($errors->has('product_name'))
                                                                                    <div class="alert alert-danger no-border mb-2">
                                                                                        <strong>{{ $errors->first('product_name') }}</strong>
                                                                                    </div>
                                                                                @endif
                                                                            </div>
                                                                        </div> 
                                                                        <div class="col-md-12">
                                                                            <div class="form-group">
                                                                                <label for="product_name">Product Sub Title</label>
                                                                                <input type="text" id="subtitle" class="form-control" placeholder="Product Sub Title" value="{{$product->subtitle}}" name="subtitle" required>
                                                                                @if ($errors->has('subtitle'))
                                                                                    <div class="alert alert-danger no-border mb-2">
                                                                                        <strong>{{ $errors->first('subtitle') }}</strong>
                                                                                    </div>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                        
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label>Regular Price</label>
                                                                                <input type="number" class="form-control square" name="regular_price" placeholder="Enter the price of the product" value="{{ $product->regular_price }}" min="0" required />
                                                                                @if ($errors->has('regular_price'))
                                                                                    <div class="alert alert-danger no-border mb-2">
                                                                                        <strong>{{ $errors->first('regular_price') }}</strong>
                                                                                    </div>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label>Sale/Discount Price (Optional)</label>
                                                                                <input type="number" class="form-control square" name="sale_price" placeholder="Enter the price of the product" value="{{ $product->sale_price }}" min="1" />
                                                                                @if ($errors->has('sale_price'))
                                                                                    <div class="alert alert-danger no-border mb-2">
                                                                                        <strong>{{ $errors->first('sale_price') }}</strong>
                                                                                    </div>
                                                                                @endif
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label>Quantity (Leave blank if unlimited)</label>
                                                                                <input type="number" class="form-control square" name="quantity" placeholder="Enter the quantity of the product" value="{{$product->quantity}}" min="1" />
                                                                                @if ($errors->has('quantity'))
                                                                                    <div class="alert alert-danger no-border mb-2">
                                                                                        <strong>{{ $errors->first('quantity') }}</strong>
                                                                                    </div>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label>Type</label>
                                                                                <select class="form-control" name="type" required>
                                                                                    <option {{ !$product->type ? 'selected' : '' }} disabled>Choose a type</option>
                                                                                    <option {{ $product->type == 'Retail' ? 'selected' : '' }}>Retail</option>
                                                                                    <option {{ $product->type == 'Wholesale' ? 'selected' : '' }}>Wholesale</option>
                                                                                    <option {{ $product->type == 'Both' ? 'selected' : '' }}>Both</option>
                                                                                </select>
                                                                                @if ($errors->has('type'))
                                                                                    <div class="alert alert-danger no-border mb-2">
                                                                                        <strong>{{ $errors->first('type') }}</strong>
                                                                                    </div>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                        

                                                                        <input type="hidden" value="{{$product->id}}" name="product_id"/>
                                                                        <div class="form-group">
                                                                            <label for="product_description">Product Description</label>
                                                                            <textarea id="product_description" rows="6" class="form-control" name="product_description" placeholder="Product Description">{{$product->product_description}}</textarea>
                                                                        </div>
                                                                    
                                    
                                                                    <h4 class="form-section"><i class="icon-cog"></i> Product Category</h4>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label for="product_category">Product Category</label>
                                                                                <select name="product_category" id="product_category" class="form-control" required>
                                                                                    
                                                                                    @foreach($categories as $c)
                                                                                        <option @if($product->cat_id == $c->id) selected @endif value="{{$c->id}}">{{$c->cat_name}}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                                @if ($errors->has('product_category'))
                                                                                    <div class="alert alert-danger no-border mb-2">
                                                                                        <strong>{{ $errors->first('product_category') }}</strong>
                                                                                    </div>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                    

                                                                    </div>
                                                                    
                                                                    <h4 class="form-section"><i class="icon-image4"></i> Product Images</h4>
                                                                    <div class="form-group">
                                                                        <label for="product_images">Add more Images</label>
                                                                        <input type="file" class="form-control" id="product_images" name="product_images[]" multiple/>
                                                                        @if ($errors->has('product_images'))
                                                                            <div class="alert alert-danger no-border mb-2">
                                                                                <strong>{{ $errors->first('product_images') }}</strong>
                                                                            </div>
                                                                        @endif
                                                                    </div>

                                                                    <h4 class="form-section"><i class="icon-clipboard4"></i> Product Details</h4>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label for="product_images">Upload File (Optional)</label>
                                                                                <input type="file" class="form-control" id="pdf" name="pdf"  />
                                                                                @if ($errors->has('pdf'))
                                                                                    <div class="alert alert-danger no-border mb-2">
                                                                                        <strong>{{ $errors->first('pdf') }}</strong>
                                                                                    </div>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label>Video Embed Link  (Optional)</label>
                                                                                <input type="text" class="form-control square" name="video_link" placeholder="Enter the embeded link of youtube" value="{{$product->video_link}}"  />
                                                                                @if ($errors->has('video_link'))
                                                                                    <div class="alert alert-danger no-border mb-2">
                                                                                        <strong>{{ $errors->first('vide_link') }}</strong>
                                                                                    </div>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                        
                                                                    <h4 class="form-section"><i class="icon-clipboard4"></i> Product Links (if any)</h4>

                                                                    <div class="form-group">
                                                                        <button type="button" class="btn btn-md btn-primary" ng-click="addAttribute()"> <i class="icon-plus-circle"></i> Add Link</button>
                                                                        <div class="row" ng-repeat="item in attributes" style="padding:5px;">
                                                                            <div class="col-md-5 col-sm-5 col-xs-5">
                                                                                <input ng-required="required" ng-model="item.name" type="text" class="form-control" placeholder="Name">
                                                                            </div>
                                                                            <div class="col-md-5 col-sm-5 col-xs-5">
                                                                                <input ng-required="required" ng-model="item.link" type="text" class="form-control" placeholder="Link">
                                                                            </div>
                                                                            <div class="col-md-2 col-sm-2 col-xs-2">
                                                                                <button type="button" ng-click="delAttribute($index)" class="btn btn-md btn-danger"><i class="icon-cross2"></i></button>
                                                                            </div>  
                                                                            
                                                                        </div>
                                                                        <input type="hidden" name="links" value="@{{attributes}}">
                                                                    </div>
                                                                    
                                                                    <h4 class="form-section"><i class="icon-ios-checkmark-outline"></i> Feature and Activate Product</h4>
                                                                    <div class="form-group">
                                                                        <div class="col-md-4">
                                                                            <label>Feature this product</label>
                                                                            <div class="input-group">
                                                                                <label class="display-inline-block custom-control custom-radio ml-1">
                                                                                    <input type="radio" value="1" name="feature"
                                                                                    @if($product->featured == true)
                                                                                    checked
                                                                                    @endif
                                                                                    class="custom-control-input">
                                                                                    <span class="custom-control-indicator"></span>
                                                                                    <span class="custom-control-description ml-0">Yes</span>
                                                                                </label>
                                                                                <label class="display-inline-block custom-control custom-radio">
                                                                                    <input type="radio" value="0" name="feature" 
                                                                                    @if($product->featured == false) checked @endif 
                                                                                    class="custom-control-input">
                                                                                    <span class="custom-control-indicator"></span>
                                                                                    <span class="custom-control-description ml-0">No</span>
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <label>Show this product in website</label>
                                                                                <div class="input-group">
                                                                                    <label class="display-inline-block custom-control custom-radio ml-1">
                                                                                        <input type="radio" value="1" name="active" class="custom-control-input" @if($product->active == true) checked @endif>
                                                                                        <span class="custom-control-indicator"></span>
                                                                                        <span class="custom-control-description ml-0">Yes</span>
                                                                                    </label>
                                                                                    <label class="display-inline-block custom-control custom-radio">
                                                                                        <input type="radio" value="0" name="active" @if($product->active == false) checked @endif class="custom-control-input">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                        <span class="custom-control-description ml-0">No</span>
                                                                                    </label>
                                                                                </div>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <label>Show this product in landing slider</label>
                                                                            <div class="input-group">
                                                                                <label class="display-inline-block custom-control custom-radio ml-1">
                                                                                    <input type="radio" value="1" name="in_landing_slider" class="custom-control-input" {{ $product->in_landing_slider ? 'checked' : '' }}>
                                                                                    <span class="custom-control-indicator"></span>
                                                                                    <span class="custom-control-description ml-0">Yes</span>
                                                                                </label>
                                                                                <label class="display-inline-block custom-control custom-radio">
                                                                                    <input type="radio" value="0" name="in_landing_slider"  class="custom-control-input" {{ !$product->in_landing_slider ? 'checked' : '' }}>
                                                                                    <span class="custom-control-indicator"></span>
                                                                                    <span class="custom-control-description ml-0">No</span>
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <h4 class="form-section"><i class="icon-tags"></i> Product Tags</h4>
                                                                    <div class="form-group">
                                                                        <div class="col-md-12">
                                                                            <label>Tags</label>
                                                                            <select class="form-control" name="tags[]" id="selectTags" multiple="multiple">
                                                                                @foreach ($tags as $tag)
                                                                                    <option value="{{ $tag->id }}" {{ in_array($tag->id,$product->tags()->pluck('id')->toArray()) ? 'selected' : '' }}>{{ $tag->name }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                            @if ($errors->has('tags'))
                                                                                <div class="alert alert-danger no-border mb-2">
                                                                                    <strong>{{ $errors->first('tags') }}</strong>
                                                                                </div>
                                                                            @endif
                                                                        </div>
                                                                    </div>

                                                                    <h4 class="form-section" style="margin-top: 7rem"><i class="icon-cog"></i> SEO Meta Tags</h4>
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="form-group">
                                                                                <label for="meta_title">Meta Title</label>
                                                                                <input type="text" id="meta_title" class="form-control" placeholder="Meta Title" value="{{ $product->meta_title }}" name="meta_title">
                                                                                @if ($errors->has('meta_title'))
                                                                                    <div class="alert alert-danger no-border mb-2">
                                                                                        <strong>{{ $errors->first('meta_title') }}</strong>
                                                                                    </div>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <div class="form-group">
                                                                                <label for="meta_description">Meta Description</label>
                                                                                <textarea rows="3" id="meta_description" class="form-control" placeholder="Meta Description" name="meta_description">{{ $product->meta_description }}</textarea>
                                                                                @if ($errors->has('meta_description'))
                                                                                    <div class="alert alert-danger no-border mb-2">
                                                                                        <strong>{{ $errors->first('meta_description') }}</strong>
                                                                                    </div>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <div class="form-group">
                                                                                <label for="meta_keywords">Meta Keywords</label>
                                                                                <input type="text" id="meta_keywords" class="form-control" placeholder="Meta Keywords" value="{{ $product->meta_keywords }}" name="meta_keywords">
                                                                                @if ($errors->has('meta_keywords'))
                                                                                    <div class="alert alert-danger no-border mb-2">
                                                                                        <strong>{{ $errors->first('meta_keywords') }}</strong>
                                                                                    </div>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                    
                                                                <div class="form-actions pull-right" style="padding:20px;">
                                                                    <a href="{{ route('products.index') }}" class="btn btn-warning mr-1">
                                                                        <i class="icon-cross2"></i> Cancel
                                                                    </a>
                                                                    <button type="submit" class="btn btn-primary">
                                                                        <i class="icon-check2"></i> Save
                                                                    </button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                            
                                </div>
                                <div class="tab-pane fade" id="link" role="tabpanel" aria-labelledby="link-tab" aria-expanded="false">
                                    <section id="header-footer">
                                        <div class="row match-height">
                                            @foreach($images as $img)
                                            <div class="col-xl-4 col-md-6">
                                                <div class="card">
                                                    <div class="card-body">
                                                    <img class="img-fluid" src="{{asset('product-images'.'/'.$img->image)}}" alt="{{$product->product_name}}">
                                                    @if(count($images) > 1)
                                                        <div class="card-block">
                                                        <a href="#" id="delete_image_product{{$img->id}}" class="btn btn-md btn-danger"><i class="icon-cross2"></i> Remove Image</a>
                                                        </div>
                                                        @endif   
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('js')
    <script src="{{asset('admin-assets/js/addAttributes.js')}}"></script>

    <script src="//cdn.tiny.cloud/1/i54lxo7292e00pntywltqhso8u1q9mrye4m6pz78losx9jjr/tinymce/5/tinymce.min.js"></script>
    
    <script>
        var editor_config = {
            path_absolute : "/",
            selector: "textarea#product_description",
            height : 200,
            content_style: "body {font-family: Verdana,Arial,Helvetica,sans-serif;font-size: 20px;}",
            branding: false,
            menubar:true,
            toolbar: false,
            plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
            height: "300",
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
            var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

            var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
            if (type == 'image') {
                cmsURL = cmsURL + "&type=Images";
            } else {
                cmsURL = cmsURL + "&type=Files";
            }

            tinyMCE.activeEditor.windowManager.open({
                file : cmsURL,
                title : 'Filemanager',
                width : x * 0.8,
                height : y * 0.8,
                resizable : "yes",
                close_previous : "no",
                forced_root_block : "", 
                force_br_newlines : true,
                force_p_newlines : false,
                image_class_list: [
                    {title: 'Responsive', value: 'img-responsive'}
                ],
            });
            }
        };

        tinymce.init(editor_config);
    </script>


    <script>
        @if(count($images) > 1)
            $(document).on('click',"[id*='delete_image_product']",function(event){
                var id = $(this).attr("id").slice(20);
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this file!",
                    type: "warning",
            
                    showCancelButton: true,
            
                }).then(function(){
                    $.post("{{route('productimage.delete')}}",{id:id,_token:"{{csrf_token()}}"},function(data){
                        swal({
                        title:"Product Image Deleted Successfully",
                        type:"success"

                        }).then(function(){
                        location.reload();
                        })
                    })
                });
            });
        @endif
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

    <script>
        $("#selectTags").select2({
            tags: true,
            placeholder: "Select tags",
        });
    </script>
@endsection