@extends('layouts.admin')

@section('body')
<div class="content-header row">
    </div>
    <div class="content-body">
        {{-- <div class="text-xs-right">
            @if($s == 'all')
                <a class="btn btn-primary btn-min-width mr-1 mb-1 " href="#">See all unseen Quotations</a>
            @else
                <a class="btn btn-primary btn-min-width mr-1 mb-1 " href="#">See all quotations</a>
            @endif
        </div> --}}
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">New Orders</h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block card-dashboard">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th>No. of Product</th>
                                    <th>Amount</th>
                                    <th>Order Status</th>
                                    <th>Payment Status</th>
                                    <th>Ordered Date</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($orders as $order)
                                    
                                    <tr>
                                        <th scope="row">{{ $order->id }}</th>
                                        <td>{{ $order->items()->where('type','Product')->count() }}</td>
                                        <td>AED {{ number_format($order->amount) }}</td>
                                        <td><span class="tag {{ ($order->status == 'delivered') ? 'tag-success': ($order->order_status == 'cancelled' ? 'tag-danger' : 'tag-warning') }}">{{ $order->status }}</span></td>
                                        <td><span class="tag {{ ($order->paid  ? 'tag-success': 'tag-danger') }}">{{ $order->paid ? 'Paid' : 'Unpaid' }}</span></td>
                                        <td>{{ date('M j, Y',strtotime($order->ordered_at)) }}</td>

                                        <td>
                                            <a class="btn btn-outline-info btn-sm" title="View" href="{{route('show.order',$order->id)}}"><i class="icon-eye"></i></a>
                                            <form action="{{ route('orders.destroy',$order->id) }}" method="POST" style="display:inline">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="_method" value="DELETE" >
                                                <button id='deleteOrder{{ $order->id }}' type="button" title="Delete" class="btn btn-sm btn-outline-danger"><i class="icon-trash-o"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        
                        <div class="text-xs-center mb-3">
                            <nav aria-label="Page navigation">
                                {{ $orders->appends('quotations', \Request::get('quotations',1))->links() }}
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <h4 class="card-title">New Quotation Messages</h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block card-dashboard">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Quotationed Product</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($quotations as $quotation)
                                {{-- @if(!$contact->seen && $state == 'all')
                                    <tr style="background:#d6d3d3">
                                @else
                                    <tr>
                                @endif --}}
                                    <th scope="row">{{ $loop->iteration }}</th>
                                    <td>{{ $quotation->name }}</td>
                                    <td>{{ $quotation->product_name }}</td>
                                    <td>
                                    <a class="btn btn-outline-info btn-sm" title="View" href="{{route('show.quotation',$quotation->id)}}"><i class="icon-eye"></i></a>
                                        <a id='deleteQuotation{{$quotation->id}}' title="delete" type="button" class="btn btn-outline-danger btn-sm"><i class="icon-trash-o"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{-- @if(count($contacts)==0)
                            <p class="font-medium-3 text-muted text-xs-center" style="margin:100px">No {{ $state == 'all'?'':'New'}} Messages</p>
                        @endif --}}
                        <div class="text-xs-center mb-3">
                            <nav aria-label="Page navigation">
                                {{ $quotations->appends('orders', \Request::get('orders',1))->links() }}
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('js')
<script>
    $(document).on('click',"[id*='deleteQuotation']",function(event){
        var id = $(this).attr("id").slice(11);
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this file!",
            type: "warning",
    
            showCancelButton: true,
    
          }).then(function(){
            $.post("{{route('delete.quotation')}}",{id:id,_token:"{{csrf_token()}}"},function(data){
              swal({
                title:"Deleted Successfully",
                type:"success"
    
              }).then(function(){
               location.reload();
              })
            })
          });
    });
    </script>

@endsection