@extends('layouts.admin')

@section('body')
<div class="content-header row">
    </div>
    <div class="content-body">
        <div class="text-xs-right">

            <a class="btn btn-primary btn-min-width mr-1 mb-1 " href="{{URL::to('admin/orders')}}">See all Unseen Orders</a>
            {{-- <a class="btn btn-success btn-min-width mr-1 mb-1 " href="{{URL::to('admin/orders/export')}}">Export all Quotations</a> --}}

        </div>
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">All Orders</h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block card-dashboard">
                    <div class="table-responsive">
                        <table class="table">
                           

                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th>No. of Product</th>
                                    <th>Amount</th>
                                    <th>Order Status</th>
                                    <th>Payment Status</th>
                                    <th>Ordered Date</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($orders as $order)
                                    @if($order->seen == true)
                                        <tr>
                                    @else
                                        <tr style="background:#d6d3d3">
                                    @endif
                                        <th scope="row">{{ $order->id }}</th>
                                        <td>{{ $order->items()->where('type','Product')->count() }}</td>
                                        <td>AED {{ number_format($order->amount) }}</td>
                                        <td><span class="tag {{ ($order->status == 'delivered') ? 'tag-success': ($order->order_status == 'cancelled' ? 'tag-danger' : 'tag-warning') }}">{{ $order->status }}</span></td>
                                        <td><span class="tag {{ ($order->paid  ? 'tag-success': 'tag-danger') }}">{{ $order->paid ? 'Paid' : 'Unpaid' }}</span></td>
                                        <td>{{ date('M j, Y',strtotime($order->ordered_at)) }}</td>

                                        <td>
                                            <a class="btn btn-outline-info btn-sm" title="View" href="{{route('show.order',$order->id)}}"><i class="icon-eye"></i></a>
                                            <form action="{{ route('orders.destroy',$order->id) }}" method="POST" style="display:inline">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="_method" value="DELETE" >
                                                <button id='deleteOrder{{ $order->id }}' type="button" title="Delete" class="btn btn-sm btn-outline-danger"><i class="icon-trash-o"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        
                        <div class="text-xs-center mb-3">
                            <nav aria-label="Page navigation">
                                {{ $orders->links() }}
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('js')
<script>
</script>

@endsection