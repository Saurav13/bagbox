@extends('layouts.admin')

@section('body')

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Order #{{ $order->id }}</h2>
            <span class="tag {{ ($order->status == 'delivered') ? 'tag-success': ($order->order_status == 'cancelled' ? 'tag-danger' : 'tag-warning') }}">{{ $order->status }}</span>
            <span class="tag {{ ($order->paid  ? 'tag-success': 'tag-danger') }}">{{ $order->paid ? 'Paid' : 'Unpaid' }}</span>
                                        
        </div>
        <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
            <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                    <a class="btn btn-primary btn-min-width mr-1 mb-1 " href="{{ route('all.orders') }}">See all Orders</a>
                    <form action="{{ route('orders.update',$order->id) }}" method="POST" style="display:inline;">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PATCH" >
                        <select class="form-control" name="status" id="statusForm" style="margin-bottom: 5px; width: 200px">
                            <option selected disabled value="">Change Status</option>
                            <option value="shipped">Mark as Shipped</option>
                            <option value="delivered">Mark as Delivered</option>
                            <option value="cancelled">Mark as Cancelled</option>
                        </select>
                    </form>
                </ol>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section class="card">
            <div id="invoice-template" class="card-block">
                <!-- Invoice Company Details -->
                <div id="invoice-company-details" class="row">
                    <div class="col-md-6 col-sm-12 text-xs-center text-md-left">
                        <ul class="px-0 list-unstyled">
                            @php
                                $billing = json_decode($order->billing);
                            @endphp
                            <li><strong>Name      : </strong>{{ $billing->name }}</li>
                            <li><strong>Email     : </strong>{{ $billing->email }}</li>   
                            <li><strong>Phone No. : </strong>{{ $billing->phone }}</li>
                            <li><strong>Address   : </strong>{{ $billing->address }}</li>
                            <li><strong>City   : </strong>{{ $billing->city }}</li>
                            @if($billing->company)
                                <li><strong>Company   : </strong>{{ $billing->company }}</li>
                            @endif
                            <br>                             
                            <li><span class="text-muted">Ordered Date :</span> {{ date('M j, Y h:i A',strtotime($order->ordered_at)) }}</li>
                            
                        </ul>
                    </div>
                </div>
                <!--/ Invoice Company Details -->

                <!-- Invoice Items Details -->
                <div id="invoice-items-details" class="pt-2">
                       
                    <div class="row">
                        <div class="table-responsive col-sm-12">
                            <h4>Invoice</h4>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Item & Description</th>
                                        <th class="text-xs-right">Rate</th>
                                        <th class="text-xs-right">Quantity</th>
                                        <th class="text-xs-right">Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $i = 1;
                                    @endphp
                                    @foreach ($order->items()->where('type','Product')->get() as $item)
                                        
                                        <tr>
                                            <th scope="row">{{ $i++ }}</th>
                                            <td class="text-xs-left">
                                                @if($item->product_id)
                                                    <a href="{{ route('products.edit', $item->product_id) }}" target="_blank">{{ $item->product_name }}</a>
                                                @else
                                                    {{ $item->product_name }}
                                                @endif
                                            </td>
                                            <td class="text-xs-right">AED {{ $item->amount }}</td>
                                            <td class="text-xs-right">{{ $item->quantity }}</td>
                                            <td class="text-xs-right">AED {{ number_format($item->quantity*$item->amount) }}</td>
                                        </tr>
                                    @endforeach
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-7 col-sm-12 text-xs-center text-md-left">
                            <p class="lead">Payment:</p>
                            <div class="row">
                                <div class="col-md-8">
                                <table class="table table-borderless table-sm">
                                    <tbody>
                                        <tr>
                                            <td style="padding: 0">Shipping Method:</td>
                                            <td style="padding: 0" class="text-xs-right">
                                                @if($order->shipping_method == 'standard_delivery') Standard Delivery
                                                @elseif($order->shipping_method == 'pick_up') Pick up
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 0">Payment Method:</td>
                                            <td style="padding: 0" class="text-xs-right">
                                                @if($order->payment_method == 'COD') Cash on Delivery
                                                @else Online Payment by {{ $order->payment_method }}
                                                @endif
                                            </td>
                                        </tr>
                                        @if($order->txn_id)
                                            <tr>
                                                <td style="padding: 0">Transaction ID:</td>
                                                <td style="padding: 0" class="text-xs-right">
                                                    {{ $order->txn_id }}
                                                </td>
                                            </tr>
                                        @endif
                                        
                                    </tbody>
                                </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-12">
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                        @foreach ($order->items()->where('type','!=','Product')->get() as $item)
                                            <tr>
                                                <td class="text-bold-800">{{ $item->product_name }}</td>
                                                <td class="text-bold-800 text-xs-right"> AED {{ number_format($item->quantity*$item->amount) }}</td>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td class="text-bold-800">Total</td>
                                            <td class="text-bold-800 text-xs-right"> AED {{ number_format($order->amount) }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </div>
@endsection

@section('js')

    <script>
        $("#statusForm").change(function(e){
            var ele = this;
            e.preventDefault();
            
            swal({
                title: "Are you sure?",
                text: "The customer will be notified about this change.",
                type: "warning",
        
                showCancelButton: true,
        
            }).then(function(){
                ele.form.submit();
            }).catch(function(){
                $("#statusForm").val('');
                swal.noop;
            });
        });
    </script>
@endsection
