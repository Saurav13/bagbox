@extends('layouts.admin')

@section('body')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            {{-- <h2 class="content-header-title">Manage Downloads</h2> --}}
            <button class="btn btn-md btn-primary" data-toggle="modal" id="addDownloadButton" data-target="#addDownload"><i class="icon-plus-circle"></i> Add Download</button>
        </div>
        <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
            <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{URL::to('admin/dashboard')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">Manage Downloads
                    </li>
                </ol>
            </div>
        </div>
    </div>
    <section id="downloads">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Manage Downloads</h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block card-dashboard">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th>Title</th>
                                    <th>File</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($downloads as $c)
                                    <input type="hidden" id="DownloadName{{$c->id}}" value="{{$c->title}}"/>

                                    <tr>
                                        <th scope="row">{{ $c->id }}</th>
                                        <td>{{$c->title}}</td>
                                        <td><a href="/download-files/{{$c->file}}" target="_blank"><i class="icon-file-o"></i></a></td>
                                        
                                        <td>
                                            <button class="btn btn-outline-warning btn-sm" title="Edit" data-toggle="modal" data-target="#editDownload" id="DownloadEdit{{$c->id}}" c_id="{{$c->id}}"><i class="icon-edit2"></i> Edit</button>
                                            <button class="btn btn-sm btn-outline-red" id="deletecat{{$c->id}}"><i class="icon-remove"></i> Delete</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        
                        <div class="text-xs-center mb-3">
                            <nav aria-label="Page navigation">
                                {{ $downloads->links() }}
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
                    

    {{-- Add Download Modal --}}
    <div  class="modal fade" id="addDownload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel19" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form id="savechanges" method="POST" class="modal-content" enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="cattitle">Add Download</h4>
                </div>
                {{csrf_field()}}
                <div class="modal-body">
                    <label>Title</label>
                    <input type="text" id="addDownloadsName"  required name="title" class="form-control" placeholder="Name of Download"/>
                    <br>
                   
                    <label>File</label>
                    
                    <input type="file" id="addDownloadsImage" required name="file" class="form-control" >

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" >Add Download</button>
                </div>
            </form>
        </div>
    </div>
    {{-- End Add Download Modal --}}
    {{-- Edit Download Modal --}}

    <div  class="modal fade" id="editDownload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel20" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <form id="editchanges" method="POST" enctype="multipart/form-data"  action="{{route('download.update')}}" class="modal-content">
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="cattitle">Edit Download</h4>
                    </div>
                    {{csrf_field()}}
                    <input type="text"  hidden id="editDownloadsId"  required name="id" class="form-control" placeholder="Name of Download"/>

                    <div class="modal-body">
                        <label>Title</label>
                        <input type="text" id="editDownloadsName"  required name="title" class="form-control" placeholder="Name of Download"/>
                        <br>
                        <label>File</label>
                        
                        <input type="file" id="editDownloadsImage" name="file" class="form-control" >
    
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" >Save </button>
                    </div>
        </form>
        </div>
    </div>    
    {{-- EndEdit Download Modal --}}

    {{-- Add Sub-download Modal --}}

 
@endsection

@section('js')
    <script>

        $(document).ready(function(){
            $("[id*='DownloadEdit']").click(function(){
                var id = $(this).attr("c_id");
                var name=$('#DownloadName'+id).val();

                $('#editDownloadsId').val(id);
                $('#editDownloadsName').val(name);

            });

        

        });

        

        $(document).on('click',"[id*='deletecat']",function(event){
            var id = $(this).attr("id").slice(9);
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this file!",
                type: "warning",
        
                showCancelButton: true,
        
            }).then(function(){
                $.post("{{route('download.delete')}}",{id:id,_token:"{{csrf_token()}}"},function(data){
                    swal({
                        title:"Deleted Successfully",
                        type:"success"
            
                    }).then(function(){
                        location.reload();
                    })
                })
            });
        });



    </script>
@endsection