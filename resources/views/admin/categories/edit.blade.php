@extends('layouts.admin')

@section('body')

    <div class="content-header row">
    </div>
    <div class="content-body">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Edit Category</h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block card-dashboard">
                    <form class="form" method="POST" id="" action="{{ route('category.update', $category->id) }}">
                        @csrf
                        <h4 class="form-section"><i class="icon-information"></i> Category Info</h4>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="category_name">Name <span class="text-danger">*</span></label>
                                    <input type="text" id="category_name" class="form-control" placeholder="Category Name" value="{{ $category->cat_name }}" name="name" required>
                                    @if ($errors->has('name'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea id="description" rows="5" class="form-control" placeholder="Category Description" name="description">{{ $category->description }}</textarea>
                                    @if ($errors->has('description'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('description') }}</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="order">Order of Category</label>
                                    <input type="text" id="order" class="form-control" placeholder="Category Order" value="{{ $category->sort }}" name="sort" required>
                                </div>
                            </div>
                        </div>
    
                        <h4 class="form-section"><i class="icon-cog"></i> SEO Meta Tags</h4>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="meta_title">Meta Title</label>
                                    <input type="text" id="meta_title" class="form-control" placeholder="Meta Title" value="{{ $category->meta_title }}" name="meta_title">
                                    @if ($errors->has('meta_title'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('meta_title') }}</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="meta_description">Meta Description</label>
                                    <textarea rows="3" id="meta_description" class="form-control" placeholder="Meta Description" name="meta_description">{{ $category->meta_description }}</textarea>
                                    @if ($errors->has('meta_description'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('meta_description') }}</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="meta_keywords">Meta Keywords</label>
                                    <input type="text" id="meta_keywords" class="form-control" placeholder="Meta Keywords" value="{{ $category->meta_keywords }}" name="meta_keywords">
                                    @if ($errors->has('meta_keywords'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('meta_keywords') }}</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-actions pull-right">
                            <a href="{{ route('category.index') }}" class="btn btn-warning mr-1">
                                <i class="icon-cross2"></i> Cancel
                            </a>
                            <button type="submit" class="btn btn-primary">
                                <i class="icon-check2"></i> Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="//cdn.tiny.cloud/1/i54lxo7292e00pntywltqhso8u1q9mrye4m6pz78losx9jjr/tinymce/5/tinymce.min.js"></script>
    
    <script>
       tinymce.init({
            selector: "textarea#description",
            height: '300px',
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            toolbar2: "print preview | forecolor backcolor emoticons | template",
            image_advtab: true,
        });
    </script>
  
@endsection