@extends('layouts.admin')

@section('body')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Pages</h2>
        </div>
    </div>
    <div class="content-body">
        <div class="card">
            <div class="card-body collapse in">
                <div class="card-block">
                    <ul class="nav nav-tabs">
                        
                        <li class="nav-item">
                            <a class="nav-link {{$active=='about'?'active':''}}" id="aboutUs-tab" data-toggle="tab" href="#aboutUs" aria-controls="aboutUs" aria-expanded="false" onclick="window.history.pushState( {} , '', '?active=about' );">AboutUs</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{$active=='privacy_policy'?'active':''}}" id="privacy_policy-tab" data-toggle="tab" href="#privacy_policy" aria-controls="privacy_policy" aria-expanded="true" onclick="window.history.pushState( {} , '', '?active=privacy_policy' );">Privacy Policy</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link  {{$active=='terms_and_conditions'?'active':''}}" id="terms_and_conditions-tab" data-toggle="tab" href="#terms_and_conditions" aria-controls="terms_and_conditions" aria-expanded="false" onclick="window.history.pushState( {} , '', '?active=terms_and_conditions' );">Terms and Conditions</a>
                        </li>
                    </ul>
                    <div class="tab-content px-1 pt-1">
                    
                        <div class="tab-pane fade {{$active=='about'?'active in':''}}" id="aboutUs" role="tabpanel" aria-labelledby="aboutUs-tab" aria-expanded="false">
                            <form class="form" id="aboutform" method="POST" action="{{ route('pages.update','about') }}"  enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input name="image" type="file" id="upload" class="hidden" onchange="">
                                {{-- <div class="form-group">
                                    <label for="aboutImage"><strong>Image:</strong></label>
                                    @if ($errors->about->has('image'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->about->first('image') }}</strong>
                                        </div>
                                    @endif
                                    <input type="file" id="aboutImage" class="form-control" name="image"/>
                                    
                                    <div>
                                        <img src="{{ route('optimize', ['landing_images',$about->photo,100,100]) }}" alt="About Us image" style="width:100px;height: 100px;" />
                                    </div>
                                </div> --}}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="meta_title" style="font-weight: 600">Meta Title</label>
                                            <input type="text" id="meta_title" class="form-control" placeholder="Meta Title" value="{{ $about->meta_title }}" name="meta_title">
                                            @if ($errors->about->has('meta_title'))
                                                <div class="alert alert-danger no-border mb-2">
                                                    <strong>{{ $errors->about->first('meta_title') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="meta_description" style="font-weight: 600">Meta Description</label>
                                            <textarea rows="3" id="meta_description" class="form-control" placeholder="Meta Description" name="meta_description">{{ $about->meta_description }}</textarea>
                                            @if ($errors->about->has('meta_description'))
                                                <div class="alert alert-danger no-border mb-2">
                                                    <strong>{{ $errors->about->first('meta_description') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="meta_keywords" style="font-weight: 600">Meta Keywords</label>
                                            <input type="text" id="meta_keywords" class="form-control" placeholder="Meta Keywords" value="{{ $about->meta_keywords }}" name="meta_keywords">
                                            @if ($errors->about->has('meta_keywords'))
                                                <div class="alert alert-danger no-border mb-2">
                                                    <strong>{{ $errors->about->first('meta_keywords') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="content" style="font-weight: 600">Content</label>

                                    @if ($errors->about->has('content'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->about->first('content') }}</strong>
                                        </div>
                                    @endif
                                    <div class="editable">
                                        {!! $about->content !!}
                                    </div>
                                </div>
                                <div class="form-actions right">
                                    <button id="aboutsubmit" class="btn btn-primary">
                                        <i class="icon-check2"></i> Save
                                    </button>
                                </div>
                            </form>
                            
                        </div>

                        <div class="tab-pane fade {{$active=='privacy_policy'?'active in':''}}" id="privacy_policy" role="tabpanel" aria-labelledby="privacy_policy-tab" aria-expanded="false">
                            <form class="form" id="privacyform" method="POST" action="{{ route('pages.update','privacy_policy') }}">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="meta_title" style="font-weight: 600">Meta Title</label>
                                            <input type="text" id="meta_title" class="form-control" placeholder="Meta Title" value="{{ $privacy_policy->meta_title }}" name="meta_title">
                                            @if ($errors->privacy_policy->has('meta_title'))
                                                <div class="alert alert-danger no-border mb-2">
                                                    <strong>{{ $errors->privacy_policy->first('meta_title') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="meta_description" style="font-weight: 600">Meta Description</label>
                                            <textarea rows="3" id="meta_description" class="form-control" placeholder="Meta Description" name="meta_description">{{ $privacy_policy->meta_description }}</textarea>
                                            @if ($errors->privacy_policy->has('meta_description'))
                                                <div class="alert alert-danger no-border mb-2">
                                                    <strong>{{ $errors->privacy_policy->first('meta_description') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="meta_keywords" style="font-weight: 600">Meta Keywords</label>
                                            <input type="text" id="meta_keywords" class="form-control" placeholder="Meta Keywords" value="{{ $privacy_policy->meta_keywords }}" name="meta_keywords">
                                            @if ($errors->privacy_policy->has('meta_keywords'))
                                                <div class="alert alert-danger no-border mb-2">
                                                    <strong>{{ $errors->privacy_policy->first('meta_keywords') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="content" style="font-weight: 600">Content</label>
                                    @if ($errors->privacy_policy->has('content'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->privacy_policy->first('content') }}</strong>
                                        </div>
                                    @endif
                                    <div class="editable">
                                        {!! $privacy_policy->content !!}
                                    </div>
                                </div>
                                <div class="form-actions right">
                                    <button id="privacysubmit" class="btn btn-primary">
                                        <i class="icon-check2"></i> Save
                                    </button>
                                </div>
                            </form>
                            
                        </div>

                        <div class="tab-pane fade {{$active=='terms_and_conditions'?'active in':''}}" id="terms_and_conditions" role="tabpanel" aria-labelledby="terms_and_conditions-tab" aria-expanded="false">
                            <form class="form" id="termsform" method="POST" action="{{ route('pages.update','terms_and_conditions') }}">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="meta_title" style="font-weight: 600">Meta Title</label>
                                            <input type="text" id="meta_title" class="form-control" placeholder="Meta Title" value="{{ $terms_and_conditions->meta_title }}" name="meta_title">
                                            @if ($errors->terms_and_conditions->has('meta_title'))
                                                <div class="alert alert-danger no-border mb-2">
                                                    <strong>{{ $errors->terms_and_conditions->first('meta_title') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="meta_description" style="font-weight: 600">Meta Description</label>
                                            <textarea rows="3" id="meta_description" class="form-control" placeholder="Meta Description" name="meta_description">{{ $terms_and_conditions->meta_description }}</textarea>
                                            @if ($errors->terms_and_conditions->has('meta_description'))
                                                <div class="alert alert-danger no-border mb-2">
                                                    <strong>{{ $errors->terms_and_conditions->first('meta_description') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="meta_keywords" style="font-weight: 600">Meta Keywords</label>
                                            <input type="text" id="meta_keywords" class="form-control" placeholder="Meta Keywords" value="{{ $terms_and_conditions->meta_keywords }}" name="meta_keywords">
                                            @if ($errors->terms_and_conditions->has('meta_keywords'))
                                                <div class="alert alert-danger no-border mb-2">
                                                    <strong>{{ $errors->terms_and_conditions->first('meta_keywords') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="content" style="font-weight: 600">Content</label>

                                    @if ($errors->terms_and_conditions->has('content'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->terms_and_conditions->first('content') }}</strong>
                                        </div>
                                    @endif
                                    <div class="editable">
                                        {!! $terms_and_conditions->content !!}
                                    </div>
                                </div>
                                <div class="form-actions right">
                                    <button id="termssubmit" class="btn btn-primary">
                                        <i class="icon-check2"></i> Save
                                    </button>
                                </div>
                            </form>
                            
                        </div>
                    </div>   
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')

    <script src="//cdn.tiny.cloud/1/i54lxo7292e00pntywltqhso8u1q9mrye4m6pz78losx9jjr/tinymce/5/tinymce.min.js"></script>
    
	<script>
		tinymce.init({
            selector: 'div.editable',
            inline: true,
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons paste textcolor colorpicker textpattern"
            ],
            toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            toolbar2: "print preview | forecolor backcolor emoticons",
            image_advtab: true,
            file_picker_callback: function(callback, value, meta) {
                if (meta.filetype == 'image') {
                    $('#upload').trigger('click');
                    $('#upload').on('change', function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        callback(e.target.result, {
                        alt: ''
                        });
                    };
                    reader.readAsDataURL(file);
                    });
                }
            }
        });

        $('#aboutsubmit').click(function(e){
            e.preventDefault();
            param = $('.editable').eq(0).html();
            param = "<textarea name='content' hidden>" + param + "</textarea>";

            // append it to the form
            $('#aboutform').append(param);
            this.form.submit();
        });
        $('#privacysubmit').click(function(e){
            e.preventDefault();
            param = $('.editable').eq(1).html();
            param = "<textarea name='content' hidden>" + param + "</textarea>";

            // append it to the form
            $('#privacyform').append(param);
            this.form.submit();
        });
        $('#termssubmit').click(function(e){
            e.preventDefault();
            param = $('.editable').eq(2).html();
            param = "<textarea name='content' hidden>" + param + "</textarea>";

            // append it to the form
            $('#termsform').append(param);
            this.form.submit();
        });
    </script>
@endsection