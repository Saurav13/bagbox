@extends('layouts.admin')

@section('css') 

<link href="/admin-assets/dist/styles.imageuploader.css" rel="stylesheet" type="text/css">
<link href="/admin-assets/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet" type="text/css">
<style>
    .editable_input{
        width: 400px!important;
    }
</style>
@endsection

@section('body')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Settings</h2>
        </div>
    </div>
    <div class="content-body">
        <div class="card">
            <div class="card-body collapse in">
                <div class="card-block">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link {{$active=='home'?'active':''}}" id="home-tab" data-toggle="tab" href="#home" aria-controls="home" aria-expanded="true" onclick="window.history.pushState( {} , '', '?active=home' );">Social Links</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link  {{$active=='contact'?'active':''}}" id="contact-tab" data-toggle="tab" href="#contact" aria-controls="contact" aria-expanded="false" onclick="window.history.pushState( {} , '', '?active=contact' );">Contacts Settings</a>
                        </li>
                        {{-- <li class="nav-item">
                            <a class="nav-link {{$active=='about'?'active':''}}" id="aboutUs-tab" data-toggle="tab" href="#aboutUs" aria-controls="aboutUs" aria-expanded="false" onclick="window.history.pushState( {} , '', '?active=about' );">AboutUs Settings</a>
                        </li> --}}
                    </ul>
                    <div class="tab-content px-1 pt-1">
                        <div role="tabpanel" class="tab-pane fade {{$active=='home'?'active':''}} in" id="home" aria-labelledby="home-tab" aria-expanded="true">
                            @foreach($social_links as $social=>$link)
                                <p><strong style="text-transform: capitalize">{{ $social }}     : </strong> <a href="javascript:void(0);" data-id="{{ $social }}" id="{{ $social }}" class="social-links">{{ $link }}</a> </p>
                            @endforeach
                            <div style="height:199px"></div>
                        </div>
                        <div class="tab-pane fade {{$active=='contact'?'active in':''}}" id="contact" role="tabpanel" aria-labelledby="contact-tab" aria-expanded="false">
                            <p><strong>Address     : </strong> <a href="javascript:void(0);" id="address">{{ $contact->address }}</a> </p>
                            <p><strong>Email    : </strong> <a href="javascript:void(0);" id="email">{{ $contact->email }}</a></p>
                            <p><strong>Phone No.: </strong> <a href="javascript:void(0);" id="phone">{{ $contact->phone }}</a> </p>
                            <p><strong>Fax: </strong> <a href="javascript:void(0);" id="fax">{{ $contact->fax }}</a> </p>
                            <p><strong>Mobile No.: </strong> <a href="javascript:void(0);" id="mobile">{{ $contact->mobile }}</a> </p>
                            <p><strong>Post Box: </strong> <a href="javascript:void(0);" id="postbox">{{ $contact->postbox }}</a> </p>
                            <div style="height:199px"></div>
                        </div>
                        <div class="tab-pane fade {{$active=='about'?'active in':''}}" id="aboutUs" role="tabpanel" aria-labelledby="aboutUs-tab" aria-expanded="false">
                            <form class="form" id="aboutform" method="POST" action="{{ route('admin.settings.aboutUpdate') }}"  enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input name="image" type="file" id="upload" class="hidden" onchange="">
                                <div class="form-group">
                                    <label for="aboutImage"><strong>Image:</strong></label>
                                    @if ($errors->has('aboutImage'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('aboutImage') }}</strong>
                                        </div>
                                    @endif
                                    <input type="file" id="aboutImage" class="form-control" name="aboutImage"/>
                                    
                                    <div>
                                        <img src="{{ route('optimize', ['landing_images',$about->photo,100,100]) }}" alt="About Us image" style="width:100px;height: 100px;" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label><strong>About Us:</strong></label>
                                    @if ($errors->has('aboutContent'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('aboutContent') }}</strong>
                                        </div>
                                    @endif
                                    <div class="editable">
                                        {!! $about->content !!}
                                    </div>
                                </div>
                                <div class="form-actions right">
                                    <button id="aboutsubmit" class="btn btn-primary">
                                        <i class="icon-check2"></i> Save
                                    </button>
                                </div>
                            </form>
                            
                        </div>
                    </div>   
                </div>
            </div>
        </div>
    </div>
  
    <!-- Modal -->
    <div class="modal fade text-xs-left" id="uploadImagesModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel5" aria-hidden="true" style="min-height:500px;min-width:500px;">
        <div class="modal-dialog" role="document">

            <div style="text-align:right">             
                <button id="cross"  type="button" class="btn btn-outline-secondary" data-dismiss="modal" style="background-color: #185284;color: #fff;">&times;</button>
            </div>            
            <div class=" uploader__box js-uploader__box l-center-box">
                <form class="form" enctype="multipart/form-data" method="POST" action="{{ route('admin.settings.addImage') }}">
                       
                    {{ csrf_field() }}
                    <div class="form-body">
                        <div class="uploader__contents">
                            <label class="button button--secondary" for="fileinput">Select Files</label>
                            <input id="fileinput" class="uploader__file-input" type="file" multiple value="Select Files">
                        </div>
                        <button class="button button--big-bottom" type="submit">Upload Selected Files</button>
                    </div>

                </form> 
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="/admin-assets/dist/jquery.imageuploader.js"></script>
    <script src="/admin-assets/bootstrap3-editable/js/bootstrap-editable.js"></script>
	<script>
		(function(){
            var options = {'ajaxUrl':"{{ route('admin.settings.addImage') }}"};
            $('.js-uploader__box').uploader(options);
        }());
        $.fn.editable.defaults.mode = 'inline';
        $('#address').editable({
            type: 'text',
            name: 'address',
            url: '{{route("admin.settings.contactUpdate")}}',
            title: 'Enter address',
            send: 'always',
            inputclass: 'editable_input',
            validate: function(value) {
                if($.trim(value) == '') {
                    return 'This field is required';
                }
            }
        });
        $('#email').editable({
            type: 'text',
            name: 'email',
            url: '{{route("admin.settings.contactUpdate")}}',
            title: 'Enter emails',
            send: 'always',
            validate: function(value) {
                if($.trim(value) == '') {
                    return 'This field is required';
                }
            }
        });
        $('#phone').editable({
            type: 'text',
            name: 'phone',
            url: '{{route("admin.settings.contactUpdate")}}',
            title: 'Enter phone',
            send: 'always',
            validate: function(value) {
                if($.trim(value) == '') {
                    return 'This field is required';
                }
            }
        });
        $('#fax').editable({
            type: 'text',
            name: 'fax',
            url: '{{route("admin.settings.contactUpdate")}}',
            title: 'Enter Fax No.',
            send: 'always',
            validate: function(value) {
                if($.trim(value) == '') {
                    return 'This field is required';
                }
            }
        });
        $('#mobile').editable({
            type: 'text',
            name: 'mobile',
            url: '{{route("admin.settings.contactUpdate")}}',
            title: 'Enter Mobile No.',
            send: 'always',
            validate: function(value) {
                if($.trim(value) == '') {
                    return 'This field is required';
                }
            }
        });
        $('#postbox').editable({
            type: 'text',
            name: 'postbox',
            url: '{{route("admin.settings.contactUpdate")}}',
            title: 'Enter postbox',
            send: 'always',
            validate: function(value) {
                if($.trim(value) == '') {
                    return 'This field is required';
                }
            }
        });

        $('.social-links').editable({
            type: 'text',
            name: $(this).data('id'),
            url: '{{route("admin.settings.editSocials")}}',
            title: 'Enter Link',
            send: 'always',
            inputclass: 'editable_input',
            validate: function(value) {
                if($.trim(value) == '') {
                    return 'This field is required';
                }
            }
        });

    </script>
    
    <script>
       $("[id*='delete']").click(function(e){
           var ele = this;
           e.preventDefault();
           
           swal({
           title: "Are you sure?",
           text: "You will not be able to recover this record!",
           type: "warning",

           showCancelButton: true,

           }).then(function(){
           ele.form.submit();
           }).catch(swal.noop);

       });
    </script>
@endsection