@extends('layouts.admin')

@section('body')
    <div class="content-header row">
        {{-- <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Manage Infos</h2>
            <button class="btn btn-md btn-primary" data-toggle="modal" id="addInfoButton" data-target="#addInfo"><i class="icon-plus-circle"></i> Add Info</button>
        </div> --}}
        <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
            <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{URL::to('admin/dashboard')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">Manage Infos
                    </li>
                </ol>
            </div>
        </div>
    </div>
    @foreach($infos as $c)
        <section id="infos">
            <div class="row" >
                <div class="col-xs-12" >
                    <div class="card">
                        <div class="card-header">
                            <input type="hidden" id="InfoTitle{{$c->id}}" value="{{$c->title}}"/> 
                            <input type="hidden" id="InfoDesc{{$c->id}}" value="{{$c->description}}"/> 
                            
                            <h3 class="card-title"><a data-action="collapse"><i class="icon-minus4"></i></a><a data-action="collapse"> {{$c->title}}</a>
                            </h3>
                            <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><button class="btn btn-sm btn-outline-blue" data-toggle="modal" data-target="#editInfo" info_id="{{$c->id}}" id="InfoEdit{{$c->id}}"><i class="icon-edit2"></i> Edit</button> </li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-body collapse in" style="padding-top: 5px;">
                            <div class="card-block"  style="padding-top:5px;">
                                
                                <!-- Content types section start -->
                                <section id="content-types">
                                    <div class="row">
                                        <div class="container">
                                        <p  >{{$c->description}}</p>
                                        @if($c->image)
                                        <p>Background Image:</p>
                                    <img src="/info_images/{{$c->image}}" style="height:10rem"> 
                                        @endif
                                        </div>
                                        
                                    </div>
                                    
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
                    
    @endforeach

    {{-- Add Info Modal --}}
    <div  class="modal fade" id="addInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel19" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form id="savechanges" class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="cattitle">Add Info</h4>
                </div>
                <div class="modal-body">
                    <input type="text" id="addCatgeoryName" class="form-control" placeholder="Name of Info"/>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" >Add Info</button>
                </div>
            </form>
        </div>
    </div>
    {{-- End Add Info Modal --}}
    {{-- Edit Info Modal --}}

    <div  class="modal fade" id="editInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel20" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form id="editchanges" class="modal-content" action="{{route('info.edit')}}" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Edit Info Name</h4>
                </div>
                <div class="modal-body">
                    <input type="text" name="title" id="editInfoTitle" class="form-control" placeholder="Title"/>
                </div>
                <div class="modal-body">
                    <textarea id="editInfoDesc" name="description" class="form-control" placeholder="Description"></textarea>
                </div>
                <div class="modal-body">
                    <input type="file" name="image" class="form-control" placeholder="Title"/>
                </div>
                <input type="hidden" id="editInfoId" name="id" value=""/>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" >Save Info</button>
                </div>
            </form>
        </div>
    </div>    
    {{-- EndEdit Info Modal --}}

    {{-- Add Sub-info Modal --}}

 
@endsection

@section('js')
    <script>

        $(document).ready(function(){
            $("[id*='InfoEdit']").click(function(){
                var id = $(this).attr("info_id");
                var title=$('#InfoTitle'+id).val();
                var description=$('#InfoDesc'+id).val();
                console.log(title);
                $('#editInfoId').val(id);
                $('#editInfoTitle').val(title);
                $('#editInfoDesc').val(description);

            });


            // $('#editchanges').on('submit', function(event){
            //     event.preventDefault();
            //     var title = $('#editInfoTitle').val();
            //     var description = $('#editInfoDesc').val();

            //     var id=$('#editInfoId').val();
            //     $.post("{{route('info.edit')}}",{'id':id,'title':title,'description''_token':$('input[name=_token]').val()},    function(data){
            //         swal({
            //             title: "Success",
            //             text: "The Info was updated",
            //             type: "success",

            //             showCancelButton: true,

            //         }).then(function(){
                    
            //             location.reload();
            //         })
                        
            //     })
            //     .fail(function(response) {
            //         alert('Error: The given data is Invalid');
            //     });
            // });
        });

        

      



    </script>
@endsection