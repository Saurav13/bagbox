@extends('layouts.admin')

@section('body')

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Message #{{ $quotation->id }}</h2>
        </div>
        <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
            <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                <a class="btn btn-primary btn-min-width mr-1 mb-1 " href="{{ route('all.quotations') }}">See all Quotations</a>
                </ol>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section class="card">
            <div id="invoice-template" class="card-block">
                <!-- Invoice Company Details -->
                <div id="invoice-company-details" class="row">
                    <div class="col-md-6 col-sm-12 text-xs-center text-md-left">
                        <ul class="px-0 list-unstyled">
                            <li><strong>Name      : </strong>{{ $quotation->name }}</li>
                            <li><strong>Phone No. : </strong>{{ $quotation->phone }}</li>
                            <li><strong>Address   : </strong>{{ $quotation->address }}</li>
                            <li><strong>Email     : </strong>{{ $quotation->email }}</li>   <br>                             
                            <li><span class="text-muted">Quotation Date :</span> {{ date('M j, Y', strtotime($quotation->created_at)) }}</li>
                        </ul>
                    </div>
                </div>
                <!--/ Invoice Company Details -->

                <!-- Invoice Footer -->
                <div id="invoice-items-details" class="pt-2">
                    <div class="row">
                        <div class="col-sm-12">
                            @if($quotation->message)
                                <p class="lead">Message:</p>
                                <p>{{ $quotation->message }}</p>
                                <br>
                            @endif
                            <h2>Quotation(s)</h2>
                            <table class="table">
                                <thead>
                                    {{-- <th>#</th> --}}
                                    <th width="45%">Product</th>
                                    <th width="15%">Price</th>

                                    <th width="10%">Quantity</th>
            
                                </thead>
                                <tbody>
                                    
                                    <tr>
                                        <td>{{ $quotation->product_name }}</td>
                                        <td>AED {{ number_format($quotation->price) }} </td>

                                        <td>{{ $quotation->quantity > 0 ? $quotation->quantity : 'Not sure' }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--/ Invoice Footer -->

            </div>
        </section>
    </div>

@endsection
