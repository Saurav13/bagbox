@extends('layouts.admin')

@section('body')

    <div class="content-header row">
    </div>
    <div class="content-body">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Edit Brand</h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block card-dashboard">
                    <form class="form" method="POST" action="{{ route('brands.update',$brand->id) }}"  enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PATCH" >

                        <div class="form-body">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input class="form-control{{ $errors->has('name') ? ' border-danger' : '' }}" id="name" type="text" class="form-control" name="name" value="{{ $brand->name }}" required>

                                        @if ($errors->has('name'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Logo (Optional)</label>
                                <input class="form-control{{ $errors->has('logo') ? ' border-danger' : '' }}" type="file" placeholder="File"  name="logo">

                                @if ($errors->has('logo'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('logo') }}</strong>
                                    </div>
                                @endif
                                @if($brand->logo) 
                                    Current Logo:
                                    
                                    <img src="{{ '/brand_logos/'.$brand->logo }}" style="max-height: 5rem"/>
                                
                                @endif
                            </div>
                        </div>

                        <div class="form-actions right">
                            <a class="btn btn-warning" href="{{ route('brands.index') }}">Cancel</a>

                            <button type="submit" class="btn btn-primary">
                                <i class="icon-check2"></i> Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
   <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
   <script>
       tinymce.init({
           selector: "textarea.content",
           
           plugins: [
               "advlist autolink lists link charmap print preview hr anchor pagebreak",
               "searchreplace wordcount visualblocks visualchars code",
               "insertdatetime media nonbreaking save table contextmenu directionality",
               "emoticons template paste textcolor colorpicker textpattern"
           ],
           toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link",
           toolbar2: "print preview | forecolor backcolor emoticons"
       });
   </script>
  
@endsection