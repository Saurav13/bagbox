@extends('layouts.admin')

@section('body')
   
    <div class="content-header row">

    <div class="content-body">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title"><a data-action="collapse">Add New Brand</a></h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="{{ count($errors)>0 ? 'icon-minus4':'icon-plus4' }}"></i></a></li>
                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse {{ count($errors)>0 ? 'in':'out' }}">
                <div class="card-block card-dashboard">
                    <form class="form" method="POST" id="AddBlogForm" action="{{ route('brands.store') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">
                                <h4 class="form-section"><i class="icon-eye6"></i> Brand</h4>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="name">Name</label>
                                            <input class="form-control{{ $errors->has('name') ? ' border-danger' : '' }}" id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required>

                                            @if ($errors->has('name'))
                                                <div class="alert alert-danger no-border mb-2">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Logo (Optional)</label>
                                    <input class="form-control{{ $errors->has('logo') ? ' border-danger' : '' }}" type="file" placeholder="File"  name="logo">

                                    @if ($errors->has('logo'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('logo') }}</strong>
                                        </div>
                                    @endif
                                </div>
                        </div>

                        <div class="form-actions right">
                            <button type="submit" class="btn btn-primary">
                                <i class="icon-check2"></i> Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Brands</h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block card-dashboard">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Products</th>
                                    <th>Logo</th>
                                    <th width="20%">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($brands as $brand)
                                <tr>
                                    <td>{{ $loop->iteration + (($brands->currentPage()-1) * $brands->perPage()) }}</td>
                                    <td>{{ $brand->name }}</td>
                                    <td>{{ $brand->products()->count() }}</td>
                                    <td>
                                            @if($brand->logo) 
                                                <img src="{{ '/brand_logos/'.$brand->logo }}" style="max-height: 3rem"/>
                                            @else 
                                                - 
                                            @endif
                                        </td>
                                    <td>

                                        <a class="btn btn-outline-warning" title="Update Details" href="{{ route('brands.edit',$brand->id) }}"><i class="icon-edit"></i></a>
                                        
                                        <form action="{{ route('brands.destroy',$brand->id) }}" method="POST" style="display:inline">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE" >
                                            <button id='deleteBrand{{ $brand->id }}' type="button" class="btn btn-outline-danger"><i class="icon-trash-o"></i></button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>


                        <div class="text-xs-center mb-3">
                            <nav aria-label="Page navigation">
                                {{ $brands->links() }}
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

   
@endsection

@section('js')
  
@endsection