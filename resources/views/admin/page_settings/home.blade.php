@extends('layouts.admin')

@section('title', 'Home Page Settings')

@section('css') 

@endsection

@section('body')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title" style="text-transform: capitalize;">
                {{ str_replace('_', ' ', $page) }}

            </h2>
        </div>
    </div>
    <div class="content-body">
        <div class="card">
            <div class="card-body collapse in">
                <div class="card-block">
                    <form class="form" method="POST" action="{{ route('admin.seo-settings.update', $page) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <h4 class="form-section"><i class="icon-cog"></i> SEO Meta Tags</h4>
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="meta_title" style="font-weight: 600">Meta Title</label>
                                        <input type="text" id="meta_title" class="form-control" placeholder="Meta Title" value="{{ $meta_title }}" name="meta_title">
                                        @if ($errors->has('meta_title'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('meta_title') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="meta_description" style="font-weight: 600">Meta Description</label>
                                        <textarea rows="3" id="meta_description" class="form-control" placeholder="Meta Description" name="meta_description">{{ $meta_description }}</textarea>
                                        @if ($errors->has('meta_description'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('meta_description') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="meta_keywords" style="font-weight: 600">Meta Keywords</label>
                                        <input type="text" id="meta_keywords" class="form-control" placeholder="Meta Keywords" value="{{ $meta_keywords }}" name="meta_keywords">
                                        @if ($errors->has('meta_keywords'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('meta_keywords') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
 
                        </div>
 
                        <div class="form-actions right">
                            <button type="submit" class="btn btn-primary">
                                <i class="icon-check2"></i> Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection