@extends('layouts.admin')

@section('body')
    <style>
        .switch input { 
            display:none;
        }
        .switch {
            display:inline-block;
            width:30px;
            height:15px;
            margin:8px;
            transform:translateY(50%);
            position:relative;
        }
        
        .slider {
            position:absolute;
            top:0;
            bottom:0;
            left:0;
            right:0;
            border-radius:15px;
            box-shadow:0 0 0 2px #777, 0 0 4px #777;
            cursor:pointer;
            border:4px solid transparent;
            overflow:hidden;
            transition:.4s;
        }
        .slider:before {
            position:absolute;
            content:"";
            width:100%;
            height:100%;
            background:#777;
            border-radius:15px;
            transform:translateX(-15px);
            transition:.4s;
        }
        
        input:checked + .slider:before {
            transform:translateX(15px);
            background:limeGreen;
        }
        input:checked + .slider {
            box-shadow:0 0 0 2px limeGreen,0 0 2px limeGreen;
        }
        
    </style>
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Manage Clients</h2>
            <button class="btn btn-md btn-primary" data-toggle="modal" id="addClientButton" data-target="#addClient"><i class="icon-plus-circle"></i> Add Client</button>
        </div>
        <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
            <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{URL::to('admin/dashboard')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">Manage Clients
                    </li>
                </ol>
            </div>
        </div>
    </div>
        <section id="clients">
            <div class="row" >
            @foreach($clients as $c)

                <div class="col-xs-2" >
                    <div class="card">
                        <div class="card-header">
                            <input type="hidden" id="ClientName{{$c->id}}" value="{{$c->name}}"/> 
                            <input type="hidden" id="ClientLink{{$c->id}}" value="{{$c->link}}"/> 
                            
                            <h3 class="card-title"> {{$c->name}}</a>
                            </h3>
                            <div class="text-center w-100">
                            <img src="/client-images/{{$c->image}}" style="height:5rem" class="img-fluid"/>
                            </div>
                            {{-- <div class="heading-elements"> --}}
                                <ul class="list-inline mb-0">
                                    <li><button class="btn btn-sm btn-outline-blue" data-toggle="modal" data-target="#editClient" id="ClientEdit{{$c->id}}" c_id="{{$c->id}}"><i class="icon-edit2"></i></button> </li>
                                    <li> <button class="btn btn-sm btn-outline-red" id="deletecat{{$c->id}}"><i class="icon-remove"></i></button></li>
                                    
                                </ul>
                            {{-- </div> --}}
                        </div>
                        <div class="card-body collapse in" style="padding-top: 5px;">
                            <div class="card-block"  style="padding-top:5px;">
                                
                                <!-- Content types section start -->
                                <section id="content-types">
                                    <div class="row">
                                        <label class="switch">
                                    
                                            <input id="feature{{$c->id}}" type="checkbox"
                                            @if($c->featured == true)
                                                checked
                                            @endif
                                            >
                                            <span class="slider"></span>
                                        </label>
                                        <span><b>Featured</b></span>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

            </div>

            {{$clients->links()}}
        </section>
                    

    {{-- Add Client Modal --}}
    <div  class="modal fade" id="addClient" tabindex="-1" role="dialog" aria-labelledby="myModalLabel19" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form id="savechanges" method="POST" class="modal-content" enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="cattitle">Add Client</h4>
                </div>
                {{csrf_field()}}
                <div class="modal-body">
                    <label>Name</label>
                    <input type="text" id="addClientsName"  required name="name" class="form-control" placeholder="Name of Client"/>
                    <br>
                    <label>Link</label>
                    <input type="text" id="addClientsLink" name="link" class="form-control" placeholder="(Optional)"/>

                    <label>Image</label>
                    
                    <input type="file" id="addClientsImage" required name="image" class="form-control" >

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" >Add Client</button>
                </div>
            </form>
        </div>
    </div>
    {{-- End Add Client Modal --}}
    {{-- Edit Client Modal --}}

    <div  class="modal fade" id="editClient" tabindex="-1" role="dialog" aria-labelledby="myModalLabel20" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <form id="editchanges" method="POST" enctype="multipart/form-data"  action="{{route('client.edit')}}" class="modal-content">
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="cattitle">Edit Client</h4>
                    </div>
                    {{csrf_field()}}
                    <input type="text"  hidden id="editClientsId"  required name="id" class="form-control" placeholder="Name of Client"/>

                    <div class="modal-body">
                        <label>Name</label>
                        <input type="text" id="editClientsName"  required name="name" class="form-control" placeholder="Name of Client"/>
                        <br>
                        <label>Link</label>
                        <input type="text" id="editClientsLink" name="link" class="form-control" placeholder="(Optional)"/>
    
                        <label>Image</label>
                        
                        <input type="file" id="editClientsImage" name="image" class="form-control" >
    
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" >Save </button>
                    </div>
        </form>
        </div>
    </div>    
    {{-- EndEdit Client Modal --}}

    {{-- Add Sub-client Modal --}}

 
@endsection

@section('js')
    <script>

        $(document).ready(function(){
            $("[id*='ClientEdit']").click(function(){
                var id = $(this).attr("c_id");
                var name=$('#ClientName'+id).val();
                var link=$('#ClientLink'+id).val();

                $('#editClientsId').val(id);
                $('#editClientsName').val(name);
                $('#editClientsLink').val(link);

            });

        

        });

        

        $(document).on('click',"[id*='deletecat']",function(event){
            var id = $(this).attr("id").slice(9);
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this file!",
                type: "warning",
        
                showCancelButton: true,
        
            }).then(function(){
                $.post("{{route('client.delete')}}",{id:id,_token:"{{csrf_token()}}"},function(data){
                    swal({
                        title:"Deleted Successfully",
                        type:"success"
            
                    }).then(function(){
                        location.reload();
                    })
                })
            });
        });

        $(document).on('click',"[id*='feature']",function(event){
            var id = $(this).attr("id").slice(7);
            $.ajax({
                type: "POST",
                url: "{{URL::to('admin/client/feature')}}",
                data: { 
                    id: id,
                    _token:"{{csrf_token()}}"
                },
                success: function(result) {
                
                },
                error: function(result) {
                    alert('Something went wrong.');
                }
            });
        });

    </script>
@endsection