<body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">

  <!-- navbar-fixed-top-->
  <nav class="header-navbar navbar navbar-with-menu navbar-fixed-top navbar-semi-dark navbar-shadow">
    <div class="navbar-wrapper">
      <div class="navbar-header">
        <ul class="nav navbar-nav">
          <li class="nav-item mobile-menu hidden-md-up float-xs-left"><a class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="icon-menu5 font-large-1"></i></a></li>
          <li class="nav-item"><a href="" class="navbar-brand nav-link"><img alt="Bagbox" src="{{asset('/logo.png')}}" style="height:4rem" class="brand-logo"></a></li>
          <li class="nav-item hidden-md-up float-xs-right"><a data-toggle="collapse" data-target="#navbar-mobile" class="nav-link open-navbar-container"><i class="icon-ellipsis pe-2x icon-icon-rotate-right-right"></i></a></li>
        </ul>
      </div>
      <div class="navbar-container content container-fluid">
        <div id="navbar-mobile" class="collapse navbar-toggleable-sm">
          <ul class="nav navbar-nav">
            <li class="nav-item hidden-sm-down"><a class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="icon-menu5">         </i></a></li>
            {{-- <li class="nav-item hidden-sm-down"><a href="#" class="nav-link nav-link-expand"><i class="ficon icon-expand2"></i></a></li> --}}
          </ul>
          <ul class="nav navbar-nav float-xs-right">

            <li class="dropdown dropdown-notification nav-item"><a href="#" data-toggle="dropdown" class="nav-link nav-link-label" onclick="getUnseenOrder()"><i class="ficon icon-bell4"></i><span class="tag tag-pill tag-default tag-danger tag-default tag-up" id="qno1">{{ $ordercount }}</span></a>
                <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                  <li class="dropdown-menu-header">
                    <h6 class="dropdown-header m-0"><span class="grey darken-2">Orders</span><span class="notification-tag tag tag-default tag-danger float-xs-right m-0" id="qno2">{{ $ordercount }} New</span></h6>
                  </li>
                  <li class="list-group scrollable-container" id="Qnotis">
                    
                  </li>
                  <li class="dropdown-menu-footer"><a href="/admin/orders" class="dropdown-item text-muted text-xs-center">View all new orders</a></li>
                </ul>
              </li>
              <li class="dropdown dropdown-notification nav-item"><a href="#" data-toggle="dropdown" class="nav-link nav-link-label" onclick="getUnseenQuote()"><i class="ficon icon-bell2"></i><span class="tag tag-pill tag-default tag-danger tag-default tag-up" id="rno1">{{ $quotationcount }}</span></a>
                <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                  <li class="dropdown-menu-header">
                    <h6 class="dropdown-header m-0"><span class="grey darken-2">Quotation Orders</span><span class="notification-tag tag tag-default tag-danger float-xs-right m-0" id="rno2">{{ $quotationcount }} New</span></h6>
                  </li>
                  <li class="list-group scrollable-container" id="Rnotis">
                    
                  </li>
                  <li class="dropdown-menu-footer"><a href="/admin/quotations" class="dropdown-item text-muted text-xs-center">Show all quotation orders</a></li>
                </ul>
              </li>
            
            
            <li class="dropdown dropdown-user nav-item"><a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link dropdown-user-link"><span class="avatar avatar-online"><img src="{{ route('optimize', ['img','avatar.png',30,30]) }}" alt="avatar"><i></i></span><span class="user-name" id="adminName">{{ Auth::guard('admin_user')->user()->name }}</span></a>
              <div class="dropdown-menu dropdown-menu-right">
                <a href="{{ route('profile') }}" class="dropdown-item"><i class="icon-head"></i> Edit Profile</a>
                <div class="dropdown-divider"></div>
                <a href="{{ url('/admin/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="dropdown-item">
                <i class="icon-power3"></i> Logout</a>
                <form id="logout-form" action="{{ url('/admin/logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
                </form>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </nav>

  <!-- ////////////////////////////////////////////////////////////////////////////-->


  <!-- main menu-->
  <div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
    <!-- main menu content-->
    <div class="main-menu-content">
      <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
        <br>
        <li id="dashboard" class="nav-item "><a href="{{URL::to('admin/')}}"><i class="icon-home3"></i><span data-i18n="nav.dash.main" class="menu-title">Dashboard</span></a>
        </li>

        <li id="managecategories" class="@if(Request::is('admin/manage/categories')) active @endif nav-item "><a href="{{URL::to('admin/manage/categories')}}"><i class="icon-ios-list"></i><span data-i18n="nav.staffs.main" class="menu-title">Manage Categories</span></a>
        </li>
        <li id="manageclients" class="@if(Request::is('admin/manage/clients')) active @endif nav-item "><a href="{{URL::to('admin/manage/clients')}}"><i class="icon-ios-list"></i><span data-i18n="nav.staffs.main" class="menu-title">Manage Clients</span></a>
        </li>
        
        <li id="tags" class="nav-item "><a href="{{ route('tags.index') }}"><i class="icon-tags"></i><span data-i18n="nav.tags" class="menu-title">Tags</span></a>
        </li>

        <li id="products" class="nav-item"><a href="{{ route('products.index') }}"><i class="icon-cart3"></i><span data-i18n="nav.albums.main" class="menu-title">Manage Products</span></a>
        </li> 

        <li id="deliveryzones" class="nav-item "><a href="{{ route('delivery_zones.index') }}"><i class="icon-truck"></i><span data-i18n="nav.deliveryzones.main" class="menu-title">Delivery Zones</span></a>
        </li>

        <li id="infos" class="nav-item"><a href="{{ route('infos.index') }}"><i class="icon-tags"></i><span data-i18n="nav.albums.main" class="menu-title">Manage Infos</span></a>
        </li> 

        <li id="orders" class="nav-item "><a href="{{ route('orders.index') }}"><i class="icon-at2"></i><span data-i18n="nav.orders" class="menu-title">Orders</span></a>
        </li>

        <li id="quotations" class="nav-item "><a href="{{ route('quotations.index') }}"><i class="icon-at2"></i><span data-i18n="nav.quotations" class="menu-title">Quotations</span></a>
        </li>

        <li id="blogs" class="nav-item "><a href="{{ route('blogs.index') }}"><i class="icon-list-alt"></i><span data-i18n="nav.blogs" class="menu-title">News/Events</span></a>
        </li>

        {{-- <li id="albums" class="nav-item "><a href="{{ route('albums.index') }}"><i class="icon-image3"></i><span data-i18n="nav.albums.main" class="menu-title">Albums</span></a>
        </li> --}}

        {{-- <li id="newsletter" class="nav-item "><a href="{{ route('newsletter') }}"><i class="icon-mail6"></i><span data-i18n="nav.newsletter.main" class="menu-title">Newsletter</span></a>
        </li> --}}
        <li id="downloads" class="nav-item "><a href="{{ route('downloads.index') }}"><i class="icon-download"></i><span data-i18n="nav.downloads.main" class="menu-title">Downloads</span></a>
        </li>

        <li id="testimonials" class="nav-item "><a href="{{ route('testimonials.index') }}"><i class="icon-commenting"></i><span data-i18n="nav.testimonials.main" class="menu-title">Testimonials</span></a>
        </li>

        {{-- <li id="contact-us-messages" class="nav-item "><a href="{{ route('contact-us-messages.index') }}"><i class="icon-at2"></i><span data-i18n="nav.contact-us-messages.main" class="menu-title">Contact Messages</span></a>
        </li> --}}
        <li id="pages" class="nav-item "><a href="{{ route('pages.index') }}"><i class="icon-ios-list"></i><span data-i18n="nav.pages.main" class="menu-title">Pages</span></a>
        </li>

        <li id="settings" class="nav-item "><a href="{{ route('admin.settings') }}"><i class="icon-gear"></i><span data-i18n="nav.settings.main" class="menu-title">Settings</span></a>
        </li>

        <li class="nav-item has-sub"><a href="#"><i class="icon-gear"></i><span data-i18n="nav.maps.main" class="menu-title">SEO Page Settings</span></a>
          <ul class="menu-content" style="">
            <li id="home_page_settings" class="is-shown {{ Request::is('*seo-settings/home_page_settings') ? 'active' : '' }}"><a href="{{ route('admin.seo-settings' , 'home_page_settings') }}" data-i18n="nav.settings.seo" class="menu-item">Home Page Settings</a>
            </li>
            <li id="shop_page_settings" class="is-shown {{ Request::is('*seo-settings/shop_page_settings') ? 'active' : '' }}"><a href="{{ route('admin.seo-settings' , 'shop_page_settings') }}" data-i18n="nav.settings.seo" class="menu-item">Shop Page Settings</a>
            </li>
            <li id="clients_page_settings" class="is-shown {{ Request::is('*seo-settings/clients_page_settings') ? 'active' : '' }}"><a href="{{ route('admin.seo-settings' , 'clients_page_settings') }}" data-i18n="nav.settings.seo" class="menu-item">Clients Page Settings</a>
            </li>
            <li id="blog_page_settings" class="is-shown {{ Request::is('*seo-settings/blog_page_settings') ? 'active' : '' }}"><a href="{{ route('admin.seo-settings' , 'blog_page_settings') }}" data-i18n="nav.settings.seo" class="menu-item">Blog Page Settings</a>
            </li>
            <li id="testimonials_page_settings" class="is-shown {{ Request::is('*seo-settings/testimonials_page_settings') ? 'active' : '' }}"><a href="{{ route('admin.seo-settings' , 'testimonials_page_settings') }}" data-i18n="nav.settings.seo" class="menu-item">Testimonials Page Settings</a>
            </li>
          </ul>
        </li>
      </ul>
    </div>
    <!-- /main menu content-->
    <!-- main menu footer-->
    <!-- include includes/menu-footer-->
    <!-- main menu footer-->
  </div>
  <!-- / main menu-->
  <div class="app-content content container-fluid">
      <div class="content-wrapper">
        
         @if (count($errors)>0 )
          <div class="alert alert-dismissible fade in mb-2">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true" style="color:black">&times;</span>
            </button>
            <ul class="list-group">
              @foreach ($errors->all() as $error)
                <li class="list-group-item list-group-item-danger">{{ $error }}</li>
              @endforeach
            </ul>
          </div>	
        @endif 